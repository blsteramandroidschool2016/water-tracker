package com.blstream.water_tracker.interfaces;

import android.graphics.Canvas;
import android.view.animation.Animation;

/**
 * Contains batch of basic control's operations
 */
public interface SwitchDrawable {
    /**
     * Gets state of control
     */
    float getState();

    /**
     * Sets state of control
     *
     * @param state values from 0.0 to 1.0, where 0.0 - unchecked, 1.0 - checked
     */
    void setState(float state);

    /**
     * Draws object on specified canvas
     *
     * @param canvas to be drawn on
     */
    void draw(Canvas canvas);

    /**
     * Gets new animation specified by param
     *
     * @param newState of the animation
     * @return new object's animation
     */
    Animation getNewAnimation(float newState);
}
