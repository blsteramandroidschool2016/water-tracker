package com.blstream.water_tracker.interfaces;

/**
 * Handles received water intake from Google Fit
 */
public interface OnWaterIntakeReceived {

    /**
     * Called when water intake value is received from Google Fit to save it in UserGoogleFitData
     * and to set it in UI
     */
    void waterIntakeReceived();
}
