package com.blstream.water_tracker.interfaces;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.blstream.water_tracker.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Handles Basic views events
 */
public interface IBasicView {
    int NO_INTERNET_CONNECTION_ERROR = R.string.no_internet_connection;
    int YOU_CANNOT_LOG_IN_WITHOUT_INTERNET = R.string.canNotLoginWithoutInternet;
    int YOU_CANNOT_LOG_OUT_WITHOUT_INTERNET = R.string.canNotLogoutWithoutInternet;
    int SOMETHING_GOES_WRONG_ERROR = R.string.somethingGoesWrongError;
    int LOGIN_FRAGMENT = 1;
    int MAIN_VIEW_FRAGMENT = 2;
    int HISTORY_VIEW_FRAGMENT = 3;

    /**
     * Shows error selecgted in the argument
     *
     * @param errorMode {@link IBasicView.ErrorMode}
     * @param context   instance of context
     * @param view      instance of view where error have to be displayed
     */
    void showError(Context context, View view, @ErrorMode int errorMode);

    /**
     * hide shown error
     */
    void hideError(Snackbar snackbar);

    /**
     * Changes fragment to a selected fragment in the argument
     *
     * @param fragmentType    {@link IBasicView.FragmentType}
     * @param fragmentManager {@inheritDoc}
     */
    void changeFragment(FragmentManager fragmentManager, @FragmentType int fragmentType);

    /**
     * Specifies three kinds of errors
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NO_INTERNET_CONNECTION_ERROR, YOU_CANNOT_LOG_IN_WITHOUT_INTERNET, YOU_CANNOT_LOG_OUT_WITHOUT_INTERNET,SOMETHING_GOES_WRONG_ERROR})
    @interface ErrorMode {
    }

    /**
     * Specifies three kinds of fragments
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LOGIN_FRAGMENT, MAIN_VIEW_FRAGMENT, HISTORY_VIEW_FRAGMENT})
    @interface FragmentType {
    }
}
