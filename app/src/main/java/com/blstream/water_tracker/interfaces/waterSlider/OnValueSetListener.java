package com.blstream.water_tracker.interfaces.waterSlider;

/**
 * Listener used in Water Slider
 */
public interface OnValueSetListener {

    /**
     * Called when WaterSlider's state has been set
     *
     */
    void onValueSet();
}
