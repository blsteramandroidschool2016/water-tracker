package com.blstream.water_tracker.interfaces;

/**
 * Callback used in CircleIcon
 */
public interface CircleIconSetStateCallback {

    /**
     * Called when CircleIcon state changes
     * @param state from 0 - left to 1 - right
     */
    void onSetState(float state);
}
