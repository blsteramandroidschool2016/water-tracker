package com.blstream.water_tracker.interfaces;

/**
 * Handles events after saving the weight from user input
 */
public interface OnWeightSaved {

    /**
     * Called when weight from user input was saved in Google Fit
     */
    void weightSaved();
}