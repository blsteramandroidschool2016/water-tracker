package com.blstream.water_tracker.interfaces;

import com.blstream.water_tracker.models.GoogleFitData;

import java.util.List;

/**
 * Provides historical data from google fit
 */
public interface HistoricalDataProvider {

    /**
     * @return list of all historical GoogleFitData (one entry per day) 
     */
    List<GoogleFitData> getAllHistoricalData();
}
