package com.blstream.water_tracker.interfaces.waterSlider;

/**
 * Listener used in WaterSliderController
 */
public interface OnValueChangedListener {

    /**
     * Called when WaterSlider's state changed
     *
     * @param state current WaterSlider state
     *              From IWaterController.MINUMUM_VALUE
     *              To WaterSliderController maxValue
     */
    void onChangeValue(float state);

}
