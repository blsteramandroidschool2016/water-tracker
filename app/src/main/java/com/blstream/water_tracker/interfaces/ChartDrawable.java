package com.blstream.water_tracker.interfaces;

import android.graphics.Canvas;

/**
 * Used in all drawable parts of Chart
 */
public interface ChartDrawable {

    /**
     * Draws object on specified canvas
     *
     * @param canvas to be drawn on
     */
    void draw(Canvas canvas);
}
