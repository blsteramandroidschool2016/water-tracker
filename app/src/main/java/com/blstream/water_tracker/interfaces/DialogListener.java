package com.blstream.water_tracker.interfaces;

/**
 * Used when user inserts data in Dialog Fragment
 */
public interface DialogListener {
    /**
     * Invokes when user confirms inserted data
     *
     * @param data to be inserted
     */
    void onInputDataListener(int data);
}
