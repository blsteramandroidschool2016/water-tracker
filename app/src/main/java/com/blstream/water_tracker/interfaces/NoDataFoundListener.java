package com.blstream.water_tracker.interfaces;

/**
 * Contains listener used in dialog fragment
 */
public interface NoDataFoundListener {
    /**
     * Listens if data was not found
     */
    void onDataNotFoundListener();
}
