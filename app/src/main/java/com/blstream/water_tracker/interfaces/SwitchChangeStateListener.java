package com.blstream.water_tracker.interfaces;

/**
 * Listener used in SwitchControl
 */
public interface SwitchChangeStateListener {
    /**
     * Called when SwitchControl's state has changed
     * @param state from 0 to 1
     *              0 means switch is on left side
     *              1 mean switch is on right side
     */
    void onStateChange(float state);
}
