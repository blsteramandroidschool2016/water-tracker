package com.blstream.water_tracker.interfaces;

import com.blstream.water_tracker.models.GoogleFitData;

import java.util.List;

/**
 * Interface used in history chart view
 */
public interface HistoryChart {

    /**
     * Set data for Chart
     * @param data GoogleFitData
     */
    void setData(List<GoogleFitData> data);
}
