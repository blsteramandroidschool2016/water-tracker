package com.blstream.water_tracker.interfaces;

/**
 * Handles received water requirement from Google Fit
 */
public interface OnWaterRequirementCalculated {

    /**
     * Called when water requirement value is received from Google Fit to save it in UserGoogleFitData
     * and to set it in UI
     */
    void waterRequirementCalculated();
}
