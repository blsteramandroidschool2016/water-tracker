package com.blstream.water_tracker.interfaces.waterSlider;

/**
 * Used to communication with WaterSliders controls views.
 */
public interface IWaterSliderControls  {
    /**
     * Minimum water value;
     */
    float MINIMUM_VALUE = 0.0f;
    /**
     * maximum allowed water lvl
     */
    float MAXIMUM_VALUE = 5f;

    /**
     * @return position of controls between MINIMUM_VALUE and maximum value of controls.
     */
    float getState();

    /**
     * Sets controls to specified position
     *
     * @param state position of controls between MINIMUM_VALUE and maximum value of controls .
     */
    void setState(float state);

    /**
     * Sets maximum position of controls
     *
     * @param maxValue max position of controls.
     */
    void setMaxValue(float maxValue);
}
