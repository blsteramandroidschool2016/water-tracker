package com.blstream.water_tracker.interfaces;

import android.app.Activity;

/**
 * Handles login operation
 */
public interface ILoginController {
    /**
     * Method signs in user with google plus account.
     * Called after click on button "Login" from LoginScreenView.
     */
    void googlePlusLogin();

    /**
     * Method changes fragment to fragment with lists of places.
     * Called when button "No Thanks" clicked from LoginScreenView.
     */
    void handleNoThanks(Activity activity);
}
