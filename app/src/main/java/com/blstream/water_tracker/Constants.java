package com.blstream.water_tracker;

import com.blstream.water_tracker.controllers.GoogleFitController;

/**
 * Contains constants used in project
 */
public class Constants {
    /**
     * Request code for sign in operation
     */
    public final static int RC_SIGN_IN = 0;

    /**
     * Key for login status in shared preferences
     */
    public final static String ACCOUNT_NAME_KEY = "ACCOUNT_NAME_KEY";

    /**
     * Key for water indicator in shared preferences
     */
    public final static String WATER_INDICATOR_KEY = "WATER_INDICATOR";

    /**
     * Key for max value of water in shared preferences
     */
    public final static String MAX_VALUE_KEY = "MAX_VALUE_KEY";

    /**
     * Shared preferences fileName
     */
    public static final String WATER_TRACKER_PREFS = "Water_tracker_prefs";

    /**
     * Shared preferences max value fileName
     */
    public static final String WATER_TRACKER_PREFS_MAX_VALUE = "Water_tracker_prefs_max_value";

    /**
     * Shared preferences water indicator fileName
     */
    public static final String WATER_TRACKER_PREFS_WATER_INDICATOR = "Water_tracker_prefs_water_indicator";


    /**
     * Data type name for daily water requirement
     */
    public static final String WATER_REQUIREMENT_DATA_TYPE = "com.blstream.water_tracker.water_requirement";

    /**
     * Data type name for current daily water intake
     */
    public static final String WATER_INTAKE_DATA_TYPE = "com.blstream.water_tracker.water_intake";

    /**
     * Field name for current daily water intake
     */
    public static final String WATER_INTAKE_FIELD = "current_water_intake";

    /**
     * Field name for current daily water requirement
     */
    public static final String WATER_REQUIREMENT_FIELD = "water_requirement";
    public static final String SWITCH_STATE_KEY = "switch_state";
    /**
     * Tag for Google Fit controller
     */
    private static final String GOOGLE_FIT_CONTROLLER_TAG = GoogleFitController.class.getName();
    /**
     * Stream name for current daily water intake
     */
    public static final String WATER_INTAKE_STREAM_NAME = GOOGLE_FIT_CONTROLLER_TAG + " - water intake";
    /**
     * Stream name for current daily water requirement
     */
    public static final String WATER_REQUIREMENT_STREAM_NAME = GOOGLE_FIT_CONTROLLER_TAG + " - water requirement";
    /**
     * AsyncTask name
     */
    public static final String THREAD_NAME = "task";

}
