package com.blstream.water_tracker.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.blstream.water_tracker.Constants;
import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.IGoogleApiCallback;
import com.blstream.water_tracker.interfaces.NoDataFoundListener;
import com.blstream.water_tracker.interfaces.OnWaterIntakeReceived;
import com.blstream.water_tracker.interfaces.OnWaterRequirementCalculated;
import com.blstream.water_tracker.interfaces.OnWeightSaved;
import com.blstream.water_tracker.models.DailyWaterRequirement;
import com.blstream.water_tracker.models.UserGoogleFitData;
import com.blstream.water_tracker.view.fragments.BasicView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataTypeCreateRequest;
import com.google.android.gms.fitness.request.DataUpdateRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.fitness.result.DataTypeResult;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleFitController extends FragmentController<BasicView> implements IGoogleApiCallback {

    private static final long HOUR_IN_MILLISECONDS = 3600000;
    private static GoogleApiClient googleApiClient;
    private static GoogleApiCallbacksHandler googleApiCallbacksHandler;
    private static DataType waterIntakeDataType = null;
    public OnWaterIntakeReceived onWaterIntakeReceivedListener;
    public OnWaterRequirementCalculated onWaterRequirementCalculatedListener;
    public OnWeightSaved onWeightSavedListener;
    private NoDataFoundListener noDataFoundListener;
    private Context context;

    /**
     * @param fragment Fragment fragment to assign to controller
     * @param context  Context instance of Context
     */
    public GoogleFitController(Fragment fragment, Context context) {
        super(fragment, (BasicView) fragment);
        this.context = context;
        googleApiClient = GoogleApiController.getGoogleApiClient();
        googleApiCallbacksHandler = GoogleApiController.getCallbacksHandler();
    }

    /**
     * @param noDataFoundListener listener
     */
    public void setNoDataFoundListener(NoDataFoundListener noDataFoundListener) {
        this.noDataFoundListener = noDataFoundListener;
    }

    /**
     * Retrieves weight, current number of burned Calories and current daily water intake from Google Fit and saves it
     * in UserGoogleFitData class. Afterwards it calculates the user's daily water requirement
     * and saves it both in UserGoogleFitData class and in Google Fit.
     */
    public void retrieveInitialGoogleFitData() {
        retrieveCustomDataFromGoogleFit(Constants.WATER_INTAKE_DATA_TYPE);
        readDataFromGoogleFit(DataType.TYPE_WEIGHT);
        readAggregatedDataFromGoogleFit(DataType.AGGREGATE_CALORIES_EXPENDED);
    }

    /**
     * Reads aggregated data from current day based on given data type from Google Fit
     *
     * @param dataType type of data to read
     */
    public void readAggregatedDataFromGoogleFit(final DataType dataType) {
        Fitness.HistoryApi.readDailyTotal(googleApiClient, dataType).setResultCallback(new ResultCallback<DailyTotalResult>() {
            /**
             * {@inheritDoc}
             */
            @Override
            public void onResult(@NonNull DailyTotalResult dailyTotalResult) {
                DataSet totalSet = dailyTotalResult.getTotal();
                assert totalSet != null;
                float aggregatedData = totalSet.isEmpty() ? 0 : totalSet.getDataPoints().get(0).getValue(Field.FIELD_CALORIES).asFloat();
                setUserData(dataType, aggregatedData);
            }
        });
    }

    /**
     * Reads standard Google Fit based on given data type from Google Fit
     *
     * @param dataType type of data to read
     */
    public void readDataFromGoogleFit(final DataType dataType) {

        final DataReadRequest dataReadRequest = queryFitnessData(dataType);
        Fitness.HistoryApi.readData(googleApiClient, dataReadRequest).setResultCallback(new ResultCallback<DataReadResult>() {
            /**
             * {@inheritDoc}
             */
            @Override
            public void onResult(@NonNull DataReadResult dataReadResult) {
                DataSet dataSet = dataReadResult.getDataSet(dataType);
                List<DataPoint> listOfDataPoints = dataSet.getDataPoints();
                if (listOfDataPoints.size() > 0) {
                    int indexOrFirstElement = 0;
                    DataPoint dataPoint = listOfDataPoints.get(indexOrFirstElement);
                    Field field = dataPoint.getDataType().getFields().get(indexOrFirstElement);
                    float profileData = dataPoint.getValue(field).asFloat();
                    setUserData(dataType, profileData);
                } else {
                    if (dataType.equals(waterIntakeDataType)) {
                        UserGoogleFitData.isWaterIntakeRetrieved = true;
                        onWaterIntakeReceivedListener.waterIntakeReceived();
                    }
                    if (dataType.equals(DataType.TYPE_WEIGHT)) {
                        noDataFoundListener.onDataNotFoundListener();
                    }
                }
            }
        });
    }

    /**
     * Creates custom data for information to save and inserts it into Google Plus
     *
     * @param dataToSave custom data to save in Google Fit
     * @param dataType   string defining type of data to save in Google Fit
     * @param fieldName  string defining name of the field in which data is going to be save
     * @param streamName string defining name of the stream which identifies the data to save
     */
    public void saveCustomDataToGoogleFit(final float dataToSave, final String dataType, final String fieldName, final String streamName) {

        DataTypeCreateRequest request = new DataTypeCreateRequest.Builder()
                .setName(dataType)
                .addField(fieldName, Field.FORMAT_FLOAT)
                .build();

        PendingResult<DataTypeResult> pendingResult =
                Fitness.ConfigApi.createCustomDataType(googleApiClient, request);

        pendingResult.setResultCallback(
                new ResultCallback<DataTypeResult>() {
                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public void onResult(@NonNull DataTypeResult dataTypeResult) {
                        DataType customType = dataTypeResult.getDataType();
                        if (customType != null) {
                            insertData(dataToSave, customType, streamName);
                        }
                    }
                }
        );
    }


    /**
     * Retrieves data specified by given data type saved in Google Fit
     *
     * @param dataType string defining type of data to retrieve from Google Fit
     */
    public void retrieveCustomDataFromGoogleFit(final String dataType) {

        PendingResult<DataTypeResult> pendingResult =
                Fitness.ConfigApi.readDataType(googleApiClient, dataType);

        pendingResult.setResultCallback(
                new ResultCallback<DataTypeResult>() {
                    @Override
                    public void onResult(@NonNull DataTypeResult dataTypeResult) {
                        DataType customType = dataTypeResult.getDataType();
                        if (customType != null) {
                            if (dataType.equals(Constants.WATER_INTAKE_DATA_TYPE)) {
                                waterIntakeDataType = customType;
                            }
                            readDataFromGoogleFit(customType);
                        }
                    }
                }
        );
    }


    /**
     * Registers listener in google fit controller
     *
     * @param callback instance of IGoogleApiCallback which handles callbacks from Google Api
     */
    public void registerCallbackHandler(IGoogleApiCallback callback) {
        googleApiCallbacksHandler.registerListener(callback);
    }


    /**
     * Unregisters listener from google fit controller
     *
     * @param callback instance of IGoogleApiCallback which handles callbacks from Google Api
     */
    public void unregisterCallbackHandler(IGoogleApiCallback callback) {
        googleApiCallbacksHandler.unregisterListener(callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnected(Bundle bundle) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionSuspended(int i) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onGoogleApiActivityResult(int requestCode, int responseCode, Intent data, int RESULT_OK) {

    }

    /**
     * Reloading google fit data if one hour is passed between last timestamp and actually timestamp.
     *
     * @param timestamp last time in timestamp.
     */
    public void reloadInitialGoogleFitData(Timestamp timestamp) {
        Timestamp actualTimeStamp = new Timestamp(System.currentTimeMillis());
        if ((actualTimeStamp.getTime() - timestamp.getTime()) > HOUR_IN_MILLISECONDS) {
            retrieveInitialGoogleFitData();
        }
    }

    /**
     * Writes standard Google Fit data based on given data type to Google Fit
     *
     * @param dataType    type of data to read
     * @param valueToSave data to save to Google Fit
     */
    public void writeDataToGoogleFit(final DataType dataType, final float valueToSave) {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        long startTime = setStartTime(cal, dataType);

        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(context)
                .setDataType(dataType)
                .setType(DataSource.TYPE_RAW)
                .build();

        DataSet dataSet = DataSet.create(dataSource);
        DataPoint dataPoint = dataSet.createDataPoint().setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);


        dataPoint = dataPoint.setFloatValues(valueToSave);

        dataSet.add(dataPoint);

        DataUpdateRequest request = new DataUpdateRequest.Builder()
                .setDataSet(dataSet)
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();
        Fitness.HistoryApi.updateData(googleApiClient, request).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                setUserData(dataType, valueToSave);
            }
        });
    }

    /**
     * Inserts given data into Google Fit
     *
     * @param dataToSave custom data to insert into Google Fit
     * @param customType string defining custom type of data to insert into Google Fit
     * @param streamName string defining name of the stream which identifies the data to insert
     */
    void insertData(float dataToSave, DataType customType, String streamName) {

        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        long startTime = setStartTime(cal, customType);

        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(context)
                .setDataType(customType)
                .setStreamName(streamName)
                .setType(DataSource.TYPE_RAW)
                .build();

        DataSet dataSet = DataSet.create(dataSource);
        DataPoint dataPoint = dataSet.createDataPoint()
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
        dataPoint.getValue(dataSet.getDataType().getFields().get(0)).setFloat(dataToSave);
        dataSet.add(dataPoint);

        DataUpdateRequest request = new DataUpdateRequest.Builder()
                .setDataSet(dataSet)
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        com.google.android.gms.common.api.Status updateStatus =
                Fitness.HistoryApi.updateData(googleApiClient, request)
                        .await(0, TimeUnit.MINUTES);

        if (!updateStatus.isSuccess()) {
            return;
        }
    }

    /**
     * Creates data request to Google Fit
     *
     * @param dataType type of data to retrieve from Google Fit
     * @return generated instance of DataReadRequest
     */
    DataReadRequest queryFitnessData(DataType dataType) {

        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        long startTime = setStartTime(cal, dataType);
        DataReadRequest readRequest;
        readRequest = new DataReadRequest.Builder()
                .read(dataType)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .setLimit(1)
                .build();
        return readRequest;
    }

    private long setStartTime(Calendar calendar, DataType dataType) {
        long startTime;
        if (dataType == DataType.TYPE_WEIGHT) {
            startTime = 1;
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            startTime = calendar.getTimeInMillis();
        }
        return startTime;
    }

    private void setUserData(DataType dataType, float retrievedData) {
        if (retrievedData < 0) {
            return;
        }
        if (dataType == DataType.TYPE_WEIGHT) {
            UserGoogleFitData.setUserWeight(retrievedData);
            UserGoogleFitData.isWeightRetrieved = true;
            onWeightSavedListener.weightSaved();
        } else if (dataType == DataType.AGGREGATE_CALORIES_EXPENDED) {
            UserGoogleFitData.setBurnedCalories((long) retrievedData);
            UserGoogleFitData.areCaloriesRetrieved = true;
        } else if (dataType.getName().equals(Constants.WATER_INTAKE_DATA_TYPE)) {
            UserGoogleFitData.setWaterIntake(retrievedData);
            UserGoogleFitData.isWaterIntakeRetrieved = true;
            onWaterIntakeReceivedListener.waterIntakeReceived();
        } else {
            UserGoogleFitData.setWaterRequirement(retrievedData);
        }
        if (UserGoogleFitData.isWeightRetrieved & UserGoogleFitData.areCaloriesRetrieved
                & !UserGoogleFitData.isWaterRequirementCalculated) {
            calculateWaterRequirement();
        }
    }

    private void calculateWaterRequirement() {
        DailyWaterRequirement dailyWaterRequirement = new DailyWaterRequirement();
        float waterRequirement = dailyWaterRequirement.calculateWaterRequirement();
        UserGoogleFitData.setWaterRequirement(waterRequirement);
        UserGoogleFitData.isWaterRequirementCalculated = true;
        saveCustomDataToGoogleFit(waterRequirement, Constants.WATER_REQUIREMENT_DATA_TYPE, Constants.WATER_REQUIREMENT_FIELD, Constants.WATER_REQUIREMENT_STREAM_NAME);
        onWaterRequirementCalculatedListener.waterRequirementCalculated();
    }
}