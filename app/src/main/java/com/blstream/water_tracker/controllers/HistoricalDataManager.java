package com.blstream.water_tracker.controllers;

import com.blstream.water_tracker.Constants;
import com.blstream.water_tracker.interfaces.HistoricalDataProvider;
import com.blstream.water_tracker.models.GoogleFitData;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.fitness.result.DataTypeResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Downloads
 */
public class HistoricalDataManager implements HistoricalDataProvider {
    private static final int DAYS_BACK = 56;
    private static final float COMMA_SHIFT = 10;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GoogleFitData> getAllHistoricalData() {
        GoogleApiClient client = GoogleApiController.getGoogleApiClient();

        DataType intakeDataType = getDataType(client);
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(getTime()[0], getTime()[1], TimeUnit.MILLISECONDS)
                .build();
        DataReadResult dataReadResult = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);
        long calories = 0;
        float weight = getWeight(client, getTime()[0], getTime()[1]);
        List<GoogleFitData> data = new ArrayList<>();
        for (Bucket bucket : dataReadResult.getBuckets()) {
            calories = getCaloriesFromBucket(calories, bucket);
            long currentTime = bucket.getEndTime(TimeUnit.MILLISECONDS);
            float waterIntake = getWaterIntake(client, currentTime, intakeDataType);
            data.add(new GoogleFitData(
                    calories,
                    currentTime,
                    getTextFromValue(waterIntake),
                    weight
            ));
            calories = 0;
        }
        Collections.reverse(data);
        return data;
    }

    private long getCaloriesFromBucket(long calories, Bucket bucket) {
        List<DataSet> dataSets = bucket.getDataSets();
        for (DataSet dataSet : dataSets) {
            if (dataSet.getDataType().equals(DataType.AGGREGATE_CALORIES_EXPENDED)) {
                for (DataPoint dataPoint : dataSet.getDataPoints()) {
                    calories += (long) dataPoint.getValue(Field.FIELD_CALORIES).asFloat();
                }
            }
        }
        return calories;
    }

    private DataType getDataType(GoogleApiClient client) {
        PendingResult<DataTypeResult> pendingResult =
                Fitness.ConfigApi.readDataType(client, Constants.WATER_INTAKE_DATA_TYPE);
        DataTypeResult dataTypeResult = pendingResult.await();
        return dataTypeResult.getDataType();
    }

    private float getWeight(GoogleApiClient client, long startTime, long endTime) {
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .read(DataType.TYPE_WEIGHT)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();
        DataReadResult dataReadResult = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);
        if (dataReadResult.getDataSet(DataType.TYPE_WEIGHT).getDataPoints().size() > 0) {
            return dataReadResult.getDataSet(DataType.TYPE_WEIGHT).getDataPoints().get(0).getValue(Field.FIELD_WEIGHT).asFloat();
        } else {
            return 0;
        }
    }

    private float getWaterIntake(GoogleApiClient client, long endTime, DataType dataType) {
        long startTime = endTime - TimeUnit.DAYS.toMillis(1);
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .read(dataType)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();
        DataReadResult dataReadResult = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);
        if (dataReadResult.getDataSet(dataType).getDataPoints().size() > 0) {
            return dataReadResult.getDataSet(dataType).getDataPoints().get(0).getValue(dataType.getFields().get(0)).asFloat();
        } else {
            return 0;
        }
    }

    private long[] getTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.DAY_OF_YEAR, DAYS_BACK * -1);
        long startTime = cal.getTimeInMillis();
        return new long[]{startTime, endTime};
    }

    private float getTextFromValue(float value){
        value = value * COMMA_SHIFT;
        value = Math.round(value);
        value = value / COMMA_SHIFT;
        return value;
    }
}
