package com.blstream.water_tracker.controllers;


import android.support.v4.app.Fragment;

import com.blstream.water_tracker.interfaces.IBasicView;

/**
 * Base class for all controllers
 *
 * @param <T> view to be assigned to current controller
 */
public abstract class FragmentController<T extends IBasicView> {

    protected Fragment fragment;
    protected T view;

    /**
     * @param fragment to be set to current controller
     */
    public FragmentController(Fragment fragment, T view) {
        this.fragment = fragment;
        this.view = view;
    }

    /**
     * @param view to be set to current controller
     */
    public void setView(T view) {
        this.view = view;
    }
}
