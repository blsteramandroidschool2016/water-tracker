package com.blstream.water_tracker.controllers;


import android.content.Context;
import android.content.SharedPreferences;

import com.blstream.water_tracker.Constants;

/**
 * Handles saving and collecting data from shared preferences
 */
public abstract class SharedPreferencesController {
    private static SharedPreferences sharedPreferences;

    /**
     * Retrieves user's account name from Shared Preferences
     *
     * @param context instance of Context
     * @return accountName user's account name - user's email address
     */
    public static String getUserAccountName(Context context) {
        String accountName = null;
        sharedPreferences = context.getSharedPreferences(Constants.WATER_TRACKER_PREFS, Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            accountName = sharedPreferences.getString(Constants.ACCOUNT_NAME_KEY, "");
        }
        return accountName;
    }

    /**
     * Sets the user with given account name as logged
     *
     * @param context     instance of Context
     * @param accountName user's account name - user's email address
     * @return SharedPreferences instance of SharedPreferences
     */
    public static SharedPreferences setUserAsLogged(Context context, String accountName) {
        sharedPreferences = context.getSharedPreferences(Constants.WATER_TRACKER_PREFS, Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Constants.ACCOUNT_NAME_KEY, accountName);
            editor.putBoolean(accountName, true);
            editor.apply();
        }
        return sharedPreferences;
    }

    /**
     * Sets the switch state  in shared preferences
     *
     * @param context     instance of Context
     * @param switchState current state of switch
     * @return SharedPreferences instance of SharedPreferences
     */
    public static SharedPreferences setSwitchState(Context context, boolean switchState) {
        sharedPreferences = context.getSharedPreferences(Constants.WATER_TRACKER_PREFS, Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(Constants.SWITCH_STATE_KEY, switchState);
            editor.apply();
        }
        return sharedPreferences;
    }

    /**
     * Sets the water indicator of user in shared preferences
     *
     * @param context   instance of Context
     * @param indicator water indicator value
     * @return SharedPreferences instance of SharedPreferences
     */
    public static SharedPreferences setUserWaterIndicator(Context context, float indicator) {
        sharedPreferences = context.getSharedPreferences(Constants.WATER_TRACKER_PREFS_WATER_INDICATOR, Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putFloat(Constants.WATER_INDICATOR_KEY, indicator);
            editor.apply();
        }
        return sharedPreferences;
    }


    /**
     * Removes logged user from shared preferences
     *
     * @param context     instance of Context
     * @param accountName user account name - user's email address
     * @return SharedPreferences instance of SharedPreferences
     */
    public static SharedPreferences removeUserData(Context context, final String accountName) {
        sharedPreferences = context.getSharedPreferences(Constants.WATER_TRACKER_PREFS, Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(accountName);
            editor.remove(Constants.ACCOUNT_NAME_KEY);
            editor.apply();
        }
        return sharedPreferences;
    }

    /**
     * Checks if the user is already logged in
     *
     * @param context     instance of Context
     * @param accountName user account name - user's email address
     * @return login state of user account collected from shared preferences
     */
    public static boolean isUserLogged(Context context, String accountName) {
        boolean isLogged = false;
        sharedPreferences = context.getSharedPreferences(Constants.WATER_TRACKER_PREFS, Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            isLogged = sharedPreferences.getBoolean(accountName, false);
        }
        return isLogged;
    }


    /**
     * @param context instance of Context
     * @return water indicator value
     */
    public static float getWaterIndicator(Context context) {
        float waterIndicator = 0.0f;
        sharedPreferences = context.getSharedPreferences(Constants.WATER_TRACKER_PREFS_WATER_INDICATOR, Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            waterIndicator = sharedPreferences.getFloat(Constants.WATER_INDICATOR_KEY, 0.0f);
        }
        return waterIndicator;
    }

    /**
     * @param context instance of app context
     * @return switch state retrieved from  shared preferences
     */
    public static boolean getSwitchState(Context context) {
        boolean switchStateValue = true; //Liters
        sharedPreferences = context.getSharedPreferences(Constants.WATER_TRACKER_PREFS, Context.MODE_PRIVATE);
        if (sharedPreferences != null) {
            switchStateValue = sharedPreferences.getBoolean(Constants.SWITCH_STATE_KEY, true);
        }
        return switchStateValue;
    }
}
