package com.blstream.water_tracker.controllers;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.blstream.water_tracker.interfaces.IBasicView;
import com.blstream.water_tracker.view.fragments.MainView;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Handles logout operation
 */
public class LogoutController extends FragmentController<MainView> {

    private static GoogleApiClient googleApiClient;
    private final SharedPreferencesControllerFactory factory;

    /**
     * @param fragment Fragment fragment to assign to controller
     */
    public LogoutController(Fragment fragment) {
        this(fragment, new SharedPreferencesControllerFactoryImpl());
        googleApiClient = GoogleApiController.getGoogleApiClient();
    }

    /**
     * @param fragment Fragment fragment to assign to controller
     * @param factory  SharedPreferencesControllerFactory factory for wrapping abstract class to object
     */
    public LogoutController(Fragment fragment, SharedPreferencesControllerFactory factory) {
        super(fragment, (MainView) fragment);
        this.factory = factory;
    }

    /**
     * Logs out user from app and changes fragment to loginView.
     * Called after click on button "Logout" in options menu.
     */
    public void logout(Context context, String accountName) {
        googlePlusLogout();
        removeUserDataFromApp(context, accountName);
        getView().changeFragment(getView().getFragmentManager(), IBasicView.LOGIN_FRAGMENT);
    }

    /**
     * Logs out user from google plus account.
     */
    void googlePlusLogout() {
        if (googleApiClient.isConnected()) {
            googleApiClient.clearDefaultAccountAndReconnect();
            googleApiClient.disconnect();
        }
    }

    /**
     * Removes user data from app's shared preferences.
     */
    void removeUserDataFromApp(Context context, String accountName) {
        if (factory.isUserLogged(context, accountName)) {
            factory.removeUserData(context, accountName);
        }
    }

    /**
     * @return view instance of MainView. For testing purposes.
     */
    MainView getView() {
        return view;
    }

    /**
     * For testing purposes
     * Abstract factory class for wrapping abstract class to object to test static methods
     */
    abstract static class SharedPreferencesControllerFactory {
        abstract boolean isUserLogged(Context context, String accountName);

        abstract void removeUserData(Context context, String accountName);
    }

    /**
     * For testing purposes
     * Factory implementation for wrapping abstract class to object to test static methods
     */
    static class SharedPreferencesControllerFactoryImpl extends SharedPreferencesControllerFactory {

        /**
         * {@inheritDoc}
         */
        @Override
        boolean isUserLogged(Context context, String accountName) {
            return SharedPreferencesController.isUserLogged(context, accountName);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        void removeUserData(Context context, String accountName) {
            SharedPreferencesController.removeUserData(context, accountName);
        }
    }
}