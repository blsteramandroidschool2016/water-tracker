package com.blstream.water_tracker.controllers;


import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Manages
 */
public class WaterUpdateController {

    private static final int NEXT_MONTH = 1;

    /**
     * Gets current timestamp
     *
     * @return current timestamp
     */
    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * Checks if drunk water is out of date.
     * Calendar.MONTH is counting from 0 value, that's why it adds 1 value
     *
     * @param lastUpdate last update of drunk water
     * @return whether drunk water is out of date
     */
    public static boolean checkIfOutOfDate(Timestamp lastUpdate) {
        long serverTimeStamp = lastUpdate.getTime();
        long currentTimeStamp = getCurrentTimestamp().getTime();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(serverTimeStamp);
        int serverDay = calendar.get(Calendar.DAY_OF_MONTH);
        int serverMonth = calendar.get(Calendar.MONTH) + NEXT_MONTH;
        int serverYear = calendar.get(Calendar.YEAR);

        calendar.setTimeInMillis(currentTimeStamp);
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        int currentMonth = calendar.get(Calendar.MONTH) + NEXT_MONTH;
        int currentYear = calendar.get(Calendar.YEAR);

        boolean isSameDay = serverDay == currentDay;
        boolean isSameMonth = serverMonth == currentMonth;
        boolean isSameYear = serverYear == currentYear;

        return !isSameDay || !isSameMonth || !isSameYear;
    }


}
