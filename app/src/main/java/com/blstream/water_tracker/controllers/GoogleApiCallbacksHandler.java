package com.blstream.water_tracker.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.blstream.water_tracker.interfaces.IGoogleApiCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.HashSet;
import java.util.Set;

/**
 * Handles callbacks from Google Api
 */
public class GoogleApiCallbacksHandler implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    Set<IGoogleApiCallback> callbacks = new HashSet<>();

    /**
     * Registers listener in controller
     *
     * @param listener to register
     */
    public void registerListener(IGoogleApiCallback listener) {
        callbacks.add(listener);
    }

    /**
     * Unregisters listener from controller
     *
     * @param listener to unregister
     */
    public void unregisterListener(IGoogleApiCallback listener) {
        callbacks.remove(listener);
    }

    /**
     * Sends response code from google api to controller
     *
     * @param requestCode  int listener code
     * @param responseCode int returned result
     * @param data         Intent returned data
     * @param RESULT_OK    int successful pattern result
     */
    public void notifyAboutActivityResult(int requestCode, int responseCode, Intent data, final int RESULT_OK) {
        for (IGoogleApiCallback listener : callbacks) {
            listener.onGoogleApiActivityResult(requestCode, responseCode, data, RESULT_OK);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnected(Bundle bundle) {
        for (IGoogleApiCallback listener : callbacks) {
            listener.onConnected(bundle);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionSuspended(int i) {
        for (IGoogleApiCallback listener : callbacks) {
            listener.onConnectionSuspended(i);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        for (IGoogleApiCallback listener : callbacks) {
            listener.onConnectionFailed(connectionResult);
        }
    }
}
