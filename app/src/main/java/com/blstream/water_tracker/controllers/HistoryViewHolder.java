package com.blstream.water_tracker.controllers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.blstream.water_tracker.R;

/**
 * Describes an item view and metadata about its place within the RecyclerView.
 */
public class HistoryViewHolder extends RecyclerView.ViewHolder {
    private TextView time;
    private TextView recommendedLitersWater;
    private TextView actualLitersWater;
    private TextView actualCupsWater;
    private TextView recommendedCupsWater;

    /**
     * Sets specified child view with the given id.
     * @param itemView specified view
     */
    public HistoryViewHolder(View itemView) {
        super(itemView);
        time = (TextView) itemView.findViewById(R.id.dateTextView);
        recommendedLitersWater = (TextView) itemView.findViewById(R.id.recommendedLitersTextView);
        actualLitersWater = (TextView) itemView.findViewById(R.id.actualLitersTextView);
        recommendedCupsWater = (TextView) itemView.findViewById(R.id.recommendedCupsTextView);
        actualCupsWater = (TextView) itemView.findViewById(R.id.actualCupsTextView);
    }

    /**
     * @return TextView of time
     */
    public TextView getTime() {
        return time;
    }

    /**
     * @return TextView of recommended liters water
     */
    public TextView getRecommendedLitersWater() {
        return recommendedLitersWater;
    }

    /**
     * @return TextView of actual liters water
     */
    public TextView getActualLitersWater() {
        return actualLitersWater;
    }

    /**
     * @return TextView of actual cups water
     */
    public TextView getActualCupsWater() {
        return actualCupsWater;
    }

    /**
     * @return TextView of recommended cups water
     */
    public TextView getRecommendedCupsWater() {
        return recommendedCupsWater;
    }
}
