package com.blstream.water_tracker.controllers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.models.DailyWaterRequirement;
import com.blstream.water_tracker.models.GoogleFitData;
import com.blstream.water_tracker.view.controls.waterSliderControls.components.LeftGraduationDrawer;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Connects recycler view with specified data
 */
public class HistoryDataAdapter extends RecyclerView.Adapter<HistoryViewHolder> {
    private static final String DATE_FORMAT = "dd/MM/yy";
    private static final float COMMA_SHIFT = 10;
    private List<GoogleFitData> dataList = new ArrayList<>();
    private Context context;

    /**
     * @param context of view
     */
    public HistoryDataAdapter(Context context) {
        this.context = context;
    }

    /**
     * Sets single item layout
     *
     * @param parent   of view
     * @param viewType type of view
     * @return view holder with specified view
     */
    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.single_history_item, parent, false);

        return new HistoryViewHolder(itemView);
    }

    /**
     * Binds data to particular views
     *
     * @param holder   of all elements
     * @param position of the dataList's element
     */
    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position) {
        GoogleFitData googleFitData = dataList.get(position);
        holder.getActualLitersWater().setText(context.getResources().getQuantityString(R.plurals.litersLabel
                , (int) googleFitData.getWaterIntake(), googleFitData.getWaterIntake()));
        int actualCupsWater = (int) (googleFitData.getWaterIntake() / LeftGraduationDrawer.CUP_SIZE);
        holder.getActualCupsWater().setText(context.getResources().getQuantityString(R.plurals.cupsLabel
                , actualCupsWater, actualCupsWater));
        holder.getTime().setText(convertToGracefulDateFormat(googleFitData.getTime()));
        double weight = (double) googleFitData.getWeight();
        double burnedCalories = (double) googleFitData.getBurnedCalories();
        double waterReqInLiters = DailyWaterRequirement.calculateWaterRequirement(weight, burnedCalories);
        waterReqInLiters = getTextFromValue((float) waterReqInLiters);
        holder.getRecommendedLitersWater().setText(context.getResources().getQuantityString(R.plurals.litersLabel
                , (int) waterReqInLiters, waterReqInLiters));
        int waterReqInCups = (int) (waterReqInLiters / LeftGraduationDrawer.CUP_SIZE);
        holder.getRecommendedCupsWater().setText(context.getResources().getQuantityString(R.plurals.cupsLabel
                , waterReqInCups, waterReqInCups));
    }

    /**
     * @return dataList size
     */
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    /**
     * Sets data list
     *
     * @param dataList to be set
     */
    public void setDataList(List<GoogleFitData> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    public List<GoogleFitData> getData() {
        return dataList;
    }

    private String convertToGracefulDateFormat(long time) {
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, java.util.Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    private float getTextFromValue(float value){
        value = value * COMMA_SHIFT;
        value = Math.round(value);
        value = value / COMMA_SHIFT;
        return value;
    }
}
