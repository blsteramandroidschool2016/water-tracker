package com.blstream.water_tracker.controllers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.blstream.water_tracker.Constants;
import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.IBasicView;
import com.blstream.water_tracker.interfaces.IGoogleApiCallback;
import com.blstream.water_tracker.interfaces.ILoginController;
import com.blstream.water_tracker.models.UserGoogleFitData;
import com.blstream.water_tracker.view.fragments.LoginView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

/**
 * {@inheritDoc}
 */
public class LoginController extends FragmentController<LoginView> implements ILoginController, IGoogleApiCallback {

    private static GoogleApiClient googleApiClient;
    private static GoogleApiCallbacksHandler googleApiCallbacksHandler;
    private ProgressDialog progressDialog;


    /**
     * @param fragment Fragment fragment to assign to controller
     */
    public LoginController(Fragment fragment) {
        super(fragment, (LoginView) fragment);
        String accountName = SharedPreferencesController.getUserAccountName(fragment.getContext());
        Activity activity = fragment.getActivity();
        googleApiCallbacksHandler = GoogleApiController.getCallbacksHandler();
        if (checkIfAlreadyLoggedIn(accountName)) {
            UserGoogleFitData.setAccountName(accountName);
            googleApiClient = GoogleApiController.buildGPApiClientWhenLogged(accountName, activity);
            googlePlusLogin();
        } else {
            googleApiClient = GoogleApiController.buildGPApiClientWhenNotLogged(activity);
        }
    }


    /**
     * Connects user to google api with google plus account
     */
    public void googlePlusLogin() {
        initialProgressDialog(fragment);
        if (!googleApiClient.isConnecting()) {
            googleApiClient.connect();
            progressDialog.show();
        }
    }

    /**
     * Handles the action after clicking "No thanks" button.
     * By clicking this button user declines login and application finishes.
     */
    public void handleNoThanks(Activity activity) {
        activity.finish();
    }

    /**
     * Registers listener in login controller
     *
     * @param callback instance of IGoogleApiCallback which handles callbacks from Google Api
     */
    public void registerCallbackHandler(IGoogleApiCallback callback) {
        googleApiCallbacksHandler.registerListener(callback);
    }

    /**
     * Unregisters listener from login controller
     *
     * @param callback instance of IGoogleApiCallback which handles callbacks from Google Api
     */
    public void unregisterCallbackHandler(IGoogleApiCallback callback) {
        googleApiCallbacksHandler.unregisterListener(callback);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        connectionFailedHandling(connectionResult);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnected(Bundle bundle) {

        connectionSuccessHandling();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionSuspended(int i) {
    }

    /**
     * Sends further the result of an attempt of connecting to google api
     * {@inheritDoc}
     *
     * @param requestCode  int listener code
     * @param responseCode int returned result
     * @param data         Intent returned data
     * @param RESULT_OK    int pattern successful result
     */
    @Override
    public void onGoogleApiActivityResult(int requestCode, int responseCode, Intent data, int RESULT_OK) {
        sendLoginResultToController(requestCode, responseCode, RESULT_OK);
    }

    /**
     * Tries to connect to google api when received response is ok and connection process hasn't been already started
     *
     * @param requestCode  int listener code
     * @param responseCode int returned result
     * @param RESULT_OK    int successful pattern result
     */
    void sendLoginResultToController(int requestCode, int responseCode, int RESULT_OK) {
        if (requestCode == Constants.RC_SIGN_IN && !googleApiClient.isConnecting() && responseCode == RESULT_OK) {
            googleApiClient.connect();
        }
    }

    /**
     * Called on failed connection with Google Plus Api
     * Handles reconnect trial.
     */
    void connectionFailedHandling(ConnectionResult connectionResult) {
        if (connectionResult != null && !connectionResult.hasResolution()) {
            GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
            int code = googleApiAvailability.isGooglePlayServicesAvailable(fragment.getContext());
            if (googleApiAvailability.isUserResolvableError(code)) {
                googleApiAvailability.getErrorDialog(fragment.getActivity(), code, Constants.RC_SIGN_IN).show();
            }
        } else {
            try {
                if (connectionResult != null) {
                    connectionResult.startResolutionForResult(fragment.getActivity(), Constants.RC_SIGN_IN);
                }
            } catch (IntentSender.SendIntentException e) {
                googlePlusLogin();
            }
        }
    }

    /**
     * Called on successful connection with Google Plus Api.
     * Checks in Shared Preferences if user is already logged in.
     * If not retrieves user's account name from Google Api Client'a, saves it in UserGoogleFitData,
     * set user as logged in Shared Preferences and changes fragment from login to main fragment.
     * If user already logged in only changes fragment from login to main fragment.
     */
    @SuppressWarnings("deprecation")
    void connectionSuccessHandling() {
        if (!googleApiClient.isConnected()) {
            googlePlusLogin();
        } else {
            if (SharedPreferencesController.getUserAccountName(fragment.getContext()).isEmpty()) {
                String accountName = Plus.AccountApi.getAccountName(googleApiClient);
                UserGoogleFitData.setAccountName(accountName);
                SharedPreferencesController.setUserAsLogged(fragment.getContext(), accountName);
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            view.changeFragment(fragment.getFragmentManager(), IBasicView.MAIN_VIEW_FRAGMENT);
        }
    }

    /**
     * Checks if user with given accountName is already logged in.
     *
     * @param accountName string with user's email defining the user's account
     * @return status if the user is logged in
     */
    private boolean checkIfAlreadyLoggedIn(String accountName) {
        boolean isLoggedIn = false;
        Context context = fragment.getContext();
        if (!accountName.isEmpty() && SharedPreferencesController.isUserLogged(context, accountName)) {
            isLoggedIn = true;
        }
        return isLoggedIn;
    }

    private void initialProgressDialog(Fragment fragment) {
        progressDialog = new ProgressDialog(fragment.getActivity());
        progressDialog.setMessage(fragment.getResources().getString(R.string.loginIn));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
    }
}


