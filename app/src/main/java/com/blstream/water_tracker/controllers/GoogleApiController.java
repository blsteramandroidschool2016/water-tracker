package com.blstream.water_tracker.controllers;


import android.app.Activity;

import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.plus.Plus;

public final class GoogleApiController {
    private static GoogleApiClient googleApiClient;
    private static GoogleApiCallbacksHandler callbacksHandler = new GoogleApiCallbacksHandler();

    /**
     * @return instance of GoogleApiClient
     */
    public static GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    /**
     * @return instance of GoogleApiCallbackHandler
     */
    public static GoogleApiCallbacksHandler getCallbacksHandler() {
        return callbacksHandler;
    }

    /**
     * Initializes google plus api client when user not logged
     */
    public static GoogleApiClient buildGPApiClientWhenNotLogged(Activity activity) {

        googleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(callbacksHandler)
                .addOnConnectionFailedListener(callbacksHandler)
                .addApi(Plus.API)
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.CONFIG_API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_BODY_READ_WRITE))
                .build();
        return googleApiClient;
    }

    /**
     * Initializes google plus api client when user already logged
     */
    public static GoogleApiClient buildGPApiClientWhenLogged(String accountName, Activity activity) {

        googleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(callbacksHandler)
                .addOnConnectionFailedListener(callbacksHandler)
                .addApi(Plus.API)
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.CONFIG_API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_BODY_READ_WRITE))
                .setAccountName(accountName)
                .build();
        return googleApiClient;
    }
}
