package com.blstream.water_tracker.internetConnection;

import java.util.Observable;

/**
 * Notifies observers about internet connection status change.
 */
public class InternetConnectionObserver extends Observable {

    private static InternetConnectionObserver instance = null;

    private InternetConnectionObserver() {
    }

    /**
     * @return singleton instance of InternetConnection observer
     */
    public static InternetConnectionObserver getInstance() {
        if (instance == null) {
            return instance = new InternetConnectionObserver();
        } else {
            return instance;
        }
    }

    /**
     * Notifying observers when internet connection status has changed.
     */
    public void notifyMyObservers() {
        this.setChanged();
        this.notifyObservers();
    }
}

