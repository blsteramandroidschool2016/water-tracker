package com.blstream.water_tracker.internetConnection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Listens to internet connection status change;
 */
public class InternetConnectionListener extends BroadcastReceiver {

    private static final String NET_CONNECTION_KEY = "android.net.conn.CONNECTIVITY_CHANGE";
    private static final String AIRPLANE_MODE_KEY = "android.intent.action.AIRPLANE_MODE";

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String intentAction = intent.getAction();
        if(intentAction.equals(NET_CONNECTION_KEY) || intentAction.equals(AIRPLANE_MODE_KEY)){
            InternetConnectionObserver.getInstance().notifyMyObservers();
        }
    }
}
