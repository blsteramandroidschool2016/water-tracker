package com.blstream.water_tracker.models;

import android.util.Log;

import java.text.NumberFormat;

/**
 * Calculates user's daily water requirement in liters based on user's weight and amount of burned
 * calories on that day.
 * The increase of burned calories during the day is updated on every launch/login of the application.
 * In this moment also weight and saved water intake are retrieved from Google Fit.
 * Afterwards daily water requirement is calculated and save on Google Fit.
 */
public class DailyWaterRequirement {

    private final static double WATER_REQUIREMENT_ML_FOR_KG = 42.9;
    private final static double WATER_REQUIREMENT_ML_FOR_KCAL = 1.5;
    private final static long ONE_KG = 1000;
    private static final String TAG = DailyWaterRequirement.class.getSimpleName();
    private float waterRequirementInL = 0.00f;

    /**
     * Calculates water requiment for given weight and burned calories
     * @param weight in kilograms
     * @param burnedKCalories in kcal
     * @return recommended water intake in liters
     */
    public static float calculateWaterRequirement(double weight, double burnedKCalories) {
        float waterRequirementInL = 0.0f;
        if (weight >= 0 && burnedKCalories >= 0) {
            double result = (((WATER_REQUIREMENT_ML_FOR_KG * weight) + (WATER_REQUIREMENT_ML_FOR_KCAL * burnedKCalories)) / ONE_KG);
            NumberFormat formatter = NumberFormat.getInstance();
            formatter.setMaximumFractionDigits(2);
            waterRequirementInL = Float.parseFloat(formatter.format(result).replace(",", "."));
        }
        return waterRequirementInL;
    }

    /**
     * Calculates user's daily water requirement in liters based on user's weight and amount of burned
     * calories on that day.
     *
     * @return user's daily water requirement
     */
    public float calculateWaterRequirement() {
        double weight = UserGoogleFitData.getUserWeight();
        double burnedKCalories = ((double) UserGoogleFitData.getBurnedCalories()) / ONE_KG;
        if (weight >= 0 && burnedKCalories >= 0) {
            double result = ((WATER_REQUIREMENT_ML_FOR_KG * weight + WATER_REQUIREMENT_ML_FOR_KCAL * burnedKCalories) / ONE_KG);
            NumberFormat formatter = NumberFormat.getInstance();
            formatter.setMaximumFractionDigits(2);
            waterRequirementInL = Float.parseFloat(formatter.format(result).replace(",", "."));
        }
        return waterRequirementInL;
    }
}
