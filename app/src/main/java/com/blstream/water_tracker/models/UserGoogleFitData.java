package com.blstream.water_tracker.models;

/**
 * Stores user's data retrieved from Google Fit
 */
public final class UserGoogleFitData {

    public static boolean isWeightRetrieved = false;
    public static boolean areCaloriesRetrieved = false;
    public static boolean isWaterIntakeRetrieved = false;
    public static boolean isWaterRequirementCalculated = false;
    private static String accountName = "";
    private static float weight = 0;
    private static long burnedCalories = -1;
    private static float waterIntake = 0;
    private static float waterRequirement = 0;

    /**
     * @return user's account name - email
     */
    public static String getAccountName() {
        return accountName;
    }

    /**
     * @param userAccountName user's account name - email
     */
    public static void setAccountName(String userAccountName) {
        accountName = userAccountName;
    }

    /**
     * @return user's weight
     */
    public static float getUserWeight() {
        return weight;
    }

    /**
     * @param userWeight user's weight
     */
    public static void setUserWeight(float userWeight) {
        weight = userWeight;
    }

    /**
     * @return user's burnt calories
     */
    public static long getBurnedCalories() {
        return burnedCalories;
    }

    /**
     * @param todaysBurnedCalories user's burnt calories
     */
    public static void setBurnedCalories(long todaysBurnedCalories) {
        burnedCalories = todaysBurnedCalories;
    }

    /**
     * @return user's current daily water intake
     */
    public static float getWaterIntake() {
        return waterIntake;
    }

    /**
     * @param userWaterIntake user's current daily water intake
     */
    public static void setWaterIntake(float userWaterIntake) {
        waterIntake = userWaterIntake;
    }

    /**
     * @return user's proposed daily water requirement
     */
    public static float getWaterRequirement() {
        return waterRequirement;
    }

    /**
     * @param userWaterRequirement user's proposed daily water requirement
     */
    public static void setWaterRequirement(float userWaterRequirement) {
        waterRequirement = userWaterRequirement;
    }

    public static void setAllFlagsFalse() {
        isWeightRetrieved = false;
        areCaloriesRetrieved = false;
        isWaterIntakeRetrieved = false;
        isWaterRequirementCalculated = false;
    }
}
