package com.blstream.water_tracker.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * DataSet retrieved from googleFit API
 */
public class GoogleFitData implements Parcelable {
    public static final Creator<GoogleFitData> CREATOR = new Creator<GoogleFitData>() {
        @Override
        public GoogleFitData createFromParcel(Parcel in) {
            return new GoogleFitData(in);
        }

        @Override
        public GoogleFitData[] newArray(int size) {
            return new GoogleFitData[size];
        }
    };
    private long time;
    private float weight;
    private long burnedCalories;
    private float waterIntake;

    public GoogleFitData(long burnedCalories, long time, float waterIntake, float weight) {
        this.burnedCalories = burnedCalories;
        this.time = time;
        this.waterIntake = waterIntake;
        this.weight = weight;
    }

    protected GoogleFitData(Parcel in) {
        time = in.readLong();
        weight = in.readFloat();
        burnedCalories = in.readLong();
        waterIntake = in.readFloat();
    }

    public long getBurnedCalories() {
        return burnedCalories;
    }

    public long getTime() {
        return time;
    }

    public float getWaterIntake() {
        return waterIntake;
    }

    public float getWeight() {
        return weight;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(time);
        dest.writeFloat(weight);
        dest.writeLong(burnedCalories);
        dest.writeFloat(waterIntake);
    }
}

