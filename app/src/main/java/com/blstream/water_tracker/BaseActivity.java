package com.blstream.water_tracker;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.blstream.water_tracker.controllers.GoogleApiController;
import com.blstream.water_tracker.view.fragments.LoginView;


/**
 * Handles loading LoginView fragment
 */
public abstract class BaseActivity extends AppCompatActivity {
    public final static String LOGIN_FRAGMENT_KEY = "LOGINFRAGMENTKEY";
    private static final int MY_PERMISSIONS_REQUEST_GET_ACCOUNTS = 1;
    private LoginView loginView;

    /**
     * Loads LoginView fragment.
     * <p/>
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager supportFragmentManager = getSupportFragmentManager();

        if (savedInstanceState != null) {
            loginView = (LoginView) supportFragmentManager.getFragment(savedInstanceState, LOGIN_FRAGMENT_KEY);
        } else if (loginView == null) {
            loginView = new LoginView();
            supportFragmentManager.beginTransaction().add(R.id.mainViewContainer, loginView).commit();
        }

        //If app do not have permissions to accounts. requestPermissions starts activity that can let user grant permission
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.GET_ACCOUNTS},
                    MY_PERMISSIONS_REQUEST_GET_ACCOUNTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        GoogleApiController.getCallbacksHandler().notifyAboutActivityResult(requestCode, resultCode, data, RESULT_OK);
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Check permissions, if permission was not granted App closes and show Toast
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_GET_ACCOUNTS) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, R.string.noPermissionMsg, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(android.R.id.home == item.getItemId()){
            getSupportFragmentManager().popBackStack();
        }
        return super.onOptionsItemSelected(item);
    }

}


