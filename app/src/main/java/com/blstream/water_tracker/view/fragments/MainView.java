package com.blstream.water_tracker.view.fragments;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.blstream.water_tracker.Constants;
import com.blstream.water_tracker.MainActivity;
import com.blstream.water_tracker.R;
import com.blstream.water_tracker.controllers.GoogleFitController;
import com.blstream.water_tracker.controllers.HistoricalDataManager;
import com.blstream.water_tracker.controllers.LogoutController;
import com.blstream.water_tracker.controllers.SharedPreferencesController;
import com.blstream.water_tracker.interfaces.DialogListener;
import com.blstream.water_tracker.interfaces.NoDataFoundListener;
import com.blstream.water_tracker.interfaces.OnWaterIntakeReceived;
import com.blstream.water_tracker.interfaces.OnWaterRequirementCalculated;
import com.blstream.water_tracker.interfaces.OnWeightSaved;
import com.blstream.water_tracker.interfaces.SwitchChangeStateListener;
import com.blstream.water_tracker.interfaces.waterSlider.OnValueChangedListener;
import com.blstream.water_tracker.interfaces.waterSlider.OnValueSetListener;
import com.blstream.water_tracker.models.UserGoogleFitData;
import com.blstream.water_tracker.view.controls.switcher.SwitchControl;
import com.blstream.water_tracker.view.controls.waterSliderControls.WaterSliderController;
import com.blstream.water_tracker.view.controls.waterSliderControls.components.LeftGraduationDrawer;
import com.google.android.gms.fitness.data.DataType;

import java.sql.Timestamp;


/**
 * Handles main views events.
 */
public class MainView extends BasicView implements OnWaterIntakeReceived, OnWaterRequirementCalculated, OnWeightSaved, NoDataFoundListener, DialogListener {

    public static final int DELAY_MILLIS = 100;
    private static final int COMMA_SHIFT = 10;
    private static final String STATE_KEY = "stateKey";
    private static final String DIALOG_FRAGMENT_NAME = "fragment_edit_name";
    private static final String RECOMMENDED_STATE_KEY = "recommended level";
    private static boolean isWaterStateSet = false;
    private SwitchControl switchControl;
    private LogoutController logoutController;
    private WaterSliderController waterSliderController;
    private TextView currentValueLitersTextView;
    private TextView currentValueCupsTextView;
    private TextView litersLabel;
    private TextView cupsLabel;
    private TextView recommendedValueLitersTextView;
    private TextView recommendedValueCupsTextView;
    private TextView recommendedLabelLitersTextView;
    private TextView recommendedLabelCupsTextView;
    private GoogleFitController googleFitController;
    private DataDialog dataDialog;
    private Timestamp timestamp;
    private String waterRequirement;
    private float waterIntake;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.main_view_layout, container, false);
    }

    /**
     * Registers listener in GoogleFirController.
     * <p/>
     * {@inheritDoc}
     */
    @Override
    public void onStart() {
        googleFitController.registerCallbackHandler(googleFitController);
        super.onStart();
    }

    @Override
    public void onResume() {
        if (timestamp != null) {
            googleFitController.reloadInitialGoogleFitData(timestamp);
        }
        if (waterRequirement != null) {
            setStateOnRecommendedCounter(Float.valueOf(waterRequirement));
            waterSliderController.setStateWithoutAnimation(waterIntake);
        }
        super.onResume();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(waterRequirement != null) {
            outState.putFloat(STATE_KEY + UserGoogleFitData.getAccountName(), waterIntake);
            outState.putString(RECOMMENDED_STATE_KEY + UserGoogleFitData.getAccountName(), waterRequirement);
        }
    }

    /**
     * Unregisters listener from GoogleFitController.
     * Sets the state of switch.
     * <p/>
     * {@inheritDoc}
     */
    @Override
    public void onStop() {
        googleFitController.unregisterCallbackHandler(googleFitController);
        SharedPreferencesController.setSwitchState(getContext(), switchControl.isChecked());
        super.onStop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.options_menu, menu);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            if (isInternetConnection((ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE))) {
                logoutController.logout(this.getContext(), UserGoogleFitData.getAccountName());
                return true;
            } else {
                showError(getContext(), getView(), YOU_CANNOT_LOG_OUT_WITHOUT_INTERNET);
                return false;
            }
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialControllers();
        initialWaterSliderController(view);
        isWaterStateSet = false;
        if (savedInstanceState != null) {
            dataDialog = (DataDialog) getActivity().getSupportFragmentManager()
                    .findFragmentByTag(DIALOG_FRAGMENT_NAME);
            if (dataDialog != null) {
                dataDialog.setConfirmListener(this);
            }
            if ((waterIntake = savedInstanceState.getFloat(STATE_KEY + UserGoogleFitData.getAccountName())) > 0) {
                waterSliderController.setStateWithoutAnimation(waterIntake);
                isWaterStateSet = true;
            }
            if ((waterRequirement = savedInstanceState.getString(RECOMMENDED_STATE_KEY + UserGoogleFitData.getAccountName())) != null && (!waterRequirement.equals(""))) {
                setStateOnRecommendedCounter(Float.valueOf(waterRequirement));
            }
        }if (waterRequirement == null) {
            googleFitController.retrieveInitialGoogleFitData();
        }
        initialToolbar();
        initSwitchControl(view);
        initialCalendarControl(view);
        switchControl.setChecked(SharedPreferencesController.getSwitchState(getContext()));
    }


    @Override
    public void onPause() {
        super.onPause();
        timestamp = new Timestamp(System.currentTimeMillis());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void waterIntakeReceived() {
        waterSliderController.setStateWithAnimation(UserGoogleFitData.getWaterIntake());
        isWaterStateSet = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void waterRequirementCalculated() {
        waterRequirement = Float.toString(UserGoogleFitData.getWaterRequirement());
        setStateOnRecommendedCounter(Float.valueOf(waterRequirement));
        UserGoogleFitData.setAllFlagsFalse();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDataNotFoundListener() {
        showEditDialog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onInputDataListener(int data) {

        isWaterStateSet = false;
        googleFitController.writeDataToGoogleFit(DataType.TYPE_WEIGHT, data);
        dataDialog.hideKeyboard();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dataDialog.getDialog().cancel();
            }
        }, DELAY_MILLIS);
    }

    @Override
    public void weightSaved() {
        if (!isWaterStateSet) {
            googleFitController.retrieveInitialGoogleFitData();
            isWaterStateSet = true;
        }
    }

    private void initialControllers() {
        logoutController = new LogoutController(this);
        googleFitController = new GoogleFitController(this, getActivity().getApplicationContext());
        googleFitController.onWaterIntakeReceivedListener = this;
        googleFitController.onWaterRequirementCalculatedListener = this;
        googleFitController.onWeightSavedListener = this;
        googleFitController.setNoDataFoundListener(this);
    }

    private void setStateOnRecommendedCounter(float req) {
        String text;
        float newState = req * COMMA_SHIFT;
        newState = Math.round(newState);
        newState = newState / COMMA_SHIFT;
        text = String.valueOf(newState);
        recommendedValueLitersTextView.setText(text);
        text = String.valueOf(((int) (req / LeftGraduationDrawer.CUP_SIZE)));
        recommendedValueCupsTextView.setText(text);
    }

    private void showEditDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        dataDialog = DataDialog.newInstance(getString(R.string.dialog_message));
        dataDialog.setCancelable(false);
        dataDialog.setConfirmListener(this);
        dataDialog.show(fm, DIALOG_FRAGMENT_NAME);
    }

    private void initialToolbar() {
        Toolbar myToolbar = (Toolbar) getActivity().findViewById(R.id.my_toolbar);
        ((MainActivity) getActivity()).setSupportActionBar(myToolbar);
        final ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    private void initialWaterSliderController(View view) {
        waterSliderController = (WaterSliderController) view.findViewById(R.id.waterSlider);
        waterSliderController.startWaitAnimation();
        currentValueLitersTextView = (TextView) view.findViewById(R.id.currentValueLiters);
        currentValueCupsTextView = (TextView) view.findViewById(R.id.currentValueCups);
        recommendedValueCupsTextView = (TextView) view.findViewById(R.id.recommendedValueGlasses);
        recommendedValueLitersTextView = (TextView) view.findViewById(R.id.recommendedValueLiters);
        recommendedLabelLitersTextView = (TextView) view.findViewById(R.id.textViewRecommendedLitersLabel);
        recommendedLabelCupsTextView = (TextView) view.findViewById(R.id.textViewRecommendedCupsLabel);
        litersLabel = (TextView) view.findViewById(R.id.textViewLitersLabel);
        cupsLabel = (TextView) view.findViewById(R.id.textViewCupsLabel);
        setStateOnMainCounter(waterSliderController.getState());
        waterSliderController.addOnValueChangedListener(new OnValueChangedListener() {
            /**
             * {@inheritDoc}
             */
            @Override
            public void onChangeValue(float state) {

                setStateOnMainCounter(state);
            }
        });
        waterSliderController.addOnValueSetListener(new OnValueSetListener() {
            /**
             * {@inheritDoc}
             */
            @Override
            public void onValueSet() {
                waterIntake = waterSliderController.getState();
                googleFitController.saveCustomDataToGoogleFit(waterIntake, Constants.WATER_INTAKE_DATA_TYPE,
                        Constants.WATER_INTAKE_FIELD, Constants.WATER_INTAKE_STREAM_NAME);
            }
        });
    }

    private void setStateOnMainCounter(float state) {
        String text;
        float newState = state * COMMA_SHIFT;
        newState = Math.round(newState);
        newState = newState / COMMA_SHIFT;
        text = String.valueOf(newState);
        currentValueLitersTextView.setText(text);
        text = String.valueOf(((int) (state / LeftGraduationDrawer.CUP_SIZE)));
        currentValueCupsTextView.setText(text);
    }

    private void initSwitchControl(View view) {
        switchControl = (SwitchControl) view.findViewById(R.id.switchControl);
        switchControl.setOnClickListener(new View.OnClickListener() {
            /**
             * {@inheritDoc}
             */
            @Override
            public void onClick(View v) {
                setLiters(switchControl.isChecked());
                setStateOnMainCounter(waterSliderController.getState());
            }
        });
        switchControl.addChangeStateListener(new SwitchChangeStateListener() {
            @Override
            public void onStateChange(float state) {
                float oppositeState = (state * -1) + 1;
                currentValueLitersTextView.setAlpha(state);
                currentValueCupsTextView.setAlpha(oppositeState);
                litersLabel.setAlpha(state);
                cupsLabel.setAlpha(oppositeState);
                recommendedValueLitersTextView.setAlpha(state);
                recommendedLabelLitersTextView.setAlpha(state);
                recommendedValueCupsTextView.setAlpha(oppositeState);
                recommendedLabelCupsTextView.setAlpha(oppositeState);
                waterSliderController.setAlphaState(state);
            }
        });
    }

    private void setLiters(boolean liters) {
        waterSliderController.setLiters(liters);
    }

    private void initialCalendarControl(View view) {
        View calendarControl = view.findViewById(R.id.calendarControl);
        calendarControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(getActivity().getSupportFragmentManager(), HISTORY_VIEW_FRAGMENT);
            }
        });
    }


}
