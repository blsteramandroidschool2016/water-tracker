package com.blstream.water_tracker.view.controls.chart.components;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.ChartDrawable;

/**
 * Work in progress
 */
public class HistogramBar implements ChartDrawable {

    private static final String TAG = HistogramBar.class.getSimpleName();
    private final int height;
    private int thickness;
    private int posX;
    private int posY;
    private int heightOfBump;
    private float maxValue;
    private float waterIntake;
    private float recommendedWaterintake;
    private Paint recommendedIntakePaint;
    private Paint actualIntakePaint;
    private Path path;

    public HistogramBar(View view, int posX, int posY, int thickness, int height, float maxValue, float waterIntake, float recommendedWaterintake) {
        this.posX = posX;
        this.posY = posY;
        this.thickness = thickness;
        this.heightOfBump = Utils.getHeightOfBump(thickness);
        this.height = height - heightOfBump;
        this.maxValue = maxValue;
        this.waterIntake = waterIntake;
        this.recommendedWaterintake = recommendedWaterintake;
        recommendedIntakePaint = new Paint();
        recommendedIntakePaint.setAntiAlias(true);
        recommendedIntakePaint.setColor(ContextCompat.getColor(view.getContext(), R.color.light_blue));
        actualIntakePaint = new Paint();
        actualIntakePaint.setAntiAlias(true);
        actualIntakePaint.setColor(ContextCompat.getColor(view.getContext(), R.color.slightly_dark_blue));
        path = new Path();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawPath(getPathForBar(posX, posY, recommendedWaterintake, thickness), recommendedIntakePaint);
        canvas.drawPath(getPathForBar(posX, posY, waterIntake, thickness), actualIntakePaint);
    }

    private Path getPathForBar(int posX, int posY, float value, int thickness) {
        path.reset();
        path.moveTo(posX, posY);
        RectF arcRect = new RectF(posX,
                posY - Utils.getPosYForValue(value, maxValue, height) - heightOfBump,
                posX + thickness,
                posY - Utils.getPosYForValue(value, maxValue, height) + heightOfBump);
        path.arcTo(arcRect, 180, 180);
        path.lineTo(posX + thickness, posY);
        return path;
    }


}
