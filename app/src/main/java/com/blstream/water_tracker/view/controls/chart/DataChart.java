package com.blstream.water_tracker.view.controls.chart;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.blstream.water_tracker.interfaces.HistoryChart;
import com.blstream.water_tracker.models.GoogleFitData;
import com.blstream.water_tracker.view.controls.chart.components.DivisionX;
import com.blstream.water_tracker.view.controls.chart.components.DivisionY;
import com.blstream.water_tracker.view.controls.chart.components.Histogram;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.Parser;

import java.util.List;

/**
 * Displays dataChart on canvas
 */
public class DataChart extends View implements HistoryChart {

    private static final int MATCH_PARENT = -1;
    private final boolean widthMatchParent;
    private final boolean heightMatchParent;
    private int width;
    private int height;
    private Histogram histogram;
    private DivisionY divisionY;
    private DivisionX divisionX;


    public DataChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        width = Parser.getWidth(attrs, metrics);
        height = Parser.getHeight(attrs, metrics);
        widthMatchParent = width == MATCH_PARENT;
        heightMatchParent = height == MATCH_PARENT;
        histogram = new Histogram(this);
        divisionY = new DivisionY(this);
        divisionX = new DivisionX(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setData(List<GoogleFitData> data) {
        histogram.setData(data);
        divisionY.setData(data);
        divisionX.setData(data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        histogram.draw(canvas);
        divisionY.draw(canvas);
        divisionX.draw(canvas);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (widthMatchParent) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }
        if (heightMatchParent) {
            height = MeasureSpec.getSize(heightMeasureSpec);
        }
        int paddingRatio = 20;
        int paddingLeft = width / paddingRatio;
        int paddingRight = width / paddingRatio;
        int paddingTop =  height / paddingRatio;
        int paddingBottom = height/ paddingRatio;
        int divisionYWidth = 100;
        int divisionXHeight = 100;
        int histogramStartX = paddingLeft + divisionYWidth;
        int histogramStartY = paddingTop;
        int histogramEndX = width - paddingRight;
        int histogramEndY = height - paddingBottom - divisionXHeight;
        histogram.updateValues(histogramStartX, histogramStartY, histogramEndX, histogramEndY);
        int fontSize = height / 30;
        int valuePadding = divisionXHeight / 2;
        divisionY.updateValues(paddingRight + divisionYWidth, histogramStartX, histogramStartY, histogramEndX, histogramEndY, valuePadding, fontSize);
        divisionX.updateValues(histogramEndY, histogramStartX, histogramEndX, valuePadding, fontSize);
        setMeasuredDimension(width, height);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
