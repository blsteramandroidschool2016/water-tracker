package com.blstream.water_tracker.view.fragments;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.blstream.water_tracker.MainActivity;
import com.blstream.water_tracker.R;
import com.blstream.water_tracker.controllers.HistoricalDataManager;
import com.blstream.water_tracker.controllers.HistoryDataAdapter;
import com.blstream.water_tracker.models.GoogleFitData;

import java.util.ArrayList;
import java.util.List;

/**
 * Work in progress
 */
public class HistoryView extends BasicView {
    private static final String TAG = HistoryView.class.getSimpleName();
    private static final String LIST_KEY = "data_list";
    private static final String TASK_STATE = "task_state";
    private ProgressDialog mDialog;

    private HistoryDataAdapter adapter;
    private HistoryDataDownloader historyDataDownloader;


    /**
     * AsyncTask that downloads Historical data and sets it in HistoryDataAdapter
     */
    private class HistoryDataDownloader extends AsyncTask<Void, Void, List<GoogleFitData>> {


        /**
         * {@inheritDoc}
         */
        @Override
        protected List<GoogleFitData> doInBackground(Void... params) {
            return new HistoricalDataManager().getAllHistoricalData();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = ProgressDialog.show(getContext(),
                    getContext().getString(R.string.history_data_progress_dialog_title),
                    getContext().getString(R.string.history_data_progress_dialog_title),
                    false, false);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void onPostExecute(List<GoogleFitData> googleFitData) {
            adapter.setDataList(googleFitData);
            if ((mDialog != null) && mDialog.isShowing()) {
                mDialog.dismiss();
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.history_screen_layout, container, false);
    }

    /**
     * Saves list of googleFitData
     *
     * @param outState to be data saved in
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(LIST_KEY, (ArrayList<? extends Parcelable>) adapter.getData());
        outState.putBoolean(TASK_STATE, isTaskRunning());
        cancelDownloader();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelDownloader();


    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu_history_view, menu);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: selected: " + item.getItemId());
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d(TAG, "onOptionsItemSelected: home pressed");
                getActivity().getSupportFragmentManager().popBackStack();
            case R.id.chart:
                changeChartFragment(getActivity().getSupportFragmentManager(), adapter.getData());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);

        initialActionBar();
        initialRecyclerVIew(view, savedInstanceState);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPause() {
        super.onPause();
        if ((mDialog != null) && mDialog.isShowing())
            mDialog.dismiss();
        mDialog = null;
    }

    private void cancelDownloader() {
        if (isTaskRunning()) {
            historyDataDownloader.cancel(true);
        }
    }

    private void initialRecyclerVIew(View view, Bundle saveInstanceState) {
        adapter = new HistoryDataAdapter(getContext());
        RecyclerView recList = (RecyclerView) view.findViewById(R.id.recyclerView);
        recList.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        historyDataDownloader = new HistoryDataDownloader();

        List<GoogleFitData> googleFitData;
        if (saveInstanceState != null) {
            googleFitData = saveInstanceState.getParcelableArrayList(LIST_KEY);
            adapter.setDataList(googleFitData);
        } else {
            googleFitData = new ArrayList<>();
        }
        if (googleFitData != null && googleFitData.isEmpty()) {
            historyDataDownloader.execute();
        }
    }

    private boolean isTaskRunning() {
        return (historyDataDownloader != null) && (historyDataDownloader.getStatus() == AsyncTask.Status.RUNNING);
    }

    private void initialActionBar() {
        Toolbar myToolbar = (Toolbar) getActivity().findViewById(R.id.my_toolbar);
        ((MainActivity) getActivity()).setSupportActionBar(myToolbar);
        final ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setShowHideAnimationEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.dark_blue)));
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(R.string.historicalDataLabel);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


}
