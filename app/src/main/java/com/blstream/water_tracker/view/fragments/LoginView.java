package com.blstream.water_tracker.view.fragments;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.blstream.water_tracker.BaseActivity;
import com.blstream.water_tracker.R;
import com.blstream.water_tracker.controllers.LoginController;
import com.google.android.gms.common.SignInButton;

import java.util.Observable;

/**
 * Handles Login screen events.
 */
public class LoginView extends BasicView {

    private LoginController loginController;
    private SignInButton signInButton;

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.login_layout, container, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginController = new LoginController(this);
        customizeButtons(view);
        update(null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Observable observable, Object data) {
        super.update(observable, data);
        if (!isInternetConnection((ConnectivityManager) getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE))) {
            signInButton.setClickable(false);
        } else {
            signInButton.setClickable(true);
        }
    }

    /**
     * Registers listener in LoginController.
     * <p/>
     * {@inheritDoc}
     */
    @Override
    public void onStart() {
        super.onStart();
        loginController.registerCallbackHandler(loginController);
    }

    /**
     * Unregisters listener from LoginController.
     * <p/>
     * {@inheritDoc}
     */
    @Override
    public void onStop() {
        super.onStop();
        loginController.unregisterCallbackHandler(loginController);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getActivity().getSupportFragmentManager().putFragment(outState, BaseActivity.LOGIN_FRAGMENT_KEY, this);
    }

    private void customizeButtons(View view) {
        signInButton = (SignInButton) view.findViewById(R.id.sign_in_button);
        signInButton.setColorScheme(SignInButton.COLOR_AUTO);
        signInButton.setOnClickListener(new View.OnClickListener() {
            /**
             *{@inheritDoc}
             */
            @Override
            public void onClick(View v) {
                if (isInternetConnection((ConnectivityManager) getContext()
                        .getSystemService(Context.CONNECTIVITY_SERVICE))) {
                    loginController.googlePlusLogin();
                } else {
                    showError(getContext(), getView(), YOU_CANNOT_LOG_IN_WITHOUT_INTERNET);
                }

            }
        });
        Button noThanksButton = (Button) view.findViewById(R.id.no_thanks_button);
        noThanksButton.playSoundEffect(android.view.SoundEffectConstants.CLICK);
        noThanksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                loginController.handleNoThanks(activity);
                Toast.makeText(getContext(), R.string.youNeedToLogin, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
