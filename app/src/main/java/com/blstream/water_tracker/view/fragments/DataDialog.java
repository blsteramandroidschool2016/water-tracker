package com.blstream.water_tracker.view.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.DialogListener;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.DialogValidator;

/**
 * Shows when user's weight is missing.
 * Provide possibility to insert missing data.
 */
public class DataDialog extends DialogFragment {
    public static final int DELAY_MILLIS = 1000;
    private static final String TITLE_KEY = "title";
    private DialogListener confirmListener;
    private int data;
    private EditText dataEditText;
    private View view;


    /**
     * Creates instance of DataDialog with specified title
     *
     * @param title to be set
     * @return new DataDialog instance
     */
    public static DataDialog newInstance(String title) {
        DataDialog frag = new DataDialog();
        Bundle args = new Bundle();
        args.putString(TITLE_KEY, title);
        frag.setArguments(args);
        return frag;
    }

    /**
     * Sets listener on DataDialog object
     *
     * @param confirmListener listener to be set
     */
    public void setConfirmListener(DialogListener confirmListener) {
        this.confirmListener = confirmListener;
    }

    /**
     * Creates new alert dialog with custom layout
     *
     * @param savedInstanceState saved state
     * @return new alert dialog
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        view = View.inflate(getContext(), R.layout.dialog_fragment_layout, null);
        return new AlertDialog.Builder(getActivity())
                .setIcon(R.mipmap.ic_launcher)
                .setTitle(getString(R.string.dialog_title))
                .setView(view)
                .setCancelable(false)
                .create();
    }

    /**
     * Sets initial controls and invokes validator
     */
    @Override
    public void onStart() {
        super.onStart();
        initControls();
        DialogValidator.dataValid(dataEditText);
    }

    /**
     * Hides keyboard from view
     */
    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    protected View.OnClickListener cancelButtonListener() {
        return new View.OnClickListener() {
            /**
             * Cancels dialog
             * {@inheritDoc}
             */
            @Override
            public void onClick(View v) {
                hideKeyboard();
                cancelButtonActions();
            }

        };
    }

    protected void cancelButtonActions() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDialog().cancel();
                getActivity().finish();
            }
        }, DELAY_MILLIS);
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    protected View.OnClickListener okButtonListener() {
        return new View.OnClickListener() {
            /**
             * Parses data to number and listens if data was sent
             *
             * {@inheritDoc}
             */
            @Override
            public void onClick(View v) {
                boolean isEmptyText = dataEditText.getText().length() == 0;
                if (!isEmptyText) {
                    data = Integer.parseInt(String.valueOf(dataEditText.getText()));
                    confirmListener.onInputDataListener(data);
                }
            }
        };
    }

    private void initControls() {
        dataEditText = (EditText) view.findViewById(R.id.dataEditText);
        Button okButton = (Button) view.findViewById(R.id.okButton);
        Button cancelButton = (Button) view.findViewById(R.id.cancelButton);
        okButton.setOnClickListener(okButtonListener());
        cancelButton.setOnClickListener(cancelButtonListener());
    }


}
