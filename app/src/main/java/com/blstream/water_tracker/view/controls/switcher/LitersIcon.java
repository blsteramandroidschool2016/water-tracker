package com.blstream.water_tracker.view.controls.switcher;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.SwitchDrawable;

/**
 * Holds and generates path in shape of Rectangle
 */
class LitersIcon implements SwitchDrawable {

    private final static float SHORTER_RECTANGLE_LENGTH = 0.6f;
    private View view;
    private int startX;
    private int startY;
    private int size;
    private Paint whiteLitersPaint;
    private Paint blueLitersPaint;
    private int squareSize;
    private float state;

    private Path rectanglePath = new Path();

    /**
     * Sets init values and liter objects' paints
     *
     * @param view   to be drawn on
     * @param startX X coordinate of icon
     * @param startY Y coordinate of icon
     * @param size   of icon
     */
    public LitersIcon(View view, int startX, int startY, int size) {
        this.view = view;
        this.startX = startX;
        this.startY = startY;
        this.size = size;
        this.squareSize = size / 5;
        setWhiteLitersPaint();
        setBlueLitersPaint();
        this.state = 0;
        setState(state);
    }

    private void setWhiteLitersPaint() {
        int whiteColor = ContextCompat.getColor(view.getContext(), R.color.white);
        whiteLitersPaint = new Paint();
        whiteLitersPaint.setColor(whiteColor);
        whiteLitersPaint.setStyle(Paint.Style.FILL);
    }

    private void setBlueLitersPaint() {
        int blueColor = ContextCompat.getColor(view.getContext(), R.color.slightly_dark_blue);
        blueLitersPaint = new Paint();
        blueLitersPaint.setColor(blueColor);
        blueLitersPaint.setStyle(Paint.Style.FILL);
    }


    private void drawRectangle(Canvas canvas, int startX, int startY, int width, int height, Paint paint) {
        rectanglePath.reset();
        rectanglePath.moveTo(startX, startY);
        rectanglePath.rLineTo(width, 0);
        rectanglePath.rLineTo(0, height);
        rectanglePath.rLineTo(-width, 0);
        rectanglePath.rLineTo(0, -width);
        canvas.drawPath(rectanglePath, paint);
    }

    /**
     * Gets state of control
     */
    @Override
    public float getState() {
        return state;
    }

    /**
     * Sets state of control
     *
     * @param state values from 0.0 to 1.0, where 0.0 - unchecked, 1.0 - checked
     */
    @Override
    public void setState(float state) {
        this.state = state;
        int alpha = (int) (state * 255);
        whiteLitersPaint.setAlpha(alpha);
    }

    /**
     * Draws object on specified canvas
     *
     * @param canvas to be drawn on
     */
    @Override
    public void draw(Canvas canvas) {
        drawRectangles(canvas, blueLitersPaint);
        drawRectangles(canvas, whiteLitersPaint);

    }

    private void drawRectangles(Canvas canvas, Paint paint) {
        drawRectangle(canvas, startX, startY, size, squareSize, paint);
        drawRectangle(canvas, startX, startY + 2 * squareSize, (int) (size * SHORTER_RECTANGLE_LENGTH), squareSize, paint);
        drawRectangle(canvas, startX, startY + 4 * squareSize, size, squareSize, paint);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Animation getNewAnimation(float newState) {
        return new SwitchAnimation(view, this, newState);
    }
}
