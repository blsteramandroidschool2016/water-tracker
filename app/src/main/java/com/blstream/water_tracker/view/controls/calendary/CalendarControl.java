package com.blstream.water_tracker.view.controls.calendary;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.blstream.water_tracker.R;


/**
 * Holds on calendars events and draw calendar icon.
 */
public class CalendarControl extends View {
    private static final float BORDER_PADDING_RATIO = 0.066f;
    private static final float STROKE_WIDTH_RATIO = 0.066f;
    private static final float WHITE_HOLDERS_STROKE_RATIO = 0.125f;
    private static final float CHECKER_STROKE_WIDTH_RATIO = 0.133f;
    private static final float BOTTOM_FILED_RECTANGLE_RATIO = 0.24f;
    private static final float CHECK_MARK_SECOND_LINE_X_RATIO = 0.44f;
    private static final float CHECK_MARK_SECOND_LINE_Y_RATIO = 0.744f;
    private static final float CHECK_MARK_SECOND_LINE_FINISH_X_RATIO = 0.31f;
    private static final float CHECK_MARK_SECOND_LINE_FINISH_Y_RATIO = 0.62f;
    private static final float CHECK_MARK_FIRST_LINE_X_START_RATIO = 0.688f;
    private static final float CHECK_MARK_FIRST_LINE_Y_START_RATIO = 0.488f;
    private static final float WIDTH_FIRST_OVAL_RATIO = 0.2f;
    private static final int TOP_FIRST_OVAL_RATIO = 0;
    private static final float RIGHT_FIRST_OVAL_RATIO = 0.355f;
    private static final float BOTTOM_FIRST_OVAL_RATIO = 0.177f;
    private static final float WIDTH_FIRST_HOLDER_RATIO = 0.244f;
    private static final float RIGHT_FIRST_HOLDER_RATIO = 0.31f;
    private static final int TOP_FIRST_HOLDER_RATIO = 0;
    private static final float BOTTOM_FIRST_HOLDER_RATIO = 0.139f;
    private static final float WIDTH_SECOND_OVAL_RATIO = 0.645f;
    private static final int TOP_SECOND_OVAL_RATIO = 0;
    private static final float RIGHT_SECOND_OVAL_RATIO = 0.8f;
    private static final float BOTTOM_SECOND_OVAL_RATIO = 0.177f;
    private static final float WIDTH_SECOND_HOLDER_RATIO = 0.69f;
    private static final int TOP_SECOND_HOLDER_RATIO = 0;
    private static final float RIGHT_SECOND_HOLDER_RATIO = 0.756f;
    private static final float BOTTOM_SECOND_HOLDER_RATIO = 0.139f;
    private float BORDER_PADDING;
    private float STROKE_WIDTH;
    private float CHECKER_STROKE_WIDTH;
    private float WHITE_HOLDERS_STROKE;
    private static final float RADIUS = 5f;
    private int layoutWidth;
    private int layoutHeight;
    private Paint borderPaint;
    private Paint fillPaint;
    private Paint holdersPaint;
    private Path checkMarkPath;

    /**
     * {@inheritDoc}
     */
    public CalendarControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        borderPaint = new Paint();
        borderPaint.setColor(ContextCompat.getColor(context, R.color.slightly_dark_blue));
        borderPaint.setAntiAlias(true);
        borderPaint.setStyle(Paint.Style.STROKE);
        fillPaint = new Paint();
        fillPaint.setColor(ContextCompat.getColor(context, R.color.slightly_dark_blue));
        fillPaint.setAntiAlias(true);
        fillPaint.setStyle(Paint.Style.FILL);
        fillPaint.setStrokeWidth(STROKE_WIDTH);
        holdersPaint = new Paint();
        holdersPaint.setColor(ContextCompat.getColor(context, R.color.white));
        holdersPaint.setAntiAlias(true);
        holdersPaint.setStyle(Paint.Style.FILL);
        holdersPaint.setStrokeWidth(WHITE_HOLDERS_STROKE);
        checkMarkPath = new Path();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        layoutWidth = MeasureSpec.getSize(widthMeasureSpec);
        layoutHeight = MeasureSpec.getSize(heightMeasureSpec);
        BORDER_PADDING = layoutHeight * BORDER_PADDING_RATIO;
        STROKE_WIDTH = layoutWidth * STROKE_WIDTH_RATIO;
        WHITE_HOLDERS_STROKE = layoutWidth * WHITE_HOLDERS_STROKE_RATIO;
        CHECKER_STROKE_WIDTH = layoutWidth * CHECKER_STROKE_WIDTH_RATIO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawRectangleBorders(canvas);
        drawFilledRectangle(canvas);
        drawHolders(canvas);
        drawCheckMark(canvas);
    }

    private void drawRectangleBorders(Canvas canvas) {
        borderPaint.setStrokeWidth(STROKE_WIDTH);
        float leftX = BORDER_PADDING;
        float topY = BORDER_PADDING;
        float rightX = layoutWidth - (STROKE_WIDTH / 2);
        float bottomY = layoutHeight - (STROKE_WIDTH / 2);
        RectF ovalBounds = new RectF(leftX, topY, rightX, bottomY);
        canvas.drawRoundRect(ovalBounds, RADIUS, RADIUS, borderPaint);
    }

    private void drawFilledRectangle(Canvas canvas) {
        float leftX = BORDER_PADDING;
        float topY = BORDER_PADDING;
        float rightX = layoutWidth - (STROKE_WIDTH / 2);
        float bottomY = layoutHeight * BOTTOM_FILED_RECTANGLE_RATIO;
        RectF ovalBounds = new RectF(leftX, topY, rightX, bottomY);
        canvas.drawRoundRect(ovalBounds, RADIUS, RADIUS, fillPaint);
    }


    private void drawCheckMark(Canvas canvas) {
        borderPaint.setStrokeWidth(CHECKER_STROKE_WIDTH);
        canvas.drawPath(getCheckMarkPath(), borderPaint);
    }

    private Path getCheckMarkPath() {
        checkMarkPath.reset();
        float xStart = getCheckMarkStartXCoordinates(layoutWidth);
        float yStart = getCheckMarkStartYCoordinates(layoutHeight);
        checkMarkPath.moveTo(xStart, yStart);
        checkMarkPath.lineTo(layoutWidth * CHECK_MARK_SECOND_LINE_X_RATIO, layoutHeight * CHECK_MARK_SECOND_LINE_Y_RATIO);
        checkMarkPath.lineTo(layoutWidth * CHECK_MARK_SECOND_LINE_FINISH_X_RATIO, layoutHeight * CHECK_MARK_SECOND_LINE_FINISH_Y_RATIO);
        return checkMarkPath;
    }

    private float getCheckMarkStartXCoordinates(int layoutWidth) {
        return layoutWidth * CHECK_MARK_FIRST_LINE_X_START_RATIO;
    }

    private float getCheckMarkStartYCoordinates(int layoutHeight) {
        return layoutHeight * CHECK_MARK_FIRST_LINE_Y_START_RATIO;
    }

    private void drawHolders(Canvas canvas) {
        canvas.drawRoundRect(getOvalBounds(WIDTH_FIRST_OVAL_RATIO, TOP_FIRST_OVAL_RATIO, RIGHT_FIRST_OVAL_RATIO, BOTTOM_FIRST_OVAL_RATIO)
                , RADIUS, RADIUS, holdersPaint);
        canvas.drawRoundRect(getOvalBounds(WIDTH_FIRST_HOLDER_RATIO, TOP_FIRST_HOLDER_RATIO, RIGHT_FIRST_HOLDER_RATIO, BOTTOM_FIRST_HOLDER_RATIO)
                , RADIUS, RADIUS, fillPaint);
        canvas.drawRoundRect(getOvalBounds(WIDTH_SECOND_OVAL_RATIO, TOP_SECOND_OVAL_RATIO, RIGHT_SECOND_OVAL_RATIO, BOTTOM_SECOND_OVAL_RATIO)
                , RADIUS, RADIUS, holdersPaint);
        canvas.drawRoundRect(getOvalBounds(WIDTH_SECOND_HOLDER_RATIO, TOP_SECOND_HOLDER_RATIO, RIGHT_SECOND_HOLDER_RATIO, BOTTOM_SECOND_HOLDER_RATIO)
                , RADIUS, RADIUS, fillPaint);
    }

    private RectF getOvalBounds(float widthRatio, float topRatio, float rightRatio, float bottomRatio) {
        float left = layoutWidth * widthRatio;
        float top = layoutHeight * topRatio;
        float right = layoutWidth * rightRatio;
        float bottom = layoutHeight * bottomRatio;
        return new RectF(left, top, right, bottom);
    }
}
