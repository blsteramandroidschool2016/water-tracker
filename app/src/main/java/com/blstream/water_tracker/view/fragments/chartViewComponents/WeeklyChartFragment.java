package com.blstream.water_tracker.view.fragments.chartViewComponents;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.models.GoogleFitData;
import com.blstream.water_tracker.view.fragments.BasicView;
import com.blstream.water_tracker.view.fragments.HistoryView;

import java.util.ArrayList;
import java.util.List;

/**
 * Work in progress
 */
public class WeeklyChartFragment extends Fragment {

    private static final int WEEKS_PER_CHART = 4;
    private static int WEEK_LEGTH = 7;

    public WeeklyChartFragment() {
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.weekly_chart_layout, container, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.weeklyTabViewPager);
        setupViewPager(viewPager, getArguments());
    }

    private void setupViewPager(ViewPager viewPager, Bundle args) {
        List<GoogleFitData> data = getArguments().getParcelableArrayList(BasicView.CHART_FRAGMENT_ARGUMENT_KEY);
        data = getWeeklyData(data);
        List<List<GoogleFitData>> listsForFragments = chopList(data, WEEKS_PER_CHART);
        android.support.v4.app.FragmentManager manager = getChildFragmentManager();
        ChartViewPagerAdapter adapter = new ChartViewPagerAdapter(manager);
        for (int i = 0; i < listsForFragments.size(); i++) {
            WeeklyChart weeklyChart = new WeeklyChart();
            Bundle chartArgs = new Bundle();
            chartArgs.putParcelableArrayList(HistoryView.CHART_FRAGMENT_ARGUMENT_KEY, (ArrayList<? extends Parcelable>) listsForFragments.get(i));
            weeklyChart.setArguments(chartArgs);
            adapter.addFragment(weeklyChart, String.valueOf(i));
        }
        viewPager.setAdapter(adapter);
    }

    private List<GoogleFitData> getWeeklyData(List<GoogleFitData> data) {
        List<List<GoogleFitData>> choopped = chopList(data, WEEK_LEGTH);
        List<GoogleFitData> result = new ArrayList<>();
        for (int i = 0; i < choopped.size(); i++) {
            result.add(getRoundedGoogleFitData(choopped.get(i)));
        }
        return result;
    }

    private GoogleFitData getRoundedGoogleFitData(List<GoogleFitData> week) {
        long time = week.get(0).getTime();
        float weight = 0;
        long burnedCalories = 0;
        float waterIntake = 0;
        int size = week.size();
        for (int j = 0; j < size; j++) {
            weight += week.get(j).getWeight();
            burnedCalories += week.get(j).getBurnedCalories();
            waterIntake += week.get(j).getWaterIntake();
        }
        return new GoogleFitData(
                burnedCalories / size,
                time,
                waterIntake / size,
                weight / size
        );

    }

    private <T> List<List<T>> chopList(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                            list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }
}
