package com.blstream.water_tracker.view.controls.chart.components;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.ChartDrawable;
import com.blstream.water_tracker.interfaces.HistoryChart;
import com.blstream.water_tracker.models.GoogleFitData;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Work in progress
 */
public class DivisionX implements ChartDrawable, HistoryChart {
    private static final String DATE_FORMAT = "dd/MM";
    private List<GoogleFitData> data;
    private Paint circlePaint;
    private int numberBars;
    private int histogramStartX;
    private int histogramEndX;
    private int startY;
    private int valuePadding;

    public DivisionX(View view) {
        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(ContextCompat.getColor(view.getContext(), R.color.black));
    }

    @Override
    public void draw(Canvas canvas) {
        for (int i = 0; i < numberBars; i++) {
            int barPosX = Utils.getBarPosX(numberBars, i, histogramStartX, histogramEndX);
            int barThickness = Utils.getBarThickness(numberBars, histogramStartX, histogramEndX);
            int circleRadius = barThickness / 8;
            int circleY = startY + valuePadding;
            if (i == 0 || i == numberBars - 1) {
                String text = convertToGracefulDateFormat(data.get(i).getTime());
                int textPosX = (int) (barPosX + (barThickness / 2) - (circlePaint.measureText(text) / 2));
                int textY = circleY + circleRadius * 2 ;
                canvas.drawText(text, textPosX, textY + circlePaint.getTextSize(), circlePaint);
            } else {
                circleRadius = barThickness / 13;
            }
            canvas.drawCircle(barPosX + (barThickness / 2), circleY, circleRadius, circlePaint);
        }
    }

    @Override
    public void setData(List<GoogleFitData> data) {
        this.data = data;
        numberBars = data.size();
    }

    public void updateValues(int startY, int histogramStartX, int histogramEndX, int valuePadding, float fontSize) {
        this.startY = startY;
        this.histogramStartX = histogramStartX;
        this.histogramEndX = histogramEndX;
        this.valuePadding = valuePadding;

        circlePaint.setTextSize(fontSize);
    }

    private String convertToGracefulDateFormat(long time) {
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, java.util.Locale.getDefault());
        return simpleDateFormat.format(date);
    }

}
