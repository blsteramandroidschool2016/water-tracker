package com.blstream.water_tracker.view.controls.waterSliderControls.components;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.waterSlider.IWaterSliderControls;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.MainControlHelper;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.Parser;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.StateChangeObserver;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.TouchReleaseObserver;

/**
 * Used to handle slider events.
 */
public class CircleView extends View implements IWaterSliderControls {

    private static final int START_COORDINATES_Y = 0;
    private static final float CIRCLE_RATIO = 11.5f;
    private static final int STROKE_WIDTH = 4;
    private static final int ARROW_COORDINATES_RATIO = 4;
    private static final int ARROW_Y_COORDINATES_RATIO = 8;
    private static final int ARROW_X_COORDINATE_RATIO = 6;
    private static final int TOUCH_MODE_IDLE = 0;
    private static final int TOUCH_MODE_DRAGGING = 1;
    private static final int TOUCH_MODE_DOWN = 2;
    private float state;
    private int posY;
    private float maxValue;
    private Paint circleBgr;
    private int layoutWidth;
    private int layoutHeight;
    private float mTouchY;
    private int mTouchMode;
    private Paint color = new Paint();
    private Path upperArrowPath = new Path();
    private Path lowerArrowPath = new Path();
    private StateChangeObserver observer;
    private TouchReleaseObserver touchReleaseObserver;
    private float circleRadius;
    private float padding;
    private float mTouchX;
    private float mTouchSlop;

    /**
     * {@inheritDoc}
     */
    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.maxValue = 2.0f;
        this.observer = StateChangeObserver.getInstance();
        this.touchReleaseObserver = TouchReleaseObserver.getInstance();
        initialColors(context);
        setStartStateOfControl(attrs);
        final ViewConfiguration config = ViewConfiguration.get(context);
        mTouchSlop = config.getScaledTouchSlop();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public float getState() {
        return state;
    }

    /**
     * invalidate canvas after set new state.
     * {@inheritDoc}
     */
    @Override
    public void setState(float state) {
        this.state = state;
        updateState();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                processDownAction(ev);
                break;

            case MotionEvent.ACTION_MOVE:
                if (processMoveAction(ev)) {
                    return true;
                }
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (mTouchMode == TOUCH_MODE_DRAGGING) {
                    touchReleaseObserver.notifyMyObservers();
                }
                break;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
        updateState();
    }

    /**
     * @return maximum value of state.
     */
    public float getMaxValue() {
        return maxValue;
    }

    /**
     * @param layoutWidth value of layout width
     * @return circle radius size;
     */
    public float calculateCircleRadius(int layoutWidth) {
        return layoutWidth / CIRCLE_RATIO;
    }

    /**
     * Sets padding for circle (to prevent draging circle out of screen)
     *
     * @param padding padding
     */
    public void setPadding(float padding) {
        this.padding = padding;
    }

    /**
     * @return return the Y coordinates of controls
     */
    public float getYCircleCoordinates() {
        return posY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawCircle(canvas, getXCircleCoordinates(layoutWidth),
                getYCircleCoordinates(),
                circleRadius);
        drawArrows(canvas);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        layoutWidth = MeasureSpec.getSize(widthMeasureSpec);
        layoutHeight = MeasureSpec.getSize(heightMeasureSpec);
        int layoutSize = layoutWidth > layoutHeight ? layoutHeight : layoutWidth;
        if (circleRadius == 0) {
            circleRadius = calculateCircleRadius(layoutSize);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * @param layoutWidth value of layout width
     * @return return the X coordinates of controls
     */
    protected float getXCircleCoordinates(int layoutWidth) {
        return (layoutWidth / 2);
    }

    /**
     * @param x           coordinates of user touch.
     * @param y           coordinates of user touch.
     * @param layoutWidth value of layout width.
     * @return true if we touched circle control, false if not.
     */
    public boolean hitThumb(float x, float y, int layoutWidth) {
        float xCoordinates = getXCircleCoordinates(layoutWidth);
        float yCoordinates = getYCircleCoordinates();
        final float thumbLeft = xCoordinates - circleRadius;
        final float thumbRight = xCoordinates + circleRadius;
        final float thumbTop = yCoordinates - circleRadius;
        final float thumbBottom = yCoordinates + circleRadius;
        boolean isHorizontallyTouchedInsideCircle = x > thumbLeft && x < thumbRight;
        boolean isVerticallyTouchedInsideCircle = y > thumbTop && y < thumbBottom;
        return isHorizontallyTouchedInsideCircle & isVerticallyTouchedInsideCircle;
    }

    /**
     * @param circleYCoordinates circle Y Coordinates.
     * @param circleRadius       value of circle radius.
     * @return start of Y coordinates of lower arrow used to draw on control .
     */
    protected float getLowerArrowStartYCoordinates(float circleYCoordinates, float circleRadius) {
        float shiftFromBorderYValueOfCircle = circleRadius / ARROW_Y_COORDINATES_RATIO;
        return circleYCoordinates + shiftFromBorderYValueOfCircle;
    }

    /**
     * @param circleXCoordinates circle X Coordinates.
     * @param circleRadius       value of circle radius.
     * @return start of X coordinates of arrow used to draw on control .
     */
    protected float getArrowStartXCoordinates(float circleXCoordinates, float circleRadius) {
        float shiftFromBorderXValueOfCircle = circleRadius / ARROW_X_COORDINATE_RATIO;
        return circleXCoordinates - shiftFromBorderXValueOfCircle;
    }

    /**
     * @param circleYCoordinates circle X Coordinates.
     * @param circleRadius       value of circle radius.
     * @return start of Y coordinates of upper arrow used to draw on control .
     */
    protected float getUpperArrowStartYCoordinates(float circleYCoordinates, float circleRadius) {
        float shiftFromBorderYValueOfCircle = circleRadius / ARROW_Y_COORDINATES_RATIO;
        return circleYCoordinates - shiftFromBorderYValueOfCircle;
    }

    private void updateState() {
        posY = MainControlHelper.getPosYpx(state, maxValue, layoutHeight, (int) padding * 2);
        invalidate();
    }

    private void initialColors(Context context) {
        this.circleBgr = new Paint();
        this.circleBgr.setColor(ContextCompat.getColor(context, R.color.slightly_dark_blue));
        this.circleBgr.setAntiAlias(true);
        this.color.setColor(Color.WHITE);
        this.color.setAntiAlias(true);
        this.color.setStrokeWidth(STROKE_WIDTH);
        this.color.setStyle(Paint.Style.STROKE);
    }

    private void drawCircle(Canvas canvas, float x, float y, float radius) {
        canvas.drawCircle(x, y, radius, circleBgr);
    }

    private void drawArrows(Canvas canvas) {
        canvas.drawPath(getPathToUpperArrow(getXCircleCoordinates(layoutWidth),
                getYCircleCoordinates(),
                circleRadius), color);
        canvas.drawPath(getPathToLowerArrow(getXCircleCoordinates(layoutWidth),
                getYCircleCoordinates(),
                circleRadius), color);
    }

    private Path getPathToUpperArrow(float circleX, float circleY, float circleRadius) {
        upperArrowPath.reset();
        float xStart = getArrowStartXCoordinates(circleX, circleRadius);
        float yStart = getUpperArrowStartYCoordinates(circleY, circleRadius);
        upperArrowPath.moveTo(xStart, yStart);
        float xCoordinates = getXCircleCoordinates(layoutWidth) - xStart;
        float yCoordinates = circleRadius / ARROW_COORDINATES_RATIO;
        upperArrowPath.rLineTo(xCoordinates, -yCoordinates);
        upperArrowPath.rLineTo(xCoordinates, yCoordinates);
        return upperArrowPath;
    }

    private Path getPathToLowerArrow(float circleX, float circleY, float circleRadius) {
        lowerArrowPath.reset();
        float xStart = getArrowStartXCoordinates(circleX, circleRadius);
        float yStart = getLowerArrowStartYCoordinates(circleY, circleRadius);
        lowerArrowPath.moveTo(xStart, yStart);
        float xCoordinates = getXCircleCoordinates(layoutWidth) - xStart;
        float yCoordinates = circleRadius / ARROW_COORDINATES_RATIO;
        lowerArrowPath.rLineTo(xCoordinates, yCoordinates);
        lowerArrowPath.rLineTo(xCoordinates, -yCoordinates);
        return lowerArrowPath;
    }

    private void processDownAction(MotionEvent ev) {
        final float x = ev.getX();
        final float y = ev.getY();
        if (isEnabled() && hitThumb(x, y, layoutWidth)) {
            mTouchMode = TOUCH_MODE_DOWN;
            mTouchY = y;
        } else {
            mTouchMode = TOUCH_MODE_IDLE;
        }
    }

    private boolean processMoveAction(MotionEvent ev) {
        switch (mTouchMode) {
            case TOUCH_MODE_IDLE:
                break;
            case TOUCH_MODE_DOWN:
                processTouchDown(ev);
                break;
            case TOUCH_MODE_DRAGGING:
                processTouchDragging(ev);
                return true;
        }
        return false;
    }

    private void processTouchDown(MotionEvent ev) {
        final float x = ev.getX();
        final float y = ev.getY();
        if (Math.abs(x - mTouchX) > mTouchSlop ||
                Math.abs(y - mTouchY) > mTouchSlop) {
            if (hitThumb(x, y, layoutWidth)) {
                mTouchMode = TOUCH_MODE_DRAGGING;
            }

            getParent().requestDisallowInterceptTouchEvent(true);
            mTouchX = x;
            mTouchY = y;
        }
    }

    private void processTouchDragging(MotionEvent ev) {
        int oldPosY = posY;
        posY = (int) calculateNewState(ev, posY);
        if (oldPosY != posY) {
            state = MainControlHelper.getValueForPosYpx(posY, maxValue, layoutHeight, (int) (padding));
            observer.notifyMyObservers();
            mTouchY = ev.getY();
            invalidate();
        }
    }

    private void setStartStateOfControl(AttributeSet attrs) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        layoutWidth = Parser.getWidth(attrs, metrics);
        layoutHeight = Parser.getHeight(attrs, metrics);
        calculateCircleRadius(layoutWidth);
    }

    private float calculateNewState(MotionEvent ev, float lastState) {
        float eventYPosition = ev.getY();
        if (eventYPosition > layoutHeight | eventYPosition < START_COORDINATES_Y) {
            return lastState;
        }
        float newState = calculateMoving(eventYPosition, lastState);
        if (newState < (START_COORDINATES_Y + padding)) {
            float circleRadiusShifting = newState - padding;
            newState = newState - circleRadiusShifting;
        } else if (newState > (layoutHeight - padding)) {
            float verticalCircleRadiusShifting = padding - (layoutHeight - newState);
            newState = newState - verticalCircleRadiusShifting;
        }
        return newState;
    }

    private float calculateMoving(float yPosition, float lastState) {
        if (yPosition < lastState) {
            return draggingUp(yPosition, lastState);
        } else if (yPosition > lastState) {
            return draggingDown(yPosition, lastState);
        }
        return lastState;
    }

    private float draggingDown(float yPosition, float lastState) {
        float touchShifting = yPosition - mTouchY;
        return lastState + touchShifting;
    }

    private float draggingUp(float yPosition, float lastState) {
        float touchShifting = mTouchY - yPosition;
        return lastState - touchShifting;
    }
}
