package com.blstream.water_tracker.view.controls.chart.components;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.ChartDrawable;
import com.blstream.water_tracker.interfaces.HistoryChart;
import com.blstream.water_tracker.models.GoogleFitData;

import java.util.List;

/**
 * Work in progress
 */
public class DivisionY implements ChartDrawable, HistoryChart {
    private float maxValue;
    private int dataSize;
    private int endX;
    private int histogramStartY;
    private int histogramEndY;
    private Paint fontPaint;
    private int valuePadding;
    private int heightOfBump;
    private int histogramEndX;
    private int histogramStartX;

    public DivisionY(View view) {
        fontPaint = new Paint();
        fontPaint.setAntiAlias(true);
        fontPaint.setColor(ContextCompat.getColor(view.getContext(), R.color.black));
    }

    @Override
    public void draw(Canvas canvas) {
        drawText(canvas);
    }

    @Override
    public void setData(List<GoogleFitData> data) {
        maxValue = Utils.findMaxValue(data);
        dataSize = data.size();
    }

    public void updateValues(int endX, int histogramStartX, int histogramStartY, int histogramEndX, int histogramEndY, int valuePadding, float fontSize) {
        this.histogramStartX = histogramStartX;
        this.histogramEndX = histogramEndX;
        this.valuePadding = valuePadding;
        this.endX = endX;
        this.histogramStartY = histogramStartY;
        this.histogramEndY = histogramEndY;
        fontPaint.setTextSize(fontSize);
        int barThickness = Utils.getBarThickness(dataSize, histogramStartX, histogramEndX);
        heightOfBump = Utils.getHeightOfBump(barThickness);
    }

    private void drawText(Canvas canvas) {
        int NUMBER_OF_VALUES = 10;
        for (int i = 0; i < NUMBER_OF_VALUES + 1; i++) {
            float value = (maxValue / NUMBER_OF_VALUES) * i;
            int height = histogramEndY - histogramStartY - heightOfBump;
            int textPos = Utils.getPosYForValue(value, maxValue, height) - histogramStartY - heightOfBump;
            textPos -= height;
            textPos *= -1;
            textPos += fontPaint.descent() / 2;
            float valueForDisplay = (int) (value * 10);
            valueForDisplay = valueForDisplay / 10;
            String text = String.valueOf(valueForDisplay);
            canvas.drawText(text, endX - fontPaint.measureText(text) - valuePadding, textPos, fontPaint);
        }
    }
}
