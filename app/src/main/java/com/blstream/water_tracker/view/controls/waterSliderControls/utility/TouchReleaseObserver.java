package com.blstream.water_tracker.view.controls.waterSliderControls.utility;

import java.util.Observable;

/**
 * Content observer for Touch release in SliderControl
 */
public class TouchReleaseObserver extends Observable {
    private static TouchReleaseObserver instance;

    private TouchReleaseObserver() {

    }

    public static TouchReleaseObserver getInstance() {
        if (instance == null) {
            return instance = new TouchReleaseObserver();
        } else {
            return instance;
        }
    }

    /**
     * Notifying observers when internet connection status has changed.
     */
    public void notifyMyObservers() {
        this.setChanged();
        this.notifyObservers();
    }
}
