package com.blstream.water_tracker.view.controls.switcher;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.blstream.water_tracker.R;

/**
 * Manages drawing background icon on canvas
 */
public class BackgroundIcon {
    private Path path = new Path();
    private Paint backgroundPaint;
    private int startX;
    private int startY;
    private int height;
    private int width;
    private View view;

    /**
     * Sets initial values and paint of background
     *
     * @param view   to be drawn on
     * @param startX X coordinate of first upper corner
     * @param startY Y coordinate of first upper corner
     * @param width  of the icon
     * @param height of the icon
     */
    public BackgroundIcon(View view, int startX, int startY, int width, int height) {
        this.startX = startX;
        this.startY = startY;
        this.height = height;
        this.width = width;
        this.view = view;
        setUpBackgroundPaint();
    }

    /**
     * Draws icon on canvas
     *
     * @param canvas to be drawn on
     */
    public void draw(Canvas canvas) {
        int halfOfHeight = height;
        int xMaxRightCorner = startX + width;
        int yStartOfArc = startY + halfOfHeight;
        int xStartOfArc = startX + halfOfHeight;
        int startRightAngle = 270;
        int sweepAngle = 180;
        int startLeftAngle = 90;

        path.reset();
        path.moveTo(startX, startY);
        path.arcTo(new RectF(xMaxRightCorner - halfOfHeight, startY, xMaxRightCorner, yStartOfArc),
                startRightAngle, sweepAngle);
        path.lineTo(xStartOfArc, yStartOfArc);
        path.arcTo(new RectF(startX, startY, xStartOfArc, yStartOfArc),
                startLeftAngle, sweepAngle);
        path.close();
        canvas.drawPath(path, backgroundPaint);
    }

    private void setUpBackgroundPaint() {
        int brightestBlueColor = ContextCompat.getColor(view.getContext(), R.color.brightest_blue);
        backgroundPaint = new Paint();
        backgroundPaint.setColor(brightestBlueColor);
        backgroundPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        backgroundPaint.setAntiAlias(true);

    }

}
