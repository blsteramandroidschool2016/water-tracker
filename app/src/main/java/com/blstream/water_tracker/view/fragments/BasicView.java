package com.blstream.water_tracker.view.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.IBasicView;
import com.blstream.water_tracker.internetConnection.InternetConnectionObserver;
import com.blstream.water_tracker.models.GoogleFitData;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Implements Basic methods of views.
 */
public abstract class BasicView extends Fragment implements IBasicView, Observer {
    public static final String CHART_FRAGMENT_ARGUMENT_KEY = "witaminy";
    private InternetConnectionObserver observer;
    private Snackbar snackBar;
    private static final String BACK_STACK_NAME = "WaterTrackerFragmentStackTrace";

    /**
     * {@inheritDoc}
     */
    @Override
    public void showError(Context context, View view, @ErrorMode int errorMode) {

        snackBar = createSnackBar(view, getResources().getString(errorMode), Snackbar.LENGTH_INDEFINITE);
        customizeSnackBar(context, snackBar);
        snackBar.show();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void hideError(Snackbar snackBar) {
        if (snackBar != null) {
            snackBar.dismiss();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void changeFragment(FragmentManager fragmentManager, @FragmentType int fragmentType) {
        switch (fragmentType) {
            case LOGIN_FRAGMENT:
                replaceFragment(fragmentManager, new LoginView(), R.id.mainViewContainer);
                break;
            case MAIN_VIEW_FRAGMENT:
                replaceFragment(fragmentManager, new MainView(), R.id.mainViewContainer);
                break;
            case HISTORY_VIEW_FRAGMENT:
                replaceFragment(fragmentManager, new HistoryView(), R.id.mainViewContainer);
                break;
            default:
                break;
        }
    }

    public void changeChartFragment(FragmentManager fragmentManager, List<GoogleFitData> data){
        ChartView chartView = new ChartView();
        Bundle args = new Bundle();
        args.putParcelableArrayList(CHART_FRAGMENT_ARGUMENT_KEY, (ArrayList<? extends Parcelable>) data);
        chartView.setArguments(args);
        replaceFragment(fragmentManager, chartView, R.id.mainViewContainer);
    }

    public void changeHistoryragment(FragmentManager fragmentManager, List<GoogleFitData> data){
        HistoryView chartView = new HistoryView();
        Bundle args = new Bundle();
        args.putParcelableArrayList(CHART_FRAGMENT_ARGUMENT_KEY, (ArrayList<? extends Parcelable>) data);
        chartView.setArguments(args);
        replaceFragment(fragmentManager, chartView, R.id.mainViewContainer);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        observer = InternetConnectionObserver.getInstance();
        observer.addObserver(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        update(null, null);
        super.onActivityCreated(savedInstanceState);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPause() {
        observer.deleteObserver(this);
        super.onPause();
    }

    /**
     * {@inheritDoc}
     * Used to show errors on screen.
     */
    @Override
    public void update(Observable observable, Object data) {
        if (!isInternetConnection((ConnectivityManager) getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE))) {
            showError(getContext(), getView(), NO_INTERNET_CONNECTION_ERROR);
        } else {
            hideError(snackBar);
        }
    }

    public Snackbar getSnackBar() {
        if (snackBar != null && snackBar.isShown()) {
            return snackBar;
        } else {
            return null;
        }
    }

    /**
     * Replacing fragment using fragment manager.
     *
     * @param fragmentManager instance of fragment manager.
     * @param fragment        instance of fragment to replace.
     */
    protected void replaceFragment(FragmentManager fragmentManager, @NonNull Fragment fragment, int containerId) {
        if (fragment instanceof MainView || fragment instanceof LoginView) {
            fragmentManager.beginTransaction()
                    .replace(containerId, fragment).commit();
        } else {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in, R.anim.standard_anim_out, R.anim.slide_in, R.anim.backstack_animation)
                    .replace(containerId, fragment).addToBackStack(BACK_STACK_NAME).commit();
        }
    }

    /**
     * Add fragment to stack using fragment manager.
     *
     * @param fragmentManager instance of fragment manager.
     * @param fragment        instance of fragment to add.
     */
    protected void addToStackFragment(FragmentManager fragmentManager, @NonNull Fragment fragment, int containerId) {
        fragmentManager.beginTransaction()
                .add(containerId, fragment).commit();
    }

    /**
     * @param context  instance of context
     * @param snackbar instance of snackbar to customize
     */
    protected void customizeSnackBar(Context context, Snackbar snackbar) {
        View snackBarView = snackbar.getView();
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.dark_blue));
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
    }

    /**
     * @param cm manager for handling connectivity changes
     * @return boolean internet connection state
     */
    protected boolean isInternetConnection(ConnectivityManager cm) {
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * @param view     instance of view on which snackbar is to be displayed
     * @param message  to be displayed
     * @param duration how long the msg is to be displayed, is defined by static snackbar fields
     * @return Snackbar instance of Snackbar
     */
    protected Snackbar createSnackBar(View view, String message, int duration) {
        return Snackbar.make(view, message, duration);
    }
}

