package com.blstream.water_tracker.view.controls.waterSliderControls.utility;

import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Converts sp and dp Values to on screen pixels
 */
public class PixelConverter {
    /**
     * Converts given value in dp, to on screen pixels
     *
     * @param dp size in dp
     * @return on screen pixels
     */
    public static float convertDpToPx(float dp, DisplayMetrics metrics) {
        if (dp > 0) {
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
        } else {
            return 0;
        }
    }

    /**
     * Converts given value in sp, to on screen pixels
     *
     * @param sp size in dp
     * @return on screen pixels
     */
    public static float convertSpToPx(float sp, DisplayMetrics metrics) {
        if (sp > 0) {
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, metrics);
        } else {
            return 0;
        }
    }

}
