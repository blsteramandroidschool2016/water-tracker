package com.blstream.water_tracker.view.controls.switcher;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.CircleIconSetStateCallback;
import com.blstream.water_tracker.interfaces.SwitchDrawable;

/**
 * Manages drawing circle icon on canvas
 */
public class CircleIcon implements SwitchDrawable {

    public static final float CIRCLE_SIZE_COMPARED_TO_CONTROL = 0.85f;
    private CircleIconSetStateCallback callback;
    private int circlePosY;
    private float circleRadius;
    private int circlePosXLeft;
    private int circlePosXRight;
    private int circlePosX;
    private float state;
    private Paint circlePaint;
    private View view;

    /**
     * Sets initial values of circle icon and its color
     *
     * @param view   to be drawn on
     * @param width  of icon
     * @param height of icon
     */
    public CircleIcon(View view, int width, int height) {
        this.view = view;
        circleRadius = (height * CIRCLE_SIZE_COMPARED_TO_CONTROL) / 2;
        circlePosY = height / 2;
        circlePosXLeft = (int) ((width / 2) - circleRadius);
        circlePosXRight = (int) ((width / 2) + circleRadius);
        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(ContextCompat.getColor(view.getContext(), R.color.slightly_dark_blue));
        state = 0;
    }

    /**
     * Gets state of control
     */
    @Override
    public float getState() {
        return state;
    }

    /**
     * {@inheritDoc}
     *
     * @param state values from 0.0 to 1.0, where 0.0 - unchecked, 1.0 - checked
     */
    @Override
    public void setState(float state) {
        this.state = state;
        if (callback != null) {
            callback.onSetState(state);
        }
        circlePosX = (int) (circlePosXLeft + ((circlePosXRight - circlePosXLeft) * state));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(circlePosX, circlePosY, circleRadius, circlePaint);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Animation getNewAnimation(float newState) {
        return new SwitchAnimation(view, this, newState);
    }

    /**
     * Sets callback listener for SetState callbacks
     *
     * @param callbackListener instance of listener
     */
    public void setCallbackListener(CircleIconSetStateCallback callbackListener) {
        callback = callbackListener;
    }

    /**
     * @return Circle pos on X axis
     */
    public int getCirclePosX() {
        return circlePosX;
    }

    /**
     * @return Circle pos on Y axis
     */
    public int getCirclePosY() {
        return circlePosY;
    }

    /**
     * @return circle radius
     */
    public float getCircleRadius() {
        return circleRadius;
    }

    /**
     * @return circle pos on X axis when switch is on left side
     */
    public int getCirclePosXLeft() {
        return circlePosXLeft;
    }

    /**
     * @return circle pos on X axis when switch is on right side
     */
    public int getCirclePosXRight() {
        return circlePosXRight;
    }


}
