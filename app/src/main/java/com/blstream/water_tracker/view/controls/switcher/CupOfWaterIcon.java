package com.blstream.water_tracker.view.controls.switcher;

import android.animation.ArgbEvaluator;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.SwitchDrawable;

/**
 * Manages drawing cup of water icon on canvas
 */
public class CupOfWaterIcon implements SwitchDrawable {
    private float strokeWidth;
    private int startX;
    private int startY;
    private int width;
    private int height;
    private Path path = new Path();
    private Paint cupPaint;
    private Paint waterPaint;
    private View view;
    private float state;
    int darkBlueColor;
    int brightestBlueColor;
    int whiteColor;

    /**
     * Sets initial values and
     *
     * @param view   to be drawn on
     * @param startX X coordinate of first upper corner
     * @param startY Y coordinate of first upper corner
     * @param width  of the icon
     * @param height of the icon
     */
    public CupOfWaterIcon(View view, int startX, int startY, int width, int height) {
        this.startX = startX;
        this.startY = startY;
        this.width = width;
        this.height = height;
        this.view = view;
        this.state = 0;
        this.strokeWidth = height / 10;
        setWaterPaint();
        setCupPaint();
        setState(state);
        darkBlueColor = ContextCompat.getColor(view.getContext(), R.color.slightly_dark_blue);
        whiteColor = ContextCompat.getColor(view.getContext(), R.color.white);
    }

    /**
     * Gets state of control
     */
    @Override
    public float getState() {
        return state;
    }

    /**
     * Sets state of icon
     *
     * @param state values from 0.0 to 1.0, where 0.0 - unchecked, 1.0 - checked
     */
    @Override
    public void setState(float state) {
        this.state = state;
        if(cupPaint != null) {
            cupPaint.setColor((Integer) new ArgbEvaluator().evaluate(state, whiteColor, darkBlueColor));
        }
    }

    /**
     * Draws object on specified canvas
     *
     * @param canvas to be drawn on
     */
    @Override
    public void draw(Canvas canvas) {
        path.reset();
        path.moveTo(startX + width / 10, startY + 2 * height / 5);
        path.lineTo(startX + 9 * width / 10, startY + height / 5);
        path.lineTo(0.75f * width + startX, height + startY);
        path.lineTo(startX + width / 4, height + startY);
        path.close();
        waterPaint.setStyle(Paint.Style.FILL);
        canvas.drawPath(path, waterPaint);
        drawIcon(canvas, cupPaint);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Animation getNewAnimation(float newState) {
        return new SwitchAnimation(view, this, newState);
    }

    /**
     * Draws cup icon on desired position and size
     *
     * @param canvas to be drawn on
     * @param startX X coordinate of first upper corner
     * @param startY Y coordinate of first upper corner
     * @param height of the icon
     */
    public void draw(Canvas canvas, int startX, int startY, int height) {
        this.startX = startX;
        this.startY = startY;
        this.height = height;
        this.width = (int) (height * SwitchControl.ASPECT_RATIO_CUP_OF_WATER);
        this.state = 0;
        this.strokeWidth = height / 10;

        draw(canvas);
    }

    /**
     * Sets alpha of icon to one given as paramaeter
     *
     * @param state from 0 t o255
     */
    public void setAlphaState(int state) {
        cupPaint.setAlpha(state);
        waterPaint.setAlpha(state);
    }


    private void setWaterPaint() {
        brightestBlueColor = ContextCompat.getColor(view.getContext(), R.color.light_blue);
        waterPaint = new Paint();
        waterPaint.setColor(brightestBlueColor);
        waterPaint.setStyle(Paint.Style.FILL);
    }

    private void setCupPaint() {
        cupPaint = new Paint();
        cupPaint.setDither(true);                    // set the dither to true
        cupPaint.setStyle(Paint.Style.STROKE);       // set to STROKE
        cupPaint.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
        cupPaint.setStrokeCap(Paint.Cap.ROUND);      // set the cupPain cap to round too
        cupPaint.setStrokeWidth(strokeWidth);
        cupPaint.setPathEffect(new CornerPathEffect(strokeWidth));   // set the path effect when they join.
        cupPaint.setAntiAlias(true);
        cupPaint.setColor(darkBlueColor);

    }

    private void drawIcon(Canvas canvas, Paint paint) {
        int moveRightUpperX = startX + width;
        float moveRightDownPointX = 0.75f * width + startX;
        int stayAtDownPositionY = height + startY;
        int moveLeftDownPointX = startX + width / 4;

        path.reset();
        path.moveTo(startX, startY);
        path.lineTo(moveRightUpperX, startY);
        path.lineTo(moveRightDownPointX, stayAtDownPositionY);
        path.lineTo(moveLeftDownPointX, stayAtDownPositionY);
        path.close();
        canvas.drawPath(path, paint);
    }

}
