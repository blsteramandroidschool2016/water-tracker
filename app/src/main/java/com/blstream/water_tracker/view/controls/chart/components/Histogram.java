package com.blstream.water_tracker.view.controls.chart.components;

import android.graphics.Canvas;
import android.util.Log;
import android.view.View;

import com.blstream.water_tracker.interfaces.ChartDrawable;
import com.blstream.water_tracker.interfaces.HistoryChart;
import com.blstream.water_tracker.models.DailyWaterRequirement;
import com.blstream.water_tracker.models.GoogleFitData;

import java.util.ArrayList;
import java.util.List;

/**
 * Draws Histogram on DataChart
 */
public class Histogram implements ChartDrawable, HistoryChart {
    private List<GoogleFitData> data;
    private int height;
    private int startX;
    private int endX;
    private int endY;
    private List<HistogramBar> bars;
    private View view;

    public Histogram(View view) {
        this.view = view;
        bars = new ArrayList<>();
    }

    @Override
    public void draw(Canvas canvas) {
        for (HistogramBar bar : bars) {
            bar.draw(canvas);
        }
    }

    @Override
    public void setData(List<GoogleFitData> data) {
        this.data = data;
        recalculateBarsPositions(data);
    }

    public void updateValues(int startX, int startY, int endX, int endY) {
        this.endX = endX;
        this.endY = endY;
        this.height = endY - startY;
        this.startX = startX;
        if (data != null) {
            recalculateBarsPositions(data);
        }
    }

    private void recalculateBarsPositions(List<GoogleFitData> data) {
        float maxValue = Utils.findMaxValue(data);
        int dataSize = data.size();
        bars.clear();
        for (int i = 0; i < dataSize; i++) {
            float waterRequirement = DailyWaterRequirement.calculateWaterRequirement(
                    data.get(i).getWeight(),
                    data.get(i).getBurnedCalories());
            bars.add(new HistogramBar(view,
                    Utils.getBarPosX(dataSize, i, startX, endX),
                    endY,
                    Utils.getBarThickness(dataSize, startX, endX),
                    height,
                    maxValue,
                    data.get(i).getWaterIntake(),
                    waterRequirement));
        }
    }
}
