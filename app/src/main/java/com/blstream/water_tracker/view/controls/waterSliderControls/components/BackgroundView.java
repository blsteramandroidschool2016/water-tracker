package com.blstream.water_tracker.view.controls.waterSliderControls.components;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.waterSlider.IWaterSliderControls;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.MainControlHelper;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.Parser;

/**
 * Draws background of WaterSliderControl on Canvas
 */
public class BackgroundView extends View implements IWaterSliderControls {

    private static final float ROUNDED_EDGE_RATIO = 0.05f;
    private static final int INITIAL_WATER_MAX_VALUE = 2;
    private static final float INITIAL_START_X = 0;
    private static final int WIDTH_MATCH_PARENT = -1;
    private static final int HEIGHT_MATCH_PARENT = -1;
    private static final float ROUNDED_EDGE_RATIO_START_ARC = 2.2f;

    private Path path;
    private RectF oval;
    private Paint backgroundPaint;
    private Paint gapPaint;
    private float state;
    private int posY;
    private float width;
    private float height;
    private float widthOfGap;
    private float heightOfGap;
    private float oneEdgeNextToGap;
    private float roundedEdge;
    private float roundedEdgeStartArc;
    private float maxValue;
    private boolean widthMatchParent;
    private boolean heightMatchParent;

    /**
     * Sets initial objects, state, paints and max value of water
     * {@inheritDoc}
     */
    public BackgroundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setInitMeasures(attrs);
        setBackgroundPaint(context);
        setGapPaint(context);
        posY = context.getResources().getDisplayMetrics().heightPixels;
        maxValue = INITIAL_WATER_MAX_VALUE;
        path = new Path();
        oval = new RectF();
    }

    /**
     * @return position of controls between MINIMUM_VALUE and maximum value of controls.
     */
    @Override
    public float getState() {
        return state;
    }

    /**
     * Sets controls to specified position, redraw view after it
     *
     * @param state position of controls between MINIMUM_VALUE and maximum value of controls.
     */
    @Override
    public void setState(float state) {
        this.state = state;
        updateState();
    }


    /**
     * Sets maximum position of controls
     *
     * @param maxValue max position of controls.
     */
    @Override
    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
        updateState();
    }

    /**
     * {@inheritDoc}
     *
     * @param canvas to be drawn on
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawBackground(canvas);
        drawArc(canvas);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (widthMatchParent) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }
        if (heightMatchParent) {
            height = MeasureSpec.getSize(heightMeasureSpec);
        }
        calculateAspectRatio();
        setMeasuredDimension((int) width, (int) height);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setHeightOfGap(float heightOfGap) {
        this.heightOfGap = heightOfGap;
    }

    private void updateState() {
        posY = MainControlHelper.getPosYpx(state, maxValue, (int) height, (int) heightOfGap * 2);
        invalidate();
    }

    private void drawArc(Canvas canvas) {
        float leftSideOval = INITIAL_START_X + oneEdgeNextToGap - roundedEdgeStartArc;
        float topSideOfOval = posY - heightOfGap;
        float rightSideOfOval = INITIAL_START_X + oneEdgeNextToGap + widthOfGap + roundedEdgeStartArc;
        float bottomSideOfOval = posY + heightOfGap;
        oval.set(leftSideOval,
                topSideOfOval,
                rightSideOfOval,
                bottomSideOfOval);
        float startAngle = 0;
        float sweepAngle = 180;
        canvas.drawArc(oval, startAngle, sweepAngle, true, gapPaint);
    }

    public void setWidthOfGap(float widthOfGap) {
        this.widthOfGap = widthOfGap;
    }

    private void drawBackground(Canvas canvas) {
        float startAngle = 270;
        float sweepAngleLeftSide = 90;
        float sweepAngleRightSide = 180;
        float halfOfRoundedEdge = roundedEdge / 2;
        float leftOvalSide = INITIAL_START_X + oneEdgeNextToGap - roundedEdge;
        float rightSideOval = INITIAL_START_X + oneEdgeNextToGap;
        float rightOvalSide = INITIAL_START_X + oneEdgeNextToGap + widthOfGap + roundedEdge;
        float bottomSideOval = posY + roundedEdge;
        float leftXPosition = INITIAL_START_X + oneEdgeNextToGap + widthOfGap;
        float currentX = INITIAL_START_X + width;
        float currentY = posY + height;

        path.reset();
        path.moveTo(INITIAL_START_X, posY);
        path.lineTo(leftOvalSide - roundedEdge, posY);
        drawArcRightRec(startAngle, sweepAngleLeftSide, leftOvalSide, rightSideOval, bottomSideOval);
        path.lineTo(leftXPosition, posY + halfOfRoundedEdge);
        drawArcLeftRec(sweepAngleLeftSide, sweepAngleRightSide, rightOvalSide, bottomSideOval, leftXPosition);
        path.lineTo(currentX, posY);
        path.lineTo(currentX, currentY);
        path.lineTo(INITIAL_START_X, currentY);
        path.close();
        canvas.drawPath(path, backgroundPaint);
    }

    private void drawArcRightRec(float startAngle, float sweepAngleLeftSide, float leftOvalSide, float rightSideOval, float bottomSideOval) {
        oval.set(leftOvalSide,
                posY,
                rightSideOval,
                bottomSideOval);

        path.arcTo(oval, startAngle, sweepAngleLeftSide);
    }

    private void drawArcLeftRec(float sweepAngleLeftSide, float sweepAngleRightSide, float rightOvalSide, float bottomSideOval, float leftXPosition) {
        oval.set(leftXPosition,
                posY,
                rightOvalSide,
                bottomSideOval);
        path.arcTo(oval, sweepAngleLeftSide, sweepAngleRightSide);
    }

    private void setGapPaint(Context context) {
        gapPaint = new Paint();
        int gapColor = ContextCompat.getColor(context, R.color.white);
        gapPaint.setColor(gapColor);
        gapPaint.setStyle(Paint.Style.FILL);
        gapPaint.setAntiAlias(true);

    }

    private void setInitMeasures(AttributeSet attrs) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        width = Parser.getWidth(attrs, metrics);
        height = Parser.getHeight(attrs, metrics);
        widthMatchParent = width == WIDTH_MATCH_PARENT;
        heightMatchParent = height == HEIGHT_MATCH_PARENT;
    }

    private void calculateAspectRatio() {
        oneEdgeNextToGap = (width - widthOfGap) / 2;
        roundedEdge = ROUNDED_EDGE_RATIO * width;
        roundedEdgeStartArc = ROUNDED_EDGE_RATIO * roundedEdge * ROUNDED_EDGE_RATIO_START_ARC;
    }

    private void setBackgroundPaint(Context context) {
        backgroundPaint = new Paint();
        int backgroundColor = ContextCompat.getColor(context, R.color.slightly_dark_blue);
        backgroundPaint.setColor(backgroundColor);
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setAntiAlias(true);
    }
}

