package com.blstream.water_tracker.view.fragments.chartViewComponents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.models.GoogleFitData;
import com.blstream.water_tracker.view.controls.chart.DataChart;
import com.blstream.water_tracker.view.fragments.HistoryView;

import java.util.ArrayList;

/**
 * Work in progress
 */
public class WeeklyChart extends Fragment {
    public WeeklyChart() {
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_pager_chart_layout, container, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DataChart dailyChart = (DataChart) view.findViewById(R.id.weeklyPagerSingleChart);
        ArrayList<GoogleFitData> data = getArguments().getParcelableArrayList(HistoryView.CHART_FRAGMENT_ARGUMENT_KEY);
        if (data != null) {
            dailyChart.setData(data);
        }
    }
}
