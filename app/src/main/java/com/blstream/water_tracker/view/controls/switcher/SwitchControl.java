package com.blstream.water_tracker.view.controls.switcher;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AnimationSet;

import com.blstream.water_tracker.interfaces.CircleIconSetStateCallback;
import com.blstream.water_tracker.interfaces.SwitchChangeStateListener;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.Parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Control for Switching between Cups of water and Liters in main app View
 */
public class SwitchControl extends View {
    public static final float ASPECT_RATIO_BACKGROUND = 0.523f;
    public static final float ASPECT_RATIO_CUP_OF_WATER = 0.8f;
    public static final float ASPECT_RATIO_CUP_OF_WATER_AND_BACKGROUND = 0.35f;
    private static final int TOUCH_MODE_IDLE = 0;
    private static final int TOUCH_MODE_DOWN = 1;
    private static final int TOUCH_MODE_DRAGGING = 2;
    private static final int TOUCH_MODE_DRAGGING_ON_BG = 3;
    private static final int TOUCH_MODE_BAD = 4;
    private static final float STATE_MIDDLE = 0.5f;
    private int mTouchSlop;
    private int iconWidth;
    private int iconHeight;
    private int iconSizePadding;
    private int iconCupHeightPadding;

    private BackgroundIcon backgroundIcon;
    private CircleIcon circleIcon;
    private CupOfWaterIcon cupOfWaterIcon;
    private LitersIcon litersIcon;
    private List<SwitchChangeStateListener> listeners = new ArrayList<>();
    private int sizeXpx;
    private int sizeYpx;
    private int mTouchMode;
    private float mTouchX;
    private float mTouchY;
    private boolean checked;


    /**
     * {@inheritDoc}
     */
    public SwitchControl(final Context context, AttributeSet attrs) {
        super(context, attrs);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = Parser.getWidth(attrs, metrics);
        int height = Parser.getHeight(attrs, metrics);
        if (width > height * ASPECT_RATIO_BACKGROUND) {
            sizeYpx = height;
            sizeXpx = (int) (height / ASPECT_RATIO_BACKGROUND);
        } else {
            sizeXpx = width;
            sizeYpx = (int) (width * ASPECT_RATIO_BACKGROUND);
        }
        convertSizeRatio();
        initIcons();
        final ViewConfiguration config = ViewConfiguration.get(context);
        mTouchSlop = config.getScaledTouchSlop();
        circleIcon.setState(0);
        circleIcon.setCallbackListener(new CircleIconSetStateCallback() {
            @Override
            public void onSetState(float state) {
                callChangeStateListeners(state);
            }
        });

        checked = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                processDownAction(ev);
                break;

            case MotionEvent.ACTION_MOVE:
                if (processMoveAction(ev)) {
                    return true;
                }

                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (processCancelAction(ev)) {
                    return true;
                }
                break;

        }
        return super.onTouchEvent(ev);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        backgroundIcon.draw(canvas);
        circleIcon.draw(canvas);
        cupOfWaterIcon.draw(canvas);
        litersIcon.draw(canvas);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(sizeXpx, sizeYpx);
    }


    /**
     * Adds on ChangeStateListener which is called every time switch changes is state(During finger swipe and animation)
     *
     * @param listener to add
     */
    public void addChangeStateListener(SwitchChangeStateListener listener) {
        listeners.add(listener);
    }

    /**
     * @return switch current state
     */
    public boolean isChecked() {
        return checked;
    }

    /**
     * Sets switch to state given ia parameter
     *
     * @param checked state to set switch
     */
    public void setChecked(boolean checked) {
        this.checked = checked;
        float newStateF = (checked) ? 1 : 0;
        circleIcon.setState(newStateF);
        litersIcon.setState(newStateF);
        cupOfWaterIcon.setState(newStateF);
    }



    private void callChangeStateListeners(float state) {
        for (SwitchChangeStateListener listener : listeners) {
            listener.onStateChange(state);
        }
    }

    private void initIcons() {
        backgroundIcon = new BackgroundIcon(this, 0, 0, sizeXpx, sizeYpx);
        circleIcon = new CircleIcon(this, sizeXpx, sizeYpx);
        cupOfWaterIcon = new CupOfWaterIcon(this, circleIcon.getCirclePosXLeft() - iconSizePadding,
                circleIcon.getCirclePosY() - iconCupHeightPadding, iconWidth, iconHeight);
        litersIcon = new LitersIcon(this, circleIcon.getCirclePosXRight() - iconSizePadding,
                circleIcon.getCirclePosY() - iconSizePadding, iconWidth);
    }

    private void convertSizeRatio() {
        iconWidth = (int) (sizeYpx * ASPECT_RATIO_CUP_OF_WATER_AND_BACKGROUND);
        iconHeight = (int) (iconWidth / ASPECT_RATIO_CUP_OF_WATER);
        iconSizePadding = iconWidth / 2;
        iconCupHeightPadding = iconHeight / 2;
    }

    private boolean processCancelAction(MotionEvent ev) {
        if (mTouchMode == TOUCH_MODE_BAD) {
            return false;
        }
        if (mTouchMode == TOUCH_MODE_DRAGGING) {
            mTouchMode = TOUCH_MODE_IDLE;
            boolean newState = circleIcon.getState() > STATE_MIDDLE;
            if (newState != checked) {
                checked = newState;
                startAnimation(checked);
                return false;
            }
            startAnimation(checked);
            return true;
        } else {
            if (hitView(ev.getX(), ev.getY())) {
                startAnimation(checked = !checked);
            }
        }
        return false;
    }

    private boolean hitView(float x, float y) {
        final float thumbLeft = 0;
        final float thumbRight = sizeXpx;
        final float thumbTop = 0;
        final float thumbBottom = sizeYpx;
        return x > thumbLeft && x < thumbRight && y > thumbTop && y < thumbBottom;
    }

    private void processDownAction(MotionEvent ev) {
        final float x = ev.getX();
        final float y = ev.getY();
        if (isEnabled() && hitView(x, y)) {
            mTouchMode = TOUCH_MODE_DOWN;
            mTouchX = x;
            mTouchY = y;
        }
    }

    private boolean processMoveAction(MotionEvent ev) {
        switch (mTouchMode) {
            case TOUCH_MODE_IDLE:
                // Didn't target the thumb, treat normally.
                break;

            case TOUCH_MODE_DOWN:
                if (processTouchDown(ev)) {
                    return true;
                }
                break;


            case TOUCH_MODE_DRAGGING:
                processTouchDragging(ev);
                return true;


            case TOUCH_MODE_DRAGGING_ON_BG:
                if (!hitView(ev.getX(), ev.getY())) {
                    mTouchMode = TOUCH_MODE_BAD;
                }

        }
        return false;
    }

    private void processTouchDragging(MotionEvent ev) {
        float newSide = normalizeNewSide(circleIcon.getState() + getNormalizedTouchDiff(ev.getX()));
        circleIcon.setState(newSide);
        cupOfWaterIcon.setState(newSide);
        litersIcon.setState(newSide);
        this.invalidate();
        mTouchX = ev.getX();
        mTouchY = ev.getY();
    }

    private float getNormalizedTouchDiff(float x) {
        final float MOVE_SPEED = 2.66f;
        float diff = (x) - mTouchX;
        return (diff * MOVE_SPEED) / sizeXpx;
    }

    private boolean processTouchDown(MotionEvent ev) {
        final float x = ev.getX();
        final float y = ev.getY();
        if (Math.abs(x - mTouchX) > mTouchSlop ||
                Math.abs(y - mTouchY) > mTouchSlop) {
            if (hitThumb(x, y)) {
                mTouchMode = TOUCH_MODE_DRAGGING;
            } else {
                mTouchMode = TOUCH_MODE_DRAGGING_ON_BG;
            }

            getParent().requestDisallowInterceptTouchEvent(true);
            mTouchX = x;
            mTouchY = y;
            return true;
        }
        return false;
    }

    private float normalizeNewSide(float newSide) {
        if (newSide > 1) {
            newSide = 1;
        } else if (newSide < 0) {
            newSide = 0;
        }
        return newSide;
    }

    private boolean hitThumb(float x, float y) {
        final float EXTRA_AREA = 1.5f;
        final float thumbLeft = circleIcon.getCirclePosX() - circleIcon.getCircleRadius() * EXTRA_AREA;
        final float thumbRight = circleIcon.getCirclePosX() + circleIcon.getCircleRadius() * EXTRA_AREA;
        final float thumbTop = circleIcon.getCirclePosY() - circleIcon.getCircleRadius() * EXTRA_AREA;
        final float thumbBottom = circleIcon.getCirclePosY() + circleIcon.getCircleRadius() * EXTRA_AREA;
        return x > thumbLeft && x < thumbRight && y > thumbTop && y < thumbBottom;
    }

    private void startAnimation(boolean newState) {
        float newStateF = (newState) ? 1 : 0;
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(circleIcon.getNewAnimation(newStateF));
        animationSet.addAnimation(cupOfWaterIcon.getNewAnimation(newStateF));
        animationSet.addAnimation(litersIcon.getNewAnimation(newStateF));
        this.startAnimation(animationSet);
    }


}
