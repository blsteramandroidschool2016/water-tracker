package com.blstream.water_tracker.view.controls.switcher;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.blstream.water_tracker.interfaces.SwitchDrawable;

/**
 * Animation for animated parts of switch (Moving circle, cups Liters icons)
 */
public class SwitchAnimation extends Animation {
    private static final long ANIMATION_DURATION = 300;
    private View view;
    private float oldState;
    private float newState;
    private SwitchDrawable icon;

    /**
     * Sets initial values of animation
     * @param view to be animated on
     * @param icon to be animated
     * @param newState of the animation
     */
    public SwitchAnimation(View view, SwitchDrawable icon, float newState) {
        this.view = view;
        this.newState = newState;
        this.oldState = icon.getState();
        this.icon = icon;
        long time = (long) ((newState - oldState) * ANIMATION_DURATION);
        if (time < 0) {
            time *= -1;
        }
        setDuration(time);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float state = oldState + ((newState - oldState) * interpolatedTime);
        icon.setState(state);
        view.requestLayout();
    }

}
