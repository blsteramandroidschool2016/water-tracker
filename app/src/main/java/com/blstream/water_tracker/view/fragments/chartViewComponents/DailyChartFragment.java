package com.blstream.water_tracker.view.fragments.chartViewComponents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.models.GoogleFitData;
import com.blstream.water_tracker.view.controls.chart.DataChart;
import com.blstream.water_tracker.view.fragments.BasicView;

import java.util.ArrayList;

/**
 * Work in progress
 */
public class DailyChartFragment extends Fragment {

    private static final int DAYS_ON_CHART = 7;

    public DailyChartFragment() {
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.daily_chart_layout, container, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DataChart dailyChart = (DataChart) view.findViewById(R.id.chartDaily);
        ArrayList<GoogleFitData> data = getArguments().getParcelableArrayList(BasicView.CHART_FRAGMENT_ARGUMENT_KEY);
        assert data != null;
        if (data.size() > DAYS_ON_CHART){
            dailyChart.setData(data.subList(0, DAYS_ON_CHART));
        } else {
            dailyChart.setData(data);
        }
    }

}
