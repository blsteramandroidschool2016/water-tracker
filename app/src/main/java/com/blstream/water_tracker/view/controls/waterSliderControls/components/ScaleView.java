package com.blstream.water_tracker.view.controls.waterSliderControls.components;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.blstream.water_tracker.interfaces.waterSlider.IWaterSliderControls;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.Parser;

/**
 * Draws left side of water indicator in main app View
 */
public class ScaleView extends View implements IWaterSliderControls {

    public static final float WIDTH_RATIO = 0.33f;
    private static final float SLIDER_THICKNESS_RATIO = 8.81f;
    private static final float SLIDER_PADDING_RATIO = 79.3f;
    private static final int WIDTH_MATCH_PARENT = -1;
    private static final int HEIGHT_MATCH_PARENT = -1;
    private static final float LANDSCAPE_RATIO = 1.5f;
    private final boolean widthMatchParent;
    private final boolean heightMatchParent;
    private float state;
    private float maxValue;
    private int width;
    private int height;
    private LeftGraduationDrawer leftGraduationDrawer;

    /**
     * {@inheritDoc}
     */
    public ScaleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        width = Parser.getWidth(attrs, metrics);
        height = Parser.getHeight(attrs, metrics);
        widthMatchParent = width == WIDTH_MATCH_PARENT;
        heightMatchParent = height == HEIGHT_MATCH_PARENT;
        leftGraduationDrawer = new LeftGraduationDrawer();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        leftGraduationDrawer.draw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (widthMatchParent) {
            width = Math.round(MeasureSpec.getSize(widthMeasureSpec) * WIDTH_RATIO);
        }
        if (heightMatchParent) {
            height = MeasureSpec.getSize(heightMeasureSpec);
        }
        calculateAspectRatio();
        setMeasuredDimension(width, height);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    /**
     * @return position of controls between MINIMUM_VALUE and maximum value of controls.
     */
    @Override
    public float getState() {
        return state;
    }

    /**
     * Sets controls to specified position
     *
     * @param state position of controls between MINIMUM_VALUE and maximum value of controls .
     */
    @Override
    public void setState(float state) {
        this.state = state;
        updateState();
    }

    /**
     * Sets maximum position of controls
     *
     * @param maxValue max position of controls.
     */
    @Override
    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
        updateState();
    }

    /**
     * Sets padding for control value and maxValue position in px.
     *
     * @param padding in pixels
     */
    public void setPadding(float padding) {
        leftGraduationDrawer.setPadding((int) (padding * 2));
    }

    public void setLiters(boolean liters) {
        leftGraduationDrawer.setLiters(liters);
        invalidate();
    }

    public void setAlphaState(float alphaState) {
        leftGraduationDrawer.setAlphaState(alphaState);
        invalidate();
    }

    private void calculateAspectRatio() {
       int thickness = (int) (height / SLIDER_THICKNESS_RATIO);
        int padding = (int) (height / SLIDER_PADDING_RATIO);
        if(height < width) {
            thickness *= LANDSCAPE_RATIO;
            padding *=LANDSCAPE_RATIO;
        }
        leftGraduationDrawer.updateValues(this, thickness, padding, width, height);
    }

    private void updateState() {
        leftGraduationDrawer.updateValue(state, maxValue);
        invalidate();
    }
}
