package com.blstream.water_tracker.view.controls.waterSliderControls.utility;

import com.blstream.water_tracker.interfaces.waterSlider.IWaterSliderControls;

/**
 * Utils for main control
 */
public class MainControlHelper{

    /**
     * Calculates Y pos of scroll in pixels compared to height of Control
     *
     * @param value         water value for which Y will be calculated
     * @param maxValue      current water max value
     * @param controlHeight overall height of control
     * @param padding       top and bottom padding (scrolling circle radius)
     * @return Y position in pixels of current selected water value
     */

    public static int getPosYpx(float value, float maxValue, int controlHeight, int padding) {
        value = value - IWaterSliderControls.MINIMUM_VALUE;
        maxValue = maxValue - IWaterSliderControls.MINIMUM_VALUE;
        float posYPercent = value / maxValue;
        int posYpx = (int) (((controlHeight - padding) * posYPercent) + (padding / 2));
        posYpx = (posYpx - controlHeight) * (-1);
        return posYpx;
    }


    /**
     * Calculates value for given scroll position in pixels
     *
     * @param posYpx        position of slider in pixels
     * @param maxValue      current max water value
     * @param controlHeight current height of control
     * @param padding       top and bottom padding (scrolling circle radius)
     * @return value between IWaterSliderControl.MINIMUM_VALUE and maxValue
     */
    public static float getValueForPosYpx(int posYpx, float maxValue, int controlHeight, int padding) {
        maxValue -= IWaterSliderControls.MINIMUM_VALUE;
        float posYPercent = (float)(posYpx - padding) / (float)(controlHeight - (padding * 2));
        float value = posYPercent * maxValue;
        value = (value - maxValue) * (-1);
        value += IWaterSliderControls.MINIMUM_VALUE;
        return value;
    }
}
