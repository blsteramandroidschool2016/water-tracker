package com.blstream.water_tracker.view.controls.waterSliderControls.utility;

import java.util.Observable;

/**
 * Content observer for state change in WaterSlider
 */
public class StateChangeObserver extends Observable {

    private static StateChangeObserver instance;

    private StateChangeObserver() {

    }

    /**
     * @return instance of obsserver
     */
    public static StateChangeObserver getInstance() {
        if (instance == null) {
            return instance = new StateChangeObserver();
        } else {
            return instance;
        }
    }

    /**
     * Notifying observers when internet connection status has changed.
     */
    public void notifyMyObservers() {
        this.setChanged();
        this.notifyObservers();
    }
}
