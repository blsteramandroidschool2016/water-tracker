package com.blstream.water_tracker.view.controls.waterSliderControls;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.waterSlider.IWaterSliderControls;
import com.blstream.water_tracker.interfaces.waterSlider.OnValueChangedListener;
import com.blstream.water_tracker.interfaces.waterSlider.OnValueSetListener;
import com.blstream.water_tracker.view.controls.waterSliderControls.components.BackgroundView;
import com.blstream.water_tracker.view.controls.waterSliderControls.components.CircleView;
import com.blstream.water_tracker.view.controls.waterSliderControls.components.ScaleView;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.StateChangeObserver;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.TouchReleaseObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Handles water slider control
 */
public class WaterSliderController extends RelativeLayout implements Observer {


    public static final float INITIAL_MAX_VALUE = 2.0f;
    private static final float HEIGHT_GAP_RATIO = 0.105f;
    private static final float WIDTH_GAP_RATIO = 0.2f;
    private static final float HEIGH_WIDTH_GAP_RATIO = WIDTH_GAP_RATIO / HEIGHT_GAP_RATIO;
    private static final float SNAP_ANIMATION_DURATION = 500;
    private static final float COMP_DELTA = 0.05f;
    private static final int SLIDER_DRAGGING = 0;
    private static final int SLIDER_SNAPPING = 1;
    private static final int SLIDER_CHANGING_MAX = 2;
    private static final float MIN_MAX_VALUE = 2;
    private static final int WAITING_ABORTED = 0;
    private static final int WAITING_UP = 1;
    private static final int WAITING_DOWN = 2;
    private CircleView circleSlider;
    private BackgroundView backgroundView;
    private ScaleView scaleView;
    private float heightOfGap;
    private int size;
    private float widthOfGap;
    private ValueAnimator snapAnimator;
    private ValueAnimator maxValueAnimator;
    private int sliderMode;
    private float maxValue;
    private List<OnValueChangedListener> onValueChangedListeners = new ArrayList<>();
    private List<OnValueSetListener> onValueSetListeners = new ArrayList<>();
    private float state;
    private boolean maxChanged;
    private int waitingState;

    /**
     * {@inheritDoc}
     */
    public WaterSliderController(Context context, AttributeSet attrs) {
        super(context, attrs);
        StateChangeObserver observer = StateChangeObserver.getInstance();
        observer.addObserver(this);
        TouchReleaseObserver touchReleaseObserver = TouchReleaseObserver.getInstance();
        touchReleaseObserver.addObserver(this);
        inflate(getContext(), R.layout.water_slider_controller_layout, this);
        circleSlider = (CircleView) findViewById(R.id.sliderControl);
        backgroundView = (BackgroundView) findViewById(R.id.bacgroundView);
        scaleView = (ScaleView) findViewById(R.id.scaleView);
        setMaxValue(INITIAL_MAX_VALUE);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (size == 0) {
            size = width > height ? height : width;
            initialGapHeight();
        }
        initialGapWidth();
        circleSlider.setPadding(heightOfGap);
        backgroundView.setHeightOfGap(heightOfGap);
        backgroundView.setWidthOfGap(widthOfGap);
        scaleView.setPadding(heightOfGap);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        circleSlider.setState(state);
        backgroundView.setState(circleSlider.getState());
        scaleView.setState(circleSlider.getState());
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Observable observable, Object data) {
        float state = circleSlider.getState();
        if (observable instanceof TouchReleaseObserver) {
            processTouchRelease(state);
        }

        if (observable instanceof StateChangeObserver) {
            processChangeState(state);
        }
    }

    /**
     * Adds OnValueChangedListener.
     *
     * @param toAdd listener to add.
     */
    public void addOnValueChangedListener(OnValueChangedListener toAdd) {
        onValueChangedListeners.add(toAdd);
    }

    /**
     * Adds OnValueSetListener.
     *
     * @param toAdd listener to add.
     */
    public void addOnValueSetListener(OnValueSetListener toAdd) {
        onValueSetListeners.add(toAdd);
    }

    /**
     * @return current state
     */
    public float getState() {
        return state;
    }

    /**
     * Sets state, circle will be drawn on that state
     *
     * @param state Must be between IWaterSliderControls.MINIMUM_VALUE
     *              and IWaterSliderControls.MAXIMUM_VALUE
     */
    public void setStateWithAnimation(float state) {
        stopWaitingAnimation();
        setMaxValue(getTargetMaxState(state));
        prepareAndStartSnapAnimation(this.state, state);
        this.state = state;
    }

    /**
     * Sets state without animation, circle will be drawn on that state
     *
     * @param state Must be between IWaterSliderControls.MINIMUM_VALUE
     *              and IWaterSliderControls.MAXIMUM_VALUE
     */
    public void setStateWithoutAnimation(float state) {
        stopWaitingAnimation();
        this.state = state;
        setMaxValue(getTargetMaxState(state));
        circleSlider.setState(state);
        setStateForScaleAndBg(state);
        circleSlider.setState(state);
        callOnValueSetListener();
    }

    /**
     * @return current maxValue
     */
    public float getMaxValue() {
        return maxValue;
    }

    /**
     * Sets current maxValue to specified in argument
     *
     * @param value maxValue to set
     */
    public void setMaxValue(float value) {
        this.maxValue = value;
        circleSlider.setMaxValue(value);
        scaleView.setMaxValue(value);
        backgroundView.setMaxValue(value);
    }

    /**
     * Calculates padding
     *
     * @return Calculated padding
     */
    public float initialGapHeight() {
        return heightOfGap = HEIGHT_GAP_RATIO * size;
    }

    /**
     * If true it sets liters as scale division if false then glasses is sets.
     *
     * @param liters true or false.
     */
    public void setLiters(boolean liters) {
        scaleView.setLiters(liters);
    }

    public void setAlphaState(float alphaState) {
        scaleView.setAlphaState(alphaState);
    }

    /**
     * Start wait animation;
     */
    public void startWaitAnimation() {
        waitingState = WAITING_UP;
        prepareAndStartWaitAnimation(0f, 2f, 1000);
    }

    private void stopWaitingAnimation() {
        if (waitingState == WAITING_DOWN || waitingState == WAITING_UP) {
            if (snapAnimator != null) {
                snapAnimator.cancel();
            }
            waitingState = WAITING_ABORTED;
        }
    }

    private void initialGapWidth() {
        widthOfGap = heightOfGap * HEIGH_WIDTH_GAP_RATIO;
    }

    private void setStateForScaleAndBg(float state) {
        scaleView.setState(state);
        backgroundView.setState(state);
        callOnValueChangedListener(state);
    }

    private void processTouchRelease(float state) {
        if (sliderMode == SLIDER_CHANGING_MAX || maxChanged) {
            maxValueAnimator.cancel();
            snapAnimator.cancel();
        }
        prepareAndStartMaxValueAnimation(maxValue, getTargetMaxState(getTargetState(state)));
        prepareAndStartSnapAnimation(state, getTargetState(state));
    }

    private void processChangeState(float state) {
        switch (sliderMode) {
            case SLIDER_SNAPPING:
                snapAnimator.cancel();
                sliderMode = SLIDER_DRAGGING;
                break;
            case SLIDER_CHANGING_MAX:
                snapAnimator.cancel();
                maxValueAnimator.cancel();
                sliderMode = SLIDER_DRAGGING;
                break;
        }
        if (compareWithDelta(state, maxValue, COMP_DELTA)) {
            if (maxValueAnimator != null) {
                maxValueAnimator.cancel();
            }
            prepareAndStartMaxValueAnimation(maxValue, IWaterSliderControls.MAXIMUM_VALUE);
            prepareAndStartSnapAnimation(maxValue, IWaterSliderControls.MAXIMUM_VALUE);
            sliderMode = SLIDER_CHANGING_MAX;
            maxChanged = true;

        }
        setStateForScaleAndBg(state);
    }

    private boolean compareWithDelta(float a, float b, float delta) {
        return (Math.abs(a - b) < delta);
    }

    private void callOnValueChangedListener(float state) {
        for (OnValueChangedListener listener : onValueChangedListeners) {
            listener.onChangeValue(state);
        }
    }

    private void callOnValueSetListener() {
        for (OnValueSetListener listener : onValueSetListeners) {
            listener.onValueSet();
        }
    }

    /**
     * Returns value snapped by 0.2 to given state
     *
     * @param state to calculate snap value
     * @return value snapped to state
     */
    private float getTargetState(float state) {
        float targetState = state * 10;
        targetState /= 2;
        targetState = Math.round(targetState);
        targetState *= 2;
        targetState /= 10;
        if (targetState > maxValue) {
            targetState -= 0.2f;
        }
        if (targetState < IWaterSliderControls.MINIMUM_VALUE) {
            targetState += 0.2f;
        }
        return targetState;
    }

    private float getTargetMaxState(float state) {
        float targetState = state * 2;
        targetState = (float) Math.ceil(targetState);
        targetState /= 2;
        targetState += 1;
        if (targetState > IWaterSliderControls.MAXIMUM_VALUE) {
            targetState = IWaterSliderControls.MAXIMUM_VALUE;
        }
        if (targetState < MIN_MAX_VALUE) {
            targetState = MIN_MAX_VALUE;
        }
        return targetState;
    }

    private void prepareAndStartSnapAnimation(final float state, float targetState) {
        snapAnimator = ValueAnimator.ofFloat(state, targetState);
        long duration = (long) ((targetState - state) * SNAP_ANIMATION_DURATION);
        if (duration < 0) {
            duration *= -1;
        }
        snapAnimator.setDuration(duration);
        snapAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                setStateForScaleAndBg(value);
                circleSlider.setState(value);
            }
        });
        snapAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (sliderMode == SLIDER_SNAPPING) {
                    callOnValueSetListener();
                }
            }
        });
        snapAnimator.start();
        this.state = targetState;
        sliderMode = SLIDER_SNAPPING;
    }

    private void prepareAndStartMaxValueAnimation(float maxValue, float targetMaxValue) {
        maxValueAnimator = ValueAnimator.ofFloat(maxValue, targetMaxValue);
        long duration = (long) ((targetMaxValue - maxValue) * SNAP_ANIMATION_DURATION);
        if (duration < 0) {
            duration *= -1;
        }
        maxValueAnimator.setDuration(duration);
        maxValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                setMaxValue(value);
            }
        });
        maxValueAnimator.start();
        sliderMode = SLIDER_CHANGING_MAX;
    }

    private void prepareAndStartWaitAnimation(final float startValue, final float maxValue, final long duration) {
        snapAnimator = ValueAnimator.ofFloat(startValue, maxValue);
        snapAnimator.setDuration(duration);
        snapAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                if (waitingState == WAITING_ABORTED) {
                    animation.cancel();
                } else {
                    state = value;
                    setStateForScaleAndBg(value);
                    circleSlider.setState(value);
                    if (waitingState == WAITING_UP && compareWithDelta(value, maxValue, COMP_DELTA)) {
                        waitingState = WAITING_DOWN;
                        animation.reverse();
                    } else if (waitingState == WAITING_DOWN && compareWithDelta(value, startValue, COMP_DELTA)) {
                        waitingState = WAITING_UP;
                        animation.reverse();
                    }
                }
            }
        });
        snapAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }
        });
        snapAnimator.start();
    }
}
