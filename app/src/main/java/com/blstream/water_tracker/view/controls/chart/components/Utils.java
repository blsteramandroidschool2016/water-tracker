package com.blstream.water_tracker.view.controls.chart.components;

import com.blstream.water_tracker.models.DailyWaterRequirement;
import com.blstream.water_tracker.models.GoogleFitData;

import java.util.List;

/**
 * Work in progress
 */
public class Utils {

    /**
     * @param dataSize number of bars
     * @param i        number of bar which position will be returned
     * @param startX   start position where bars are drawn
     * @param endX     position to which bars are drawn
     * @return Array [0] - bar position X
     * [1] - bar thickness
     */
    public static int getBarPosX(int dataSize, int i, int startX, int endX) {
        int barSpace;
        int barThickness = getBarThickness(dataSize, startX, endX);
        if (dataSize > 1) {
            barSpace = (endX - startX) - (barThickness * dataSize);
            barSpace /= dataSize - 1;
        } else {
            barSpace = 0;
        }
        return startX + i * (barThickness + barSpace);
    }

    public static int getBarThickness(int dataSize, int startX, int endX) {
        int barThickness;
        if (dataSize > 1) {
            barThickness = (endX - startX) / (dataSize) / 2;
        } else {
            barThickness = endX;
        }
        return barThickness;
    }

    public static float findMaxValue(List<GoogleFitData> data) {
        float maxValue = 0;
        for (GoogleFitData piece : data) {
            float dailyWaterReq = DailyWaterRequirement.calculateWaterRequirement(piece.getWeight(), piece.getBurnedCalories());
            if (dailyWaterReq > maxValue) {
                maxValue = dailyWaterReq;
            }
            if (piece.getWaterIntake() > maxValue) {
                maxValue = piece.getWaterIntake();
            }
        }
        return maxValue;
    }

    public static int getPosYForValue(float value, float maxValue, int height) {
        float valuePercent = value / maxValue;
        return ((int) ((height) * valuePercent));
    }

    public static int getHeightOfBump(int barThickness){
        return barThickness / 2;
    }
}
