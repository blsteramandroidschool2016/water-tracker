package com.blstream.water_tracker.view.controls.waterSliderControls.utility;

import android.util.AttributeSet;
import android.util.DisplayMetrics;

/**
 * Manages parsing values from string data
 */
public class Parser {

    private static final String DENSITY_DP = "dip";
    private static final String DENSITY_SP = "sp";
    private static final String NAMESPACE = "http://schemas.android.com/apk/res/android";

    /**
     * Gets Height in pixels from given AttributeSet and DisplayMetrics.
     *
     * @param set     to extract value from
     * @param metrics to convert dp/sp to pixels
     * @return height in pixels
     */
    public static int getHeight(AttributeSet set, DisplayMetrics metrics) {
        final String LAYOUT_HEIGHT_KEY = "layout_height";
        String layoutHeight = set.getAttributeValue(NAMESPACE,
                LAYOUT_HEIGHT_KEY);
        return getValueInPixels(layoutHeight, metrics);
    }

    /**
     * Gets Width in pixels from given AttributeSet and DisplayMetrics.
     *
     * @param set     to extract value from
     * @param metrics to convert dp/sp to pixels
     * @return width in pixels
     */
    public static int getWidth(AttributeSet set, DisplayMetrics metrics) {
        final String LAYOUT_WIDTH_KEY = "layout_width";
        String layoutWidth = set.getAttributeValue(NAMESPACE,
                LAYOUT_WIDTH_KEY);
        return getValueInPixels(layoutWidth, metrics);
    }

    /**
     * Parses given mixed data and extracts float numbers
     *
     * @param valueToParse contains mixed data to parse
     * @return parsed numbers
     */
    public static float parseValueToFloat(String valueToParse) {
        float defaultValue = 0.0f;
        String numberOnly = valueToParse.replaceAll("(-?[a-z])", "");
        return numberOnly.isEmpty() ? defaultValue : Float.parseFloat(numberOnly);
    }

    /**
     * Extracts characters from specified value
     *
     * @param value to be extracted from
     * @return extracted characters in string
     */
    public static String extractCharactersFromValue(String value) {
        return value.replaceAll("-?[0-9.]", "");
    }

    private static int getValueInPixels(String value, DisplayMetrics metrics) {
        String density = extractCharactersFromValue(value);
        float valueFromString = parseValueToFloat(value);
        int valueInPx;
        switch (density) {
            case DENSITY_DP:
                valueInPx = (int) PixelConverter.convertDpToPx(valueFromString, metrics);
                break;
            case DENSITY_SP:
                valueInPx = (int) PixelConverter.convertSpToPx(valueFromString, metrics);
                break;
            default:
                valueInPx = (int) valueFromString;
                break;
        }
        return valueInPx;
    }

}
