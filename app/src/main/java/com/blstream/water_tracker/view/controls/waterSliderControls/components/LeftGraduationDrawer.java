package com.blstream.water_tracker.view.controls.waterSliderControls.components;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.blstream.water_tracker.R;
import com.blstream.water_tracker.interfaces.waterSlider.IWaterSliderControls;
import com.blstream.water_tracker.view.controls.switcher.CupOfWaterIcon;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.MainControlHelper;

/**
 * Draws left slider with current value inside it and background with scale
 */
public class LeftGraduationDrawer {
    public static final float[] SCALE_DIVISION_STEPS_CUPS = {0.2f, 0.4f, 1f, 2f};
    public static final float[] SCALE_DIVISION_STEPS_LITERS = {0.5f, 1f, 2f, 5f};
    public static final float TEXT_POS_X_RATIO = 0.6f; //less-left; more-right
    public static final float SMALL_RECT_WIDTH_RATIO = 0.11f;
    public static final float SMALL_RECT_HEIGHT_RATIO = 0.07f;
    public static final int SLIDER_RECTS_NUMBER = 6;
    public static final int INTEGER_SCALE_RECT_NUMBER = 6;
    public static final int FLOAT_SCALE_RECT_NUMBER = 4;
    public static final int SLIDER_POS_X = 0;
    public static final float CUP_SIZE = 0.2f;
    public static final float INITIAL_SCALE_VALUE = 0.1f;
    private Path sliderPath;
    private Path bgPath;
    private Rect bgRect;
    private Rect blueGradientRect;
    private Rect whiteGradientRect;
    private Rect smallRect;
    private RectF sliderArcRect;
    private Paint bluePaint;
    private Paint whitePaint;
    private Paint blueGradientPaint;
    private Paint whiteGradientPaint;
    private CupOfWaterIcon cupOfWaterIcon;
    private float value;
    private float maxValue;
    private int sliderThickness;
    private int controlWidth;
    private int bgPadding;
    private int controlHeight;
    private int textPosX;
    private int textSize;
    private int gradientHeight;
    private int slightlyDarkBlue;
    private int white;
    private int smallRectWidth;
    private int smallRectHeight;
    private int padding;
    private boolean liters;
    private int alphaState;


    /**
     * creates instances of fields
     */
    public LeftGraduationDrawer() {
        sliderPath = new Path();
        bgPath = new Path();
        bgRect = new Rect();
        smallRect = new Rect();
        blueGradientRect = new Rect();
        whiteGradientRect = new Rect();
        sliderArcRect = new RectF();
        bluePaint = new Paint();
        whitePaint = new Paint();
        blueGradientPaint = new Paint();
        whiteGradientPaint = new Paint();
    }

    /**
     * Setups slider variables
     *
     * @param view            on which slider will be drawn
     * @param sliderThickness of slider(counted with padding)
     * @param bgPadding       white padding around slider
     * @param controlWidth    width of whole control
     * @param controlHeight   height of whole control
     */
    public void updateValues(View view, int sliderThickness, int bgPadding, int controlWidth, int controlHeight) {
        this.sliderThickness = sliderThickness;
        this.controlWidth = controlWidth;
        this.bgPadding = bgPadding;
        this.controlHeight = controlHeight;
        this.textPosX = (int) (controlWidth - (sliderThickness * TEXT_POS_X_RATIO));
        textSize = sliderThickness / 2;
        gradientHeight = sliderThickness / 2;
        smallRectWidth = (int) (sliderThickness * SMALL_RECT_WIDTH_RATIO);
        smallRectHeight = (int) (sliderThickness * SMALL_RECT_HEIGHT_RATIO);
        setupPaints(view);
        cupOfWaterIcon = new CupOfWaterIcon(view, textPosX, getTextPos(value, maxValue), sliderThickness, sliderThickness / 2);
    }

    /**
     * Call this in View onDraw method
     *
     * @param canvas on which Slider will be drawn
     */
    public void draw(Canvas canvas) {
        int sliderPosY = MainControlHelper.getPosYpx(value, maxValue, controlHeight, padding) - sliderThickness / 2;
        generateSlider(SLIDER_POS_X, sliderPosY);
        canvas.drawRect(bgRect, bluePaint);
        int sliderTextPos = getTextPos(value, maxValue);
        drawDivision(canvas, sliderTextPos);
        liters = !liters;
        drawDivision(canvas, sliderTextPos);
        liters = !liters;
        canvas.drawPath(bgPath, whitePaint);
        canvas.drawPath(sliderPath, bluePaint);
        int smallRectPosY = (MainControlHelper.getPosYpx(value, maxValue, controlHeight, padding) - sliderThickness / 2) + ((sliderThickness - smallRectHeight) / 2);
        drawSmallRects(canvas, SLIDER_RECTS_NUMBER, smallRectPosY, whitePaint);
        liters = !liters;
        drawSmallRects(canvas, SLIDER_RECTS_NUMBER, smallRectPosY, whitePaint);
        liters = !liters;
        canvas.drawRect(blueGradientRect, blueGradientPaint);
        canvas.drawRect(whiteGradientRect, whiteGradientPaint);
        //Draw value on slider
        cupOfWaterIcon.setState(0);
        cupOfWaterIcon.setAlphaState(alphaState);
        drawText(canvas, value, textPosX, sliderTextPos, whitePaint);
        liters = !liters;
        drawText(canvas, value, textPosX, sliderTextPos, whitePaint);
        liters = !liters;
    }

    /**
     * Updates values and draws slider on new position corresponding to new values
     *
     * @param value    must be between IWaterSliderControls.MINIMUM_VALUE and max value
     * @param maxValue max scale value
     */
    public void updateValue(float value, float maxValue) {
        this.maxValue = maxValue;
        this.value = value;
    }

    /**
     * Sets padding for control value and maxValue position in px.
     *
     * @param padding in pixels
     */
    public void setPadding(int padding) {
        this.padding = padding;
    }

    /**
     * Sets scale to liters or cups
     * @param liters true- Scale is in Liters
     *               false - scale is in Cups
     */
    public void setLiters(boolean liters) {
        this.liters = liters;
    }

    /**
     * Sets alpha state of Graduation when alpha = 0 Switch is on left - Graduation with cupos is visible, liters are invisible
     *      when alpha = 1 Switch is on right - Graduation with cups is invisible, liters are shown
     * @param alphaState State from 0 (Switch on left) to 1 (Switch on right)
     */
    public void setAlphaState(float alphaState) {
        this.alphaState = (int) (alphaState * 255);
    }

    private float calculateScaleDivision() {
        if (liters) {
            return getDivisionStep(SCALE_DIVISION_STEPS_LITERS);
        } else {
            return getDivisionStep(SCALE_DIVISION_STEPS_CUPS);
        }
    }

    private float getDivisionStep(float[] steps) {
        final float DIVISION_STEP_HEIGHT_RATIO = 1.5f; //less - more minimal space for each division step
        float space = controlHeight - padding;
        float maxCups = (space / (sliderThickness / DIVISION_STEP_HEIGHT_RATIO));
        int i = 0;
        float cupNumber = maxValue / steps[i];
        while (cupNumber > maxCups) {
            if (i < steps.length - 1) {
                i++;
                cupNumber = maxValue / steps[i];
            } else {
                return steps[i];
            }
        }
        return steps[i];
    }

    private void drawDivision(Canvas canvas, int sliderTextPos) {
        float scaleDivision = calculateScaleDivision();
        for (float i = IWaterSliderControls.MINIMUM_VALUE; i <= maxValue; i += scaleDivision) {
            int newTextPos = getTextPos(i, maxValue);
            int smallRectPos = (MainControlHelper.getPosYpx(i, maxValue, controlHeight, padding) - sliderThickness / 2) + (sliderThickness - smallRectHeight) / 2;
            int rectNumber = getRectNumber(i);
            if (i > IWaterSliderControls.MINIMUM_VALUE + INITIAL_SCALE_VALUE) {
                if (newTextPos > sliderTextPos) {
                    cupOfWaterIcon.setState(0);
                    drawText(canvas, i, textPosX, newTextPos, whitePaint);
                    drawSmallRects(canvas, rectNumber, smallRectPos, whitePaint);
                } else {
                    cupOfWaterIcon.setState(1);
                    drawText(canvas, i, textPosX, newTextPos, bluePaint);
                    drawSmallRects(canvas, rectNumber, smallRectPos, bluePaint);
                }
            }
        }
    }

    /**
     * @param value scale value on which number will be drawn.
     *              For integer number - INTEGER_SCALE_RECT
     *              For non integer number with FLOAT_SCALE_RECT_NUMBER
     * @return number of rects
     */
    private int getRectNumber(float value) {
        if (liters) {
            if (value == Math.round(value)) {
                return INTEGER_SCALE_RECT_NUMBER;
            } else {
                return FLOAT_SCALE_RECT_NUMBER;
            }
        } else {
            int newValue = (int) (value / CUP_SIZE);
            if (newValue % 2 == 0) {
                return INTEGER_SCALE_RECT_NUMBER;
            } else {
                return FLOAT_SCALE_RECT_NUMBER;
            }
        }
    }

    /**
     * Generates small rects on given posY and with x = 0.
     * Distance between rect is rectHeight
     *
     * @param canvas on which rects will be drawn
     * @param number number of rects do draw
     * @param posY   position on which Rects will be drawn
     * @param paint  for small rects
     */
    private void drawSmallRects(Canvas canvas, int number, int posY, Paint paint) {
        if (liters) {
            paint.setAlpha(alphaState);
        } else {
            paint.setAlpha(alphaState * (-1) + 255);
        }
        for (int i = 0; i < number; i++) {
            int left = i * (smallRectWidth + smallRectHeight);
            int right = i * (smallRectWidth + smallRectHeight) + smallRectWidth;
            int bottom = posY + smallRectHeight;
            smallRect.set(left,
                    posY,
                    right,
                    bottom);
            canvas.drawRect(smallRect, paint);
        }
        paint.setAlpha(255);
    }

    private void drawText(Canvas canvas, float value, float textPosX, float textPosY, Paint paint) {
        if (liters) {
            paint.setAlpha(alphaState);
            String textValue = String.format("%.1f", value);
            textValue = textValue.replace(",", ".");
            textPosX -= paint.measureText(textValue);
            canvas.drawText(textValue, textPosX, textPosY, paint);
        } else {
            int oppositeAlphaState = (alphaState * (-1)) + 255;
            paint.setAlpha(oppositeAlphaState);
            int sliderPosY = MainControlHelper.getPosYpx(value, maxValue, controlHeight, padding) - sliderThickness / 2;
            int cupHeight = getCupHeight();
            int cupPosY = sliderPosY + cupHeight / 2;
            String text = String.valueOf(((int) (value / CUP_SIZE)));
            text = text + "x";
            canvas.drawText(text, textPosX - whitePaint.measureText(text) - cupHeight / 2, textPosY, paint);
            cupOfWaterIcon.setAlphaState(oppositeAlphaState);
            cupOfWaterIcon.draw(canvas, (int) (textPosX - cupHeight / 2), cupPosY, cupHeight);
        }
        paint.setAlpha(255);
    }

    private int getCupHeight() {
        return sliderThickness / 2;
    }

    private int getTextPos(float value, float maxValue) {
        float posYpx = MainControlHelper.getPosYpx(value, maxValue, controlHeight, padding) - sliderThickness / 2;
        return (int) ((posYpx + (sliderThickness / 2) + (textSize / 4)) + (whitePaint.descent() / 2));
    }

    /**
     * Generates slider with white padding on given position and its gradients
     *
     * @param posX slider pos
     * @param posY slider pos
     */
    private void generateSlider(int posX, int posY) {
        final int startAngle = 270;
        final int sweepAngle = 180;

        sliderPath.reset();
        sliderPath.moveTo(posX - sliderThickness, posY + bgPadding);
        sliderArcRect.set(posX - sliderThickness + controlWidth,
                posY + bgPadding,
                posX + controlWidth - bgPadding,
                posY + sliderThickness - bgPadding);
        sliderPath.arcTo(sliderArcRect,
                startAngle,
                sweepAngle);
        sliderPath.lineTo(posX, posY + sliderThickness - bgPadding);
        sliderPath.close();

        bgPath.reset();
        bgPath.moveTo(posX - sliderThickness, posY);
        sliderArcRect.set(posX + controlWidth - sliderThickness,
                posY,
                posX + controlWidth,
                posY + sliderThickness);
        bgPath.arcTo(sliderArcRect,
                startAngle,
                sweepAngle);
        bgPath.lineTo(posX, posY + sliderThickness);
        bgPath.close();

        bgRect.set(posX, posY + (sliderThickness / 2), posX + controlWidth, posY + controlHeight);

        generateGradients(posX, posY);
    }

    private void generateGradients(int posX, int posY) {
        setupGradientAndRect(whiteGradientRect,
                whiteGradientPaint,
                posX,
                posY - gradientHeight,
                posX + controlWidth,
                posY,
                Color.TRANSPARENT,
                white);
        setupGradientAndRect(blueGradientRect,
                blueGradientPaint,
                posX,
                posY + sliderThickness,
                posX + controlWidth,
                posY + sliderThickness + gradientHeight,
                slightlyDarkBlue,
                Color.TRANSPARENT);
    }

    private void setupGradientAndRect(Rect gradientRect, Paint gradientPaint, int left, int top, int right, int bottom, int topColor, int bottomColor) {
        gradientRect.set(left, top, right, bottom);
        gradientPaint.setShader(new LinearGradient(
                left + controlWidth / 2,
                top,
                right / 2,
                bottom,
                topColor,
                bottomColor,
                Shader.TileMode.CLAMP));
    }

    private void setupPaints(View view) {
        slightlyDarkBlue = ContextCompat.getColor(view.getContext(), R.color.slightly_dark_blue);
        white = ContextCompat.getColor(view.getContext(), R.color.white);

        bluePaint.setStyle(Paint.Style.FILL);
        bluePaint.setColor(slightlyDarkBlue);
        bluePaint.setAntiAlias(true);
        bluePaint.setTextSize(textSize);

        whitePaint.setStyle(Paint.Style.FILL);
        whitePaint.setColor(white);
        whitePaint.setAntiAlias(true);
        whitePaint.setTextSize(textSize);

    }

}
