package com.blstream.water_tracker.view.fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blstream.water_tracker.MainActivity;
import com.blstream.water_tracker.R;
import com.blstream.water_tracker.view.fragments.chartViewComponents.ChartViewPagerAdapter;
import com.blstream.water_tracker.view.fragments.chartViewComponents.DailyChartFragment;
import com.blstream.water_tracker.view.fragments.chartViewComponents.WeeklyChartFragment;

/**
 * View that displays historical chart
 */
public class ChartView extends BasicView {

    private Bundle args;

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.chart_screen_layout, container, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        args = getArguments();
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.chartViewPager);
        setupViewPager(viewPager);


        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.chartTabLayout);
        tabLayout.setupWithViewPager(viewPager);
        initialActionBar();
    }

    private void initialActionBar() {
        Toolbar myToolbar = (Toolbar) getActivity().findViewById(R.id.my_toolbar);
        ((MainActivity) getActivity()).setSupportActionBar(myToolbar);
        final ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.dark_blue)));
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(R.string.historicalChartLabel);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Defines the number of tabs by setting appropriate fragment and tab name.
     *
     * @param viewPager to configure
     */
    private void setupViewPager(ViewPager viewPager) {
        android.support.v4.app.FragmentManager manager = getChildFragmentManager();
        ChartViewPagerAdapter adapter = new ChartViewPagerAdapter(manager);
        DailyChartFragment dailyChartFragment = new DailyChartFragment();
        dailyChartFragment.setArguments(args);
        WeeklyChartFragment weeklyChartFragment = new WeeklyChartFragment();
        weeklyChartFragment.setArguments(args);
        adapter.addFragment(dailyChartFragment, getActivity().getString(R.string.dailyChartTabName));
        adapter.addFragment(weeklyChartFragment, getActivity().getString(R.string.weeklyChartTabName));
        viewPager.setAdapter(adapter);
    }

}
