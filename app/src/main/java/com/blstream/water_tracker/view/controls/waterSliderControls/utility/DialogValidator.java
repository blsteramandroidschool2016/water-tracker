package com.blstream.water_tracker.view.controls.waterSliderControls.utility;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Validates data inserted in dialog
 */
public class DialogValidator {

    private static final int MAX_WEIGHT_VALUE = 300;
    private static final int MAX_LENGTH = 3;
    private static final char FIRST_CHAR_ELEMENT = '0';
    private static final String EMPTY_CHAR = "";

    /**
     * Validates data
     *
     * @param data user inserted
     */
    public static void dataValid(final EditText data) {
        data.addTextChangedListener(new TextWatcher() {
            /**
             * {@inheritDoc}
             */
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            /**
             * {@inheritDoc}
             */
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validateIfFirstNumberIsNotZero(s, data);
                validateRangeOfNumber(s, data);
            }

            /**
             * {@inheritDoc}
             */
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    private static void validateRangeOfNumber(CharSequence s, EditText data) {
        int length = s.length();
        boolean isEmptyString = (s.length() == 0);
        int number = !isEmptyString ? Integer.parseInt(String.valueOf(s)) : 0;
        if (length > MAX_LENGTH || number > MAX_WEIGHT_VALUE) {
            data.getText().delete(length - 1, length);
        }
    }

    private static void validateIfFirstNumberIsNotZero(CharSequence s, EditText data) {
        if (s.length() > 0 && s.charAt(0) == FIRST_CHAR_ELEMENT) {
            data.setText(EMPTY_CHAR);
        }
    }
}
