package com.blstream.water_tracker.internetConnection;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class InternetConnectionObserverTest {

    InternetConnectionObserver internetConnectionObserver;

    @Before
    public void setup(){
        internetConnectionObserver = InternetConnectionObserver.getInstance();
    }

    @Test
    public void shouldReturnTheSameInstance(){
        //given
        //when
        InternetConnectionObserver newInternetConnectionObserver = InternetConnectionObserver.getInstance();
        //then
        assertEquals(internetConnectionObserver,newInternetConnectionObserver);
    }
}