package com.blstream.water_tracker.view.controls.waterSliderControls;

import android.util.Log;

import com.blstream.water_tracker.view.controls.waterSliderControls.utility.MainControlHelper;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for controlHelper class
 */
public class MainControlHelperTest {

    public static final float DELTA = 0.01f;

    @Test
    public void shouldReturnZero() throws Exception {
        //Given
        float value = 1;
        float maxValue = 1;
        int controlHeight = 100;
        //When
        int result = MainControlHelper.getPosYpx(value, maxValue, controlHeight, 0);
        int expected = 0;
        //Then
        assertEquals(expected, result);
    }

    @Test
    public void shouldReturnHalfOfPadding() throws Exception {
        //Given
        float value = 1;
        float maxValue = 1;
        int controlHeight = 100;
        int padding = 10;
        //When
        int result = MainControlHelper.getPosYpx(value, maxValue, controlHeight, padding);
        int expected = padding / 2;
        //Then
        assertEquals(expected, result);
    }

    @Test
    public void shouldReturnHalfOfControlHeight() throws Exception {
        //Given
        float value = 1;
        float maxValue = 2;
        int controlHeight = 100;
        //When
        int result = MainControlHelper.getPosYpx(value, maxValue, controlHeight, 0);
        int expected = 50;
        //Then
        assertEquals(expected, result);
    }

    @Test
    public void shouldReturnMaxValue() {
        //Given
        int controlHeight = 100;
        float maxValue = 2;
        int posPx = 0; //slider on top of Control
        //When
        float result = MainControlHelper.getValueForPosYpx(posPx, maxValue, controlHeight, 0);
        float expected = maxValue;
        //Then
        assertEquals(expected, result, DELTA);
    }

    @Test
    public void shouldReturnMinValue() {
        //Given
        int controlHeight = 100;
        int padding = 10;
        float maxValue = 2;
        int posPx = controlHeight - padding;  //slider on bottom of Control
        Log.d("test", "shouldReturnMinValue: posPx :" + posPx);
        //When
        float result = MainControlHelper.getValueForPosYpx(posPx, maxValue, controlHeight, padding);
        float expected = 0;
        //Then
        assertEquals(expected, result, DELTA);
    }

    @Test
    public void shouldReturnHalfOfMaxValue() {
        //Given
        int controlHeight = 100;
        int padding = 0;
        float maxValue = 2;
        int posPx = (controlHeight - padding) / 2;  //slider on half of control
        //When
        float result = MainControlHelper.getValueForPosYpx(posPx, maxValue, controlHeight, padding);
        float expected = 1;
        //Then
        assertEquals(expected, result, DELTA);
    }
}