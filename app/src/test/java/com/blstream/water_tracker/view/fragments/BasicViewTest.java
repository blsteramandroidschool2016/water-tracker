package com.blstream.water_tracker.view.fragments;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.blstream.water_tracker.R;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BasicViewTest extends BasicView {
    @Mock
    ConnectivityManager connectivityManager;
    @Mock
    NetworkInfo networkInfo;
    @Mock
    FragmentManager fragmentManager;
    @Mock
    FragmentTransaction fragmentTransaction;
    @Mock
    MainView mainView;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(fragmentManager.beginTransaction()).thenReturn(fragmentTransaction);
        when(fragmentTransaction.replace(R.id.mainViewContainer, mainView)).thenReturn(fragmentTransaction);
        when(fragmentTransaction.commit()).thenReturn(1);
    }

    @Test
    public void shouldReturnTrue_ThereIsInternetConnection() {
        //given
        when(connectivityManager.getActiveNetworkInfo()).thenReturn(networkInfo);
        when(networkInfo.isConnectedOrConnecting()).thenReturn(true);
        //when
        boolean isInternetConnection = isInternetConnection(connectivityManager);
        //then
        assertTrue(isInternetConnection);
    }

    @Test
    public void shouldReturnFalse_ThereIsNoInternetConnection() {
        //given
        when(connectivityManager.getActiveNetworkInfo()).thenReturn(networkInfo);
        when(networkInfo.isConnectedOrConnecting()).thenReturn(false);
        //when
        boolean isInternetConnection = isInternetConnection(connectivityManager);
        //then
        assertFalse(isInternetConnection);
    }

    @Test
    public void shouldReturnFalse_NetworkInfoIsNull() {
        //given
        when(connectivityManager.getActiveNetworkInfo()).thenReturn(null);
        //when
        boolean isInternetConnection = isInternetConnection(connectivityManager);
        //then
        assertFalse(isInternetConnection);
    }

    @Test
    public void shouldReplaceFragment() {
        //given
        //when
        replaceFragment(fragmentManager, mainView, R.id.mainViewContainer);
        //then
        verify(fragmentTransaction).commit();
    }

    @Test
    public void shouldNotReplaceFragment_FragmentDoNotExist() {
        //given
        //when
        replaceFragment(fragmentManager, null, R.id.mainViewContainer);
        //then
        verify(fragmentTransaction, never()).commit();
    }
}