package com.blstream.water_tracker.view.controls;

import com.blstream.water_tracker.view.controls.waterSliderControls.utility.Parser;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Tests Parser methods
 */
public class ParserUnitTest {

    @Test
    public void shouldReturnZeroWhenValueDoesNotContainAnyNumber() throws Exception {
        //given
        String value = "aaabbbccc";

        //when
        float actual = Parser.parseValueToFloat(value);
        float expected = 0.0f;

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnNumberWhenValueContainsAny() throws Exception {
        //given
        String value = "20px";

        //when
        float actual = Parser.parseValueToFloat(value);
        float expected = 20.0f;

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnNumberWhenValueContainsNegativeNumber() throws Exception {
        //given
        String value = "-20px";

        //when
        float actual = Parser.parseValueToFloat(value);
        float expected = -20.0f;

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnExtractedCharactersFromGivenValue() throws Exception {
        //given
        String value = "-20px";

        //when
        String actual = Parser.extractCharactersFromValue(value);
        String expected = "px";

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnEmptyStringWhenThereIsNoCharactersToBeExtracted() throws Exception {
        //given
        String value = "-2045";

        //when
        String actual = Parser.extractCharactersFromValue(value);
        String expected = "";

        //then
        assertEquals(expected, actual);
    }

}