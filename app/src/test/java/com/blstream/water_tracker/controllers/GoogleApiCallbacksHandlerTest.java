package com.blstream.water_tracker.controllers;

import com.blstream.water_tracker.Constants;
import com.blstream.water_tracker.interfaces.IGoogleApiCallback;
import com.google.android.gms.common.ConnectionResult;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

public class GoogleApiCallbacksHandlerTest {

    @Mock
    private IGoogleApiCallback listener;
    private GoogleApiCallbacksHandler googleApiCallbacksHandler;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        googleApiCallbacksHandler = new GoogleApiCallbacksHandler();
    }

    @Test
    public void notifyAboutActivityResult() throws Exception {
        // given
        int responseCode = -1;
        int requestCode = Constants.RC_SIGN_IN;
        int RESULT_OK = -1;
        googleApiCallbacksHandler.registerListener(listener);
        // when
        googleApiCallbacksHandler.notifyAboutActivityResult(requestCode, responseCode, null, RESULT_OK);
        // then
        verify(listener).onGoogleApiActivityResult(requestCode, responseCode, null, RESULT_OK);
    }

    @Test
    public void registerListener() throws Exception {
        // given
        int expectedRegisteredCallbacksCount = 1;
        // when
        googleApiCallbacksHandler.registerListener(listener);
        int actualRegisteredCallbacksCount = googleApiCallbacksHandler.callbacks.size();
        // then
        assertEquals(expectedRegisteredCallbacksCount, actualRegisteredCallbacksCount);
    }

    @Test
    public void unregisterListener() throws Exception {
        // given
        int expectedRegisteredCallbacksCount = 0;
        googleApiCallbacksHandler.registerListener(listener);
        // when
        googleApiCallbacksHandler.unregisterListener(listener);
        int actualRegisteredCallbacksCount = googleApiCallbacksHandler.callbacks.size();
        // then
        assertEquals(expectedRegisteredCallbacksCount, actualRegisteredCallbacksCount);
    }

    @Test
    public void onConnected() throws Exception {
        // given
        googleApiCallbacksHandler.registerListener(listener);
        // when
        googleApiCallbacksHandler.onConnected(null);
        // then
        verify(listener).onConnected(null);
    }

    @Test
    public void onConnectionFailed() throws Exception {
        // given
        ConnectionResult connectionResult = new ConnectionResult(ConnectionResult.SIGN_IN_REQUIRED);
        googleApiCallbacksHandler.registerListener(listener);
        // when
        googleApiCallbacksHandler.onConnectionFailed(connectionResult);
        // then
        verify(listener).onConnectionFailed(connectionResult);
    }

    @Test
    public void onConnectionSuspended() throws Exception {
        // given
        int i = 0;
        googleApiCallbacksHandler.registerListener(listener);
        // when
        googleApiCallbacksHandler.onConnectionSuspended(i);
        // then
        verify(listener).onConnectionSuspended(i);
    }
}