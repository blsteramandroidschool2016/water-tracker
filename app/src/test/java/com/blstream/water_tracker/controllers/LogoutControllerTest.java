package com.blstream.water_tracker.controllers;

import android.content.Context;

import com.blstream.water_tracker.Constants;
import com.blstream.water_tracker.view.fragments.MainView;
import com.google.android.gms.common.api.GoogleApiClient;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LogoutControllerTest {

    String accountName = "m.kaczynska@gmail.com";
    @Mock
    private MainView mainView;
    @Mock
    private GoogleApiClient googleApiClient;
    @Mock
    private Context context;
    @Mock
    private LogoutController.SharedPreferencesControllerFactory factory;
    private LogoutController logoutController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        logoutController = new LogoutController(mainView);
        LogoutController spyLogoutController = spy(logoutController);
        when(spyLogoutController.getView()).thenReturn(mainView);
    }

    @Test
    public void testRemoveUserDataFromAppWhenIsNotLogged() throws Exception {
        //given
        when(factory.isUserLogged(context, accountName)).thenReturn(false);
        //when
        logoutController.removeUserDataFromApp(context, accountName);
        //then
        verify(factory, never()).removeUserData(context, Constants.ACCOUNT_NAME_KEY);
    }
}