package com.blstream.water_tracker.controllers;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Test for WaterUpdateController
 */
public class WaterUpdateControllerTest {
    private static final int NEXT_VALUE = 1;
    private Timestamp lastUpdateTime;
    private Calendar calendar;
    private Date date;

    @Before
    public void setUp() throws Exception {
        calendar = Calendar.getInstance();
        date = new Date();
        calendar.setTime(date);
    }

    @After
    public void tearDown() throws Exception {
        lastUpdateTime = null;
        calendar = null;
        date = null;
    }

    @Test
    public void shouldReturnFalseWhenDayIsTheSameAsCurrentAndOtherValuesAreDifferent() throws Exception {
        //given
        lastUpdateTime = new Timestamp(System.currentTimeMillis());

        //when
        boolean actual = WaterUpdateController.checkIfOutOfDate(lastUpdateTime);

        //then
        Assert.assertFalse(actual);

    }

    @Test
    public void shouldReturnTrueWhenDayAndMonthAreTheSameAsCurrentButYearIsDifferent() throws Exception {
        //given
        calendar.add(Calendar.YEAR, NEXT_VALUE);
        date = calendar.getTime();
        lastUpdateTime = new Timestamp(date.getTime());
        //when
        boolean actual = WaterUpdateController.checkIfOutOfDate(lastUpdateTime);

        //then
        Assert.assertTrue(actual);

    }

    @Test
    public void shouldReturnTrueWhenDayIsTheSameAsCurrentMonthIsDifferentAndYearIsTheSameAsCurrent() throws Exception {
        //given
        calendar.add(Calendar.MONTH, NEXT_VALUE);
        date = calendar.getTime();
        lastUpdateTime = new Timestamp(date.getTime());
        //when
        boolean actual = WaterUpdateController.checkIfOutOfDate(lastUpdateTime);

        //then
        Assert.assertTrue(actual);

    }

    @Test
    public void shouldReturnTrueWhenDayIsDifferentMonthAndYearAreTheSameAsCurrent() throws Exception {
        //given
        calendar.add(Calendar.DAY_OF_MONTH, NEXT_VALUE);
        date = calendar.getTime();
        lastUpdateTime = new Timestamp(date.getTime());
        //when
        boolean actual = WaterUpdateController.checkIfOutOfDate(lastUpdateTime);

        //then
        Assert.assertTrue(actual);
    }

    @Test
    public void shouldReturnTrueWhenDayAndMonthAreDifferentButYearIsTheSameAsCurrent() throws Exception {
        //given
        calendar.add(Calendar.DAY_OF_MONTH, NEXT_VALUE);
        calendar.add(Calendar.MONTH, NEXT_VALUE);
        date = calendar.getTime();
        lastUpdateTime = new Timestamp(date.getTime());
        //when
        boolean actual = WaterUpdateController.checkIfOutOfDate(lastUpdateTime);

        //then
        Assert.assertTrue(actual);
    }

    @Test
    public void shouldReturnTrueWhenDayAndYearAreDifferentButMonthIsTheSameAsCurrent() throws Exception {
        //given
        calendar.add(Calendar.DAY_OF_MONTH, NEXT_VALUE);
        calendar.add(Calendar.YEAR, NEXT_VALUE);
        date = calendar.getTime();
        lastUpdateTime = new Timestamp(date.getTime());
        //when
        boolean actual = WaterUpdateController.checkIfOutOfDate(lastUpdateTime);

        //then
        Assert.assertTrue(actual);
    }

    @Test
    public void shouldReturnTrueWhenAllValuesAreDifferent() throws Exception {
        //given
        calendar.add(Calendar.DAY_OF_MONTH, NEXT_VALUE);
        calendar.add(Calendar.MONTH, NEXT_VALUE);
        calendar.add(Calendar.YEAR, NEXT_VALUE);
        date = calendar.getTime();
        lastUpdateTime = new Timestamp(date.getTime());
        //when
        boolean actual = WaterUpdateController.checkIfOutOfDate(lastUpdateTime);

        //then
        Assert.assertTrue(actual);
    }
}