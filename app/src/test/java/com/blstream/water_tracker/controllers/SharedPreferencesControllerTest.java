package com.blstream.water_tracker.controllers;

import android.content.Context;
import android.content.SharedPreferences;

import com.blstream.water_tracker.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class SharedPreferencesControllerTest {

    @Mock
    private SharedPreferences sharedPreferences;
    @Mock
    private SharedPreferences.Editor editor;
    @Mock
    private Context context;
    String accountName = "m.kaczynska@gmail.com";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(sharedPreferences);
        when(sharedPreferences.edit()).thenReturn(editor);
        editor.apply();
    }

    @Test
    public void testSetLoginStatusToTrue() {
        //given
        //when
        SharedPreferencesController.setUserAsLogged(context, accountName);
        //then
        verify(editor).putBoolean(accountName, true);
    }

    @Test
    public void testSetLoginStatusToFalse() {
        //given
        //when
        SharedPreferencesController.setUserAsLogged(context, accountName);
        //then
        verify(editor).putBoolean(accountName, true);
    }

    @Test
    public void testRemoveUserDataRightKey() {
        //given
        when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(sharedPreferences);
        //when
        SharedPreferencesController.removeUserData(context, accountName);
        //then
        verify(editor).remove(accountName);
    }

    @Test
    public void testIsUserLogged() {
        //given
        when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(sharedPreferences);
        //when
        SharedPreferencesController.isUserLogged(context, accountName);
        //then
        verify(sharedPreferences).getBoolean(accountName, false);
    }

    @Test
    public void putSwitchState(){
        //Given

        //When
        SharedPreferencesController.setSwitchState(context, true);
        //Then
        verify(editor).putBoolean(Constants.SWITCH_STATE_KEY, true);
    }

    @Test
    public void getSwitchState(){
        //Given
        when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(sharedPreferences);
        //When
        SharedPreferencesController.getSwitchState(context);
        //Then
        verify(sharedPreferences).getBoolean(Constants.SWITCH_STATE_KEY, true);
    }
}