package com.blstream.water_tracker.view.controls.waterSliderControls;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.RelativeLayout;

import com.blstream.water_tracker.MainActivity;
import com.blstream.water_tracker.R;
import com.blstream.water_tracker.view.controls.waterSliderControls.components.CircleView;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.MainControlHelper;
import com.blstream.water_tracker.view.fragments.MainView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class CircleViewTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);
    private CircleView circleView;
    private RelativeLayout layout;

    @Before
    public void setUp() {
        MainView mainView = new MainView();
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainViewContainer, mainView).commit();
        layout = (RelativeLayout) mActivityRule.getActivity().findViewById(R.id.waterSlider);
        assert layout != null;
        circleView = (CircleView) layout.findViewById(R.id.sliderControl);
    }

    @After
    public void tearDown() {
        mActivityRule.getActivity().finish();
    }

    @Test
    public void shouldSetStartStateAfterFirstActivityCreate() {
        //given
        int layoutWidth = layout.getLayoutParams().width;
        int layoutHeight = layout.getLayoutParams().height;
        float circleRadius = circleView.calculateCircleRadius(layoutWidth);
        int value = layoutHeight - (int) circleRadius;
        //when
        float expectedValue = MainControlHelper.getValueForPosYpx(value, circleView.getMaxValue(), layoutHeight, (int) circleRadius);
        float stateValue = circleView.getState();
        //then
        float delta = 0.01f;
        assertEquals(expectedValue, stateValue, delta);
    }

    @Test
    public void shouldSetCircleRadiusAsTwenty() {
        //given
        int layoutWidth = 200;
        float expected = 20.0f;
        //when
        float circleRadius = circleView.calculateCircleRadius(layoutWidth);
        //then
        assertEquals(expected, circleRadius);
    }

    @Test
    public void shouldReturnTrueTheTouchIsOnControls() {
        //given
        int layoutWidth = 300;
        float state = circleView.getYCircleCoordinates();
        //when
        float hitX = 150;
        boolean isControlTouched = circleView.hitThumb(hitX, state, layoutWidth);
        //then
        assertTrue(isControlTouched);
    }

    @Test
    public void shouldReturnFalseTheTouchIsNotOnControls() {
        //given
        float xPos = 50;
        int layoutWidth = 600;
        float state = circleView.getYCircleCoordinates();
        //when
        boolean isControlTouched = circleView.hitThumb(xPos, state, layoutWidth);
        //then
        assertFalse(isControlTouched);
    }
}