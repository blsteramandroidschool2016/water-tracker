package com.blstream.water_tracker;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.UiThreadTest;

import com.blstream.water_tracker.controllers.SharedPreferencesController;
import com.blstream.water_tracker.interfaces.waterSlider.IWaterSliderControls;
import com.blstream.water_tracker.view.controls.waterSliderControls.WaterSliderController;
import com.blstream.water_tracker.view.controls.waterSliderControls.utility.MainControlHelper;
import com.blstream.water_tracker.view.fragments.MainView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for WaterSlider
 */
@RunWith(AndroidJUnit4.class)
public class WaterSliderTest {

    public static final float DELTA = 0.3f;
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    private WaterSliderController controller;

    @UiThreadTest
    @Before
    public void setUp() throws Throwable {
        String accountName = "person@example.com";
        if (!SharedPreferencesController.isUserLogged(mActivityRule.getActivity().getApplicationContext(), accountName)) {
            SharedPreferencesController.setUserAsLogged(mActivityRule.getActivity().getApplicationContext(), accountName);
            mActivityRule.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainViewContainer, new MainView()).commit();
        }
        onView(withId(R.id.switchControl)).check(ViewAssertions.matches(isDisplayed()));
        controller = (WaterSliderController) mActivityRule.getActivity().findViewById(R.id.waterSlider);
        mActivityRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                controller.setStateWithAnimation(0);
            }
        });
    }

    @After
    public void tearDown() throws Exception {
        mActivityRule.getActivity().finish();
    }


    @Test
    public void shouldReturnZeroAfterCallSetStateZero() {
        //Given

        //When

        //Then
        assertEquals(controller.getState(), 0f);
    }


//    @Test
//    public void shouldReturnOneAfterDragingToPsyhicalPositionNearOne() {
//        //Given
//        int scrollX = controller.getWidth() / 2;
//        int scrollY1 = MainControlHelper.getPosYpx(1.0f, controller.getMaxValue(), controller.getHeight(), (int) controller.initialGapHeight());
//        int scrollY2 = MainControlHelper.getPosYpx(0, controller.getMaxValue(), controller.getHeight(), (int) controller.initialGapHeight());
//        //When
//        onView(withId(R.id.waterSlider)).perform(CustomActions.swipe(scrollX, scrollY2, scrollX, scrollY1));
//        //Then
//        assertEquals(1f, controller.getState(), DELTA);
//    }
//
//    @Test
//    public void maxValueShouldBeAboveTwo(){
//        //Given
//        int scrollX = controller.getWidth() / 2;
//        int scrollY1 = MainControlHelper.getPosYpx(2.0f, controller.getMaxValue(), controller.getHeight(), (int) controller.initialGapHeight());
//        int scrollY2 = MainControlHelper.getPosYpx(0, controller.getMaxValue(), controller.getHeight(), (int) controller.initialGapHeight());
//        //When
//        onView(withId(R.id.waterSlider)).perform(CustomActions.swipe(scrollX, scrollY2, scrollX, scrollY1));
//        boolean moreThanTwo = controller.getMaxValue() > 2f;
//        float expected = IWaterSliderControls.MAXIMUM_VALUE;
//        //Then
//        assertTrue(moreThanTwo);
//        assertEquals(expected, controller.getState(), DELTA);
//        //During dragging to max value, maxvalue extends to allow you select higher value.
//        //When animations are turned off maxValue instantly scales to MAXIMUM_VALUE.
//    }

}
