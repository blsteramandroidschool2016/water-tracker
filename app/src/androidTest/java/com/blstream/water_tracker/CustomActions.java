package com.blstream.water_tracker;

import android.support.annotation.NonNull;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.CoordinatesProvider;
import android.support.test.espresso.action.GeneralClickAction;
import android.support.test.espresso.action.GeneralSwipeAction;
import android.support.test.espresso.action.Press;
import android.support.test.espresso.action.Swipe;
import android.support.test.espresso.action.Tap;
import android.view.View;

/**
 * Contains custom actions for espresso tests
 */
public class CustomActions {

    /**
     * Action that performs single click on given coords
     *
     * @param x coord where perform click
     * @param y coord where perform click
     * @return action that click on given coordinates
     */
    public static ViewAction clickXY(final int x, final int y) {
        return new GeneralClickAction(
                Tap.SINGLE,
                getCoords(x, y),
                Press.FINGER);
    }

    /**
     * Returns action that performs swipe from specific coords to specific coords
     *
     * @param startX coord where start performing swipe
     * @param startY coord where start performing swipe
     * @param endX   coord where swipe will end
     * @param endY   coord where swipe will end
     * @return ViewAction that performs swipe
     */
    public static ViewAction swipe(final int startX, final int startY, final int endX, final int endY) {
        CoordinatesProvider start = getCoords(startX, startY);
        CoordinatesProvider end = getCoords(endX, endY);

        return new GeneralSwipeAction(Swipe.FAST, start, end, Press.FINGER);
    }

    @NonNull
    private static CoordinatesProvider getCoords(final int startX, final int startY) {
        return new CoordinatesProvider() {
            @Override
            public float[] calculateCoordinates(View view) {
                final int[] screenPos = new int[2];
                view.getLocationOnScreen(screenPos);
                final float screenX = screenPos[0] + startX;
                final float screenY = screenPos[1] + startY;

                return new float[]{screenX, screenY};
            }
        };
    }
}
