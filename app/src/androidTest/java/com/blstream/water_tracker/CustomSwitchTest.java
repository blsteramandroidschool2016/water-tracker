package com.blstream.water_tracker;

import android.support.annotation.NonNull;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.blstream.water_tracker.controllers.GoogleFitController;
import com.blstream.water_tracker.controllers.SharedPreferencesController;
import com.blstream.water_tracker.models.UserGoogleFitData;
import com.blstream.water_tracker.view.controls.switcher.SwitchControl;
import com.blstream.water_tracker.view.fragments.MainView;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Espresso tests for Switch
 */
public class CustomSwitchTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    private SwitchControl switchControl;

    @Before
    public void setUp() {
        String accountName = UserGoogleFitData.getAccountName();
        if(!SharedPreferencesController.isUserLogged(mActivityRule.getActivity().getApplicationContext(), accountName)) {
            SharedPreferencesController.setUserAsLogged(mActivityRule.getActivity().getApplicationContext(), accountName);
            mActivityRule.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainViewContainer, new MainView()).commit();
        }
        onView(withId(R.id.switchControl)).check(ViewAssertions.matches(isDisplayed()));
        switchControl = (SwitchControl) mActivityRule.getActivity().findViewById(R.id.switchControl);
    }

    @After
    public void tearDown() throws Exception {
        mActivityRule.getActivity().finish();
    }

    @Test
    public void shouldBeCheckedAfterClick()   {
        //Given
        //When
        onView(withId(R.id.switchControl)).perform(click());
        boolean state = switchControl.isChecked();
        //Then
        assertTrue(state);
    }

    @Test
    public void shouldBeCheckedAfterClickLeftSide() {
        //Given
        int centerOfLeftSideX = switchControl.getWidth() / 4;
        int centerY = switchControl.getHeight() / 2;
        //When
        onView(getView()).perform(CustomActions.clickXY(centerOfLeftSideX, centerY));
        boolean state = switchControl.isChecked();
        //Then
        assertTrue(state);
    }

    @Test
    public void shouldBeCheckedAfterClickRightSide() {
        //Given
        int centerOfRightSideX = (switchControl.getWidth() / 4) * 3;
        int centerY = switchControl.getHeight() / 2;
        //When
        onView(getView()).perform(CustomActions.clickXY(centerOfRightSideX, centerY));
        boolean state = switchControl.isChecked();
        //Then
        assertTrue(state);
    }

    @Test
    public void shouldBeUncheckedAfterClickOutside() throws Exception {
        //Given
        int outOfBoundPadding = 10;
        int xOutsideOfView = switchControl.getWidth() + outOfBoundPadding;
        int centerY = switchControl.getHeight() / 2;
        //When
        onView(getView()).perform(CustomActions.clickXY(xOutsideOfView, centerY));
        boolean state = switchControl.isChecked();
        //Then
        assertFalse(state);
    }

    @Test
    public void shouldBeEnabledAfterSwipeFromLeftCenterToRightCenter() {
        //Given
        int centerOfLeftSideX = switchControl.getWidth() / 4;
        int centerOfRightSideX = (switchControl.getWidth() / 4) * 3;
        int centerY = switchControl.getHeight() / 2;
        //When
        onView(getView()).perform(CustomActions.swipe(centerOfLeftSideX, centerY, centerOfRightSideX, centerY));
        boolean state = switchControl.isChecked();
        //Then
        assertTrue(state);
    }

    @Test
    public void shouldBeEnabledAfterSwipeFromRightCenterToLeftCenter() {
        //Given
        int centerOfLeftSideX = switchControl.getWidth() / 4;
        int centerOfRightSideX = (switchControl.getWidth() / 4) * 3;
        int centerY = switchControl.getHeight() / 2;
        //When
        onView(getView()).perform(CustomActions.swipe(centerOfLeftSideX, centerY,centerOfRightSideX , centerY));
        boolean state = switchControl.isChecked();
        //Then
        assertTrue(state);
    }

    @Test
    public void shouldBeEnabledAfterSwipeFromRightCenterToRightOutside() {
        //Given
        int centerOfLeftSideX = switchControl.getWidth() / 4;
        int outOfBound = 100;
        int rightOutsideX = switchControl.getWidth() + outOfBound;
        int centerY = switchControl.getHeight() / 2;
        //When
        onView(getView()).perform(CustomActions.swipe(centerOfLeftSideX, centerY, rightOutsideX, centerY));
        boolean state = switchControl.isChecked();
        //Then
        assertTrue(state);
    }

    @Test
    public void shouldBeDisabledAfterSwipeFromRightCenterToLeftOutside() {
        //Given
        int centerOfLeftSideX = switchControl.getWidth() / 4;
        int leftOutsideX = -100;
        int centerY = switchControl.getHeight() / 2;
        //When
        onView(getView()).perform(CustomActions.swipe(centerOfLeftSideX, centerY, leftOutsideX, centerY));
        boolean state = switchControl.isChecked();
        //Then
        assertFalse(state);
    }


    @NonNull
    private Matcher<View> getView() {
        return withId(R.id.switchControl);
    }
}
