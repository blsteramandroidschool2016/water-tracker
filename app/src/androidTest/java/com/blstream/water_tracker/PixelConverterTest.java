package com.blstream.water_tracker;

import android.support.test.rule.ActivityTestRule;
import android.util.DisplayMetrics;

import com.blstream.water_tracker.view.controls.waterSliderControls.utility.PixelConverter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Some tests for PixelConverter.
 */
public class PixelConverterTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);
    private DisplayMetrics metrics;

    @Before
    public void setUp() throws Exception {
        metrics = new DisplayMetrics();
        metrics.widthPixels = 0;
        metrics.heightPixels = 0;
        metrics.density = 2;
        metrics.densityDpi = 320;
        metrics.xdpi = metrics.density;
        metrics.ydpi = metrics.density;
        metrics.scaledDensity = 2;
    }

    @Test
    public void convertDp() {
        //Given
        float dp = 10;
        //when
        float px = PixelConverter.convertDpToPx(dp, metrics);
        float expected = 20f;
        //Then
        assertEquals(expected, px);
    }

    @Test
    public void convertDpNegative() {
        //Given
        float dp = -10;
        //When
        float px = PixelConverter.convertDpToPx(dp, metrics);
        float expected = 0;
        //Then
        assertEquals(expected, px);
    }

    @Test
    public void convertSp() {
        //Given
        float sp = 10;
        //when
        float px = PixelConverter.convertSpToPx(sp, metrics);
        float expected = 20f;
        //Then
        assertEquals(expected, px);
    }

    @Test
    public void convertSpNegative() {
        //Given
        float sp = -10;
        //When
        float px = PixelConverter.convertSpToPx(sp, metrics);
        float expected = 0;
        //Then
        assertEquals(expected, px);
    }
}