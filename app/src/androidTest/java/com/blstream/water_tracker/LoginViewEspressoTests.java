package com.blstream.water_tracker;

import android.content.pm.ActivityInfo;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.blstream.water_tracker.view.fragments.LoginView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class LoginViewEspressoTests {

    @Rule
    public final ActivityTestRule<MainActivity> base = new ActivityTestRule<>(MainActivity.class);
    private static final String FRAGMENT_KEY = "FragmentKey";
    private LoginView loginView;
    @Before
    public void setup(){
        loginView = new LoginView();
        base.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainViewContainer,loginView,FRAGMENT_KEY).commit();
    }
    @After
    public void tearDown(){
        base.getActivity().finish();
    }
    @Test
    public void logoIsDisplayed(){
        onView(withId(R.id.waterTrackerLogo)).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void buttonsShouldBeDisplayedAtStartScreen(){
        onView(withId(R.id.no_thanks_button)).check(ViewAssertions.matches(isDisplayed()));
        onView(withId(R.id.sign_in_button)).check(ViewAssertions.matches(isDisplayed()));
    }
    @Test
    public void afterOrientationChange_buttonsAndLogoShouldBeVisible(){
        //given
        //when
        base.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //then
        buttonsShouldBeDisplayedAtStartScreen();
        logoIsDisplayed();
    }
}
